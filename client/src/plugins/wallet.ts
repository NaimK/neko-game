import { BrowserProvider } from 'ethers'

import LocalizationPlugin from './localization'

import { get } from '~/core/api'
import UnauthorizedAddressError from '~/core/errors/UnauthorizedAddressError'
import EventBus from '~/core/EventBus'
import { networks } from '~/helpers/networks'
import { normalizeAddress } from '~/helpers/strings'
import GameState from '~/store/game'

export default class WalletPlugin extends Phaser.Plugins.BasePlugin {  
  private i18n!: LocalizationPlugin
  
  public provider!: BrowserProvider

  public isActive!: boolean

  public onlyWhitelisted!: boolean

  public isAuthorized!: boolean

  public allowedNetworks!: number[]

  constructor(pluginManager: Phaser.Plugins.PluginManager) {    
    super(pluginManager)
    this.i18n = pluginManager.get('LocalizationPlugin') as LocalizationPlugin
    this.isActive = import.meta.env.VITE_SKIP_WALLET === 'true' ? false : true
    this.allowedNetworks = import.meta.env.VITE_ALLOWED_NETWORKS.split(',').map((n: string) => Number(n))
  }

  public async start(): Promise<void> {
    const result = await get('/api/whitelisted/restrict') as any
    this.onlyWhitelisted = result.restrict

    if (!this.isActive) return

    if (!window.ethereum) {
      console.log('No Ethereum wallet detected.')
    } else {
      this.provider = new BrowserProvider(window.ethereum)
      this.getConnectedUserAccount()
      this.bindListeners()
    }
  }

  private bindListeners(): void {
    window.ethereum.on('accountsChanged', this.handleAccountsChanged)
  }

  private async getConnectedUserAccount(): Promise<void> {
    try {
      const accounts = await this.provider.listAccounts()

      if (accounts.length > 0) {
        const address = accounts[0].address
        this.setWallet(address, true)
      }
    } catch (error) {
      EventBus.emit(
        'message', 'error', error,
      )
    }
  }

  private handleAccountsChanged = (accounts: string[]): void => {
    EventBus.emit('accounts-changed', { accounts })
    
    if (accounts.length > 0) {
      const address = accounts[0]

      this.setWallet(address, true)
    } else {
      GameState.set('wallet', { address: null })
      EventBus.emit('wallet-disconnected')
    }
  }

  public async connectWallet(): Promise<void> {
    try {
      await this.provider.send('eth_requestAccounts', [])
      const address = await (await this.provider.getSigner()).address

      if (this.onlyWhitelisted) {
        this.isAuthorized = await this.checkIsWhitelisted(address)

        if (!this.isAuthorized) {
          throw new UnauthorizedAddressError(`Unauthorized address: ${address}`)
        }
      } else {
        this.isAuthorized = true
      }
      this.setWallet(address)
    } catch (error) {
      console.log('error', error)

      const messageKey = error instanceof UnauthorizedAddressError ? 'wallet_connect_unauthorized_address' : 'wallet_connect_error'
      
      EventBus.emit('text-message', {
        text: this.i18n.t(messageKey),
        size: 'big',
        duration: 5000,
        typewriter: false,
      })
    } 
  }

  private async setWallet(address: string, noEmit = false): Promise<void> {
    const network = await this.provider.getNetwork()
    const chainId = Number(network.chainId)

    if (!this.allowedNetworks.includes(chainId)) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      EventBus.emit('text-message', `Network unavailable. Please switch to ${this.allowedNetworks.map((n: any) => `${networks[n]}`).join(' or ')}.`)
      return
    }

    console.log(
      'Wallet connected', address, chainId,
    )

    GameState.set('wallet', {
      address: normalizeAddress(address),
      chainId,
    })

    // TODO: Refactor this events emission
    EventBus.emit('wallet-set')
    
    if (noEmit) return

    EventBus.emit('wallet-connected', {
      address,
      chainId,
      provider: this.provider,
    })
  }
  
  public get isConnected(): boolean {
    return !!GameState.getState().wallet.address
  }

  public get hasNfts(): boolean {
    return GameState.getState().nfts?.length > 0
  }

  public async checkIsWhitelisted(address: string): Promise<boolean> {    
    console.log('Checking if address is whitelisted', address)

    const result = await get(`/api/whitelisted/${normalizeAddress(address)}`) as any
    return result.whitelisted ?? false
  }
}