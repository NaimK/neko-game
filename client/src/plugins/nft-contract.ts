import { ethers } from 'ethers'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore

import contractABI from '../contracts-abi/CypherNekoNFT.json'

import EventBus from '~/core/EventBus'
import WalletPlugin from '~/plugins/wallet'
import GameState from '~/store/game'

export default class NftContractPlugin extends Phaser.Plugins.BasePlugin {
  public contractAddress: string = import.meta.env.VITE_NFT_CONTRACT_ADDRESS

  public contract!: ethers.Contract

  private walletPlugin!: WalletPlugin

  // private nfts!: []

  constructor(pluginManager: Phaser.Plugins.PluginManager) {
    super(pluginManager)
    
    this.walletPlugin = pluginManager.get('WalletPlugin') as WalletPlugin

    this.bindListeners()
  }

  private bindListeners(): void {
    EventBus.on(
      'wallet-set', this.initContract, this,
    )
  }

  private async initContract(): Promise<void> {        
    const signer = await this.walletPlugin.provider.getSigner()

    this.contract = new ethers.Contract(
      this.contractAddress,
      contractABI.abi,
      signer,
    )

    // this.mintNft()
    this.listNfts()
  }

  private async listNfts(): Promise<void> {
    // this.nfts = await get('/api/nfts/me')
  }

  public async mintNft(): Promise<string|void> {
    const userWallet = GameState.get('wallet') as { address: string }

    if (!userWallet) {
      return
    }

    const mintPrice = await this.contract.getMintPrice()

    // This is the genesis nft metadata
    // TODO: make this dynamic
    const tx = await this.contract.mint('https://ipfs.moralis.io:2053/ipfs/QmQJy3XxQJ8t37eSjBDKGE5WEYkK219LRifhJ7MfUqDcid/metadata.json',
      {
        gasLimit: ethers.parseUnits('500000', 'wei'),
        value: mintPrice,
      })

    console.log('tx', tx)

    return tx.hash
  }
}