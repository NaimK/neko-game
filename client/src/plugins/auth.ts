import { ethers } from 'ethers'
import { BrowserProvider } from 'ethers/types/providers/provider-browser'

import LocalizationPlugin from './localization'

import { get, post } from '~/core/api'
import EventBus from '~/core/EventBus'
import GameState from '~/store/game'
import { WalletConnectedDTO } from '~/types/events/wallet'
import { User } from '~/types/user'

interface ChallengeResponse {
  id: string
  message: string
  profileId: string
}

interface VerifyResponse {
  id: string
  domain: string
  chainId: number
  address: string
  statement: string
  uri: string
  expirationTime: string
  notBefore: string
  version: string
  nonce: string
  profileId: string
}

export default class AuthPlugin extends Phaser.Plugins.BasePlugin {
  private i18n!: LocalizationPlugin

  constructor(pluginManager: Phaser.Plugins.PluginManager) {
    super(pluginManager)
    this.i18n = pluginManager.get('LocalizationPlugin') as LocalizationPlugin
  }

  // GETTERS

  public get isLoggedIn(): boolean {
    return GameState.get('isLoggedIn') as boolean
  }

  public start(): void {
    this.bindListeners()
    this.checkAuth()
  }

  private bindListeners(): void {
    EventBus.on(
      'wallet-connected', this.handleWalletConnected, this,
    )
    EventBus.on(
      'wallet-disconnected', this.handleWalletDisconnected, this,
    )
    EventBus.on(
      'accounts-changed', this.handleAccountsChanged, this,
    )
  }

  private async handleWalletConnected (data: WalletConnectedDTO): Promise<void> {
    if (!GameState.get('isLoggedIn')) {
      this.authenticate(data)
    }
  }

  public async authenticate(data: WalletConnectedDTO): Promise<void> {
    console.log('authenticate')
    try {
      const challenge = await this.requestChallenge(data)
      const signature = await this.signMessage(challenge.message, data.provider)
      await this.verifySignature(challenge.message, signature)
      await this.checkAuth()
      const user = await this.getUser({ createIfNotExists: true })
      GameState.set('isLoggedIn', true)
      GameState.set('user', user)
      EventBus.emit('user-authenticated')
    } catch (error) {
      if (error !== 'Unauthorized') {
        EventBus.emit('user-rejected-action')
      }

      EventBus.emit(
        'message', 'error', error,
      )
    }
  }

  private async requestChallenge(data: WalletConnectedDTO): Promise<ChallengeResponse> {
    return await post('/api/auth/request-message', data)
  }

  private async signMessage(message: string, provider: BrowserProvider): Promise<string> {
    const signer = await provider.getSigner()
    const encodedMessage = ethers.toUtf8Bytes(message)
    const signature = await signer.signMessage(encodedMessage)

    if (!signature) {
      throw new Error('Unable to sign message.')
    }

    return signature
  }

  private async verifySignature(message: string, signature: string): Promise<VerifyResponse> {
    const response =  await post('/api/auth/verify', {
      message,
      signature,
    })

    if (!response) {
      throw new Error('Unable to verify signature.')
    }

    return response
  }

  private async checkAuth(): Promise<void> {
    try {
      const response = await get<object>('/api/auth/check')
      if (response) {
        GameState.set('isLoggedIn', true)
        const user = await this.getUser()
        GameState.set('user', user)
      }
    }  catch (error) {
      GameState.set('isLoggedIn', false)
    }
  }

  private async getUser(options?: {
    createIfNotExists: boolean
  }): Promise<User|null> {
    const response = await get<User|null>('/api/user', { createIfNotExists: options?.createIfNotExists ?? false })
    
    return response
  }

  private async handleWalletDisconnected() {
    await get('/api/auth/logout')    
    GameState.set('isLoggedIn', false)
  }

  private async handleAccountsChanged(accounts: string[]) {
    console.log('accounts changed', accounts)

    // If the user is logged in, we need to re-authenticate
    if (GameState.get('isLoggedIn')) {
      try {
        await get('/api/auth/logout')
      } catch (error) {
        console.log(error)
      } finally {
        window.location.reload()
      }
    }
  }
}