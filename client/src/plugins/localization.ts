export default class LocalizationPlugin extends Phaser.Plugins.BasePlugin {
  translations: Record<string, string>

  locale!: string

  constructor(pluginManager: Phaser.Plugins.PluginManager) {
    super(pluginManager)
    this.translations = {}
  }

  init(options: { translationFolder: string | URL; defaultLocale: string }) {
    this.locale = options.defaultLocale

    const file = `${options.translationFolder}/${this.locale}.json`
    
    // Load translations from the provided file
    const xhr = new XMLHttpRequest()
    xhr.open(
      'GET', file, false,
    )
    xhr.send()
    if (xhr.status === 200) {
      this.translations = JSON.parse(xhr.responseText)
    } else {
      console.error(`Failed to load translation file: ${file}`)
    }
  }

  t(key: string | number, variables?: object) {
    if (variables) {
      let text = this.translations[key]

      if (!text) {
        return `MISSING TRANSLATION : ${key}`
      }

      Object.entries(variables).forEach(([key, value]) => {
        console.log(key, value)
        text = text.replace(`{{${key}}}`, value)
      })

      return text
    }

    if (!this.translations[key]) {
      return `MISSING TRANSLATION : ${key}`
    }
    
    return this.translations[key]
  }

  setLocale(locale: string) {
    this.locale = locale
  }
}
