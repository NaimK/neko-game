import { UAParser } from 'ua-parser-js'

export default class UserAgentPlugin extends Phaser.Plugins.BasePlugin {
  private _parser!: UAParser
  
  private _userAgent!: UAParser.IResult

  constructor(pluginManager: Phaser.Plugins.PluginManager) {
    super(pluginManager)

    this._parser = new UAParser(navigator.userAgent)
    this._userAgent = this._parser.getResult()
  }

  public get userAgent(): UAParser.IResult | null {
    return this._userAgent
  }

  public get isMobile(): boolean {
    return this._userAgent.device.type === 'mobile'
  }

  public get isSafari(): boolean {
    return this._userAgent.browser.name?.includes('Safari') ?? false
  }

  public get isIOS(): boolean {
    return this._userAgent.os.name === 'iOS'
  }
}