import BaseEnemy from '~/base/enemies/BaseEnemy'
import EnemyStateInterface from '~/base/enemies/interfaces/EnemyStateInterface'
import { PRESPAWN_POSITIONS } from '~/const/positions'
import { ROBOT_RUN_SPEED } from '~/const/velocities'
import EventBus from '~/core/EventBus'
import StateMachine from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'
import GameState from '~/store/game'

export default class RobotState implements EnemyStateInterface {
  public stateMachine: StateMachine

  private sprite!: BaseEnemy

  private scene: Level

  constructor(scene: Level, sprite: BaseEnemy) {
    this.scene = scene
    this.sprite = sprite
    this.stateMachine = new StateMachine(this, 'Robot')
    this.addStates()
  }

  private addStates(): void {
    this.stateMachine.addState('idle', {
      onEnter: this.idleOnEnter,
      onUpdate: this.idleOnUpdate,
    })
      .addState('run-left', { onEnter: this.runLeftOnEnter })
      .addState('run-right',{ onEnter: this.runRightOnEnter })
      .addState('stand-shoot', { onEnter: this.attackLaserOnEnter })
      .addState('hurt', {
        onEnter: this.hurtOnEnter,
        onUpdate: this.hurtOnUpdate,
        onExit: this.hurtOnExit,
      })
      .addState('dead', { onEnter: this.deadOnEnter })
      .setState('idle')
  }

  private idleOnEnter(): void {
    this.sprite.play('robot-idle')
  }

  private idleOnUpdate(): void {
    //
  }

  private runLeftOnEnter(): void{
    this.sprite.play('robot-run')
    this.run('left')
  }

  private runRightOnEnter(): void{
    this.sprite.play('robot-run')
    this.run('right')
  }

  private attackLaserOnEnter(): void {
    this.sprite.setVelocityX(0)
    this.sprite.play('robot-stand-shoot')
    EventBus.emit('robot-shoot-laser', this.sprite)
    
    setTimeout(() => {
      this.stateMachine.setState(this.stateMachine.previousStateName)
    }, 500)
  }

  private hurtOnEnter(): void {
    this.sprite.play('robot-hurt')
    this.sprite.on('animationcomplete', () => {
      this.stateMachine.setState('run-left')
    })
  }
  
  private hurtOnUpdate(): void {
    //
  }

  private hurtOnExit(): void {
    //
  }
  
  private deadOnEnter(): void {
    const { enemiesKilled, score } = GameState.getState()

    GameState.set('enemiesKilled', enemiesKilled + 1)
    GameState.set('score', score + 50)

    EventBus.emit('enemy-killed', this.sprite)
    
    this.sprite.setActive(false)
    this.sprite.setVisible(false)
    this.sprite.body.reset(PRESPAWN_POSITIONS.enemy.x, PRESPAWN_POSITIONS.enemy.y)
  }

  public run(direction: string): void {
    if (direction === 'left') {
      this.sprite.setVelocityX(-ROBOT_RUN_SPEED)
      this.sprite.setFlipX(true)
    } else {
      this.sprite.setVelocityX(ROBOT_RUN_SPEED)
      this.sprite.setFlipX(false)
    }
  }
}