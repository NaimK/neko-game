import BaseEnemy from '~/base/enemies/BaseEnemy'
import { ROBOT_HEALTH } from '~/const/health'
import Level from '~/scenes/levels/Level'

export default class Robot extends BaseEnemy {  
  constructor(
    scene: Level,
    x: number,
    y: number,
  ) {
    super(
      scene, x, y, 'robot', ROBOT_HEALTH,
    )
    this.createAnimations()
  }

  protected createAnimations(): void {
    const world = this.scene.physics.world

    world.scene.anims.create({
      key: 'robot-idle',
      frameRate: 1,
      frames: world.scene.anims.generateFrameNames('robot', {
        start: 1,
        end: 1,
        prefix: 'robot-idle-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'robot-run',
      frameRate: 10,
      frames: world.scene.anims.generateFrameNames('robot', {
        start: 1,
        end: 6,
        prefix: 'robot-run-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'robot-hurt',
      frameRate: 20,
      frames: world.scene.anims.generateFrameNames('robot', {
        start: 1,
        end: 2,
        prefix: 'robot-hurt-',
        suffix: '.png',
      }),
      repeat: 2,
    })

    world.scene.anims.create({
      key: 'robot-stand-shoot',
      frameRate: 1,
      frames: world.scene.anims.generateFrameNames('robot', {
        start: 1,
        end: 1,
        prefix: 'robot-stand-shoot-',
        suffix: '.png',
      }),
      repeat: 0,
    })
  }
}