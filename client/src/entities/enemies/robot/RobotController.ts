
import RobotState from './RobotState'

import BaseEnemy from '~/base/enemies/BaseEnemy'
import BaseEnemyController from '~/base/enemies/BaseEnemyController'
import EnemyControllerInterface from '~/base/enemies/interfaces/EnemyControllerInterface'
import { DELAY_ROBOT_ATTACK_LASER } from '~/const/delays'
import Level from '~/scenes/levels/Level'

export default class RobotController extends BaseEnemyController<RobotState> implements EnemyControllerInterface<RobotState> {
  private lastShotTime = 0
  
  constructor(
    sprite: BaseEnemy, scene: Level, x: number, y: number,
  ) {
    super(
      sprite, scene, x, y, RobotState,
    )
  }

  public update(dt: number): void {
    super.update(dt)

    if (this.isWithinShootingRange() && this.reloadTimeIsOver()) {
      this.state.stateMachine.setState('stand-shoot')
      this.lastShotTime = Date.now()
    }
  }

  private isWithinShootingRange(): boolean {
    if (this.isOnDifferentFloorThanPlayer) return false
    
    if (this.isWithinShootingRangeY) {
      return this.isWithinShootingRangeX
    }

    return false
  }

  private reloadTimeIsOver(): boolean {
    return (this.lastShotTime + DELAY_ROBOT_ATTACK_LASER) < Date.now()
  }
}