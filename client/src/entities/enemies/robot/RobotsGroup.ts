
import Robot from './Robot'
import RobotController from './RobotController'

import BaseEnemiesGroup from '~/base/enemies/BaseEnemiesGroup'

export default class RobotsGroup extends BaseEnemiesGroup<RobotController> {
  quantity = 10
  
  constructor(scene: Phaser.Scene) {
    super(
      scene,  10, 'robot', Robot,
    )
  }

  // add update method
  public update(dt: number): void {
    super.update(dt)
  }
}