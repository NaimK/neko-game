
import RobotController from './RobotController'

import BaseEnemiesGroupController from '~/base/enemies/BaseEnemiesGroupController'
import RobotsGroup from '~/entities/enemies/robot/RobotsGroup'
import Level from '~/scenes/levels/Level'

export default class RobotsGroupController extends BaseEnemiesGroupController<RobotsGroup, RobotController> {
  constructor(scene: Level, robots: RobotsGroup) {
    super(
      scene, robots, RobotController, 'robot',
    )
  }
}