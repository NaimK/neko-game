
import YellowSuit from './YellowSuit'
import YellowSuitController from './YellowSuitController'

import BaseEnemiesGroup from '~/base/enemies/BaseEnemiesGroup'

export default class YellowSuitsGroup extends BaseEnemiesGroup<YellowSuitController> {
  quantity = 10
  
  constructor(scene: Phaser.Scene) {
    super(
      scene,  10, 'yellow-suit', YellowSuit,
    )
  }

  // add update method
  public update(dt: number): void {
    super.update(dt)
  }
}