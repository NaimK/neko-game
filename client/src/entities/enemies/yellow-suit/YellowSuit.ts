import BaseEnemy from '~/base/enemies/BaseEnemy'
import { YELLOW_SUIT_HEALTH } from '~/const/health'
import Level from '~/scenes/levels/Level'

export default class YellowSuit extends BaseEnemy {  
  constructor(
    scene: Level,
    x: number,
    y: number,
  ) {
    super(
      scene, x, y, 'yellow-suit', YELLOW_SUIT_HEALTH,
    )
    this.createAnimations()
  }

  protected createAnimations(): void {
    const world = this.scene.physics.world

    world.scene.anims.create({
      key: 'yellow-suit-idle',
      frameRate: 1,
      frames: world.scene.anims.generateFrameNames('yellow-suit', {
        start: 1,
        end: 1,
        prefix: 'yellow-suit-idle-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'yellow-suit-run',
      frameRate: 10,
      frames: world.scene.anims.generateFrameNames('yellow-suit', {
        start: 1,
        end: 6,
        prefix: 'yellow-suit-run-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'yellow-suit-hurt',
      frameRate: 20,
      frames: world.scene.anims.generateFrameNames('yellow-suit', {
        start: 1,
        end: 2,
        prefix: 'yellow-suit-hurt-',
        suffix: '.png',
      }),
      repeat: 1,
    })

    world.scene.anims.create({
      key: 'yellow-suit-stand-shoot',
      frameRate: 1,
      frames: world.scene.anims.generateFrameNames('yellow-suit', {
        start: 1,
        end: 1,
        prefix: 'yellow-suit-stand-shoot-',
        suffix: '.png',
      }),
      repeat: 0,
    })
  }
}