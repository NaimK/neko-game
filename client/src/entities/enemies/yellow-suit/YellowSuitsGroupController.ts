
import YellowSuitController from './YellowSuitController'

import BaseEnemiesGroupController from '~/base/enemies/BaseEnemiesGroupController'
import YellowSuitsGroup from '~/entities/enemies/yellow-suit/YellowSuitsGroup'
import Level from '~/scenes/levels/Level'

export default class YellowSuitsGroupController extends BaseEnemiesGroupController<YellowSuitsGroup, YellowSuitController> {
  constructor(scene: Level, yellowSuits: YellowSuitsGroup) {
    super(
      scene, yellowSuits, YellowSuitController, 'yellow-suit',
    )
  }
}