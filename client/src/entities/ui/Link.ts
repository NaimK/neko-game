export default class Link {
  scene!: Phaser.Scene

  linkElement!: HTMLAnchorElement

  url!: string

  text!: string

  x!: number

  y!: number

  linkSprite!: Phaser.GameObjects.DOMElement

  constructor(config: {
    scene: Phaser.Scene,
    x: number,
    y: number,
    text: string,
    url: string,
  }) {
    const { scene, url, text, x, y } = config

    this.scene = scene
    this.url = url
    this.text = text
    this.x = x
    this.y = y

    this.setLinkElement()
    this.setLinkSprite()
    this.bindListeners()
  }

  private setLinkElement(): void {
    this.linkElement = document.createElement('a')

    this.linkElement.href =  this.url
    this.linkElement.target =  '_blank'
    this.linkElement.innerText = this.text
    this.linkElement.style.color = '#fff'
    this.linkElement.style.textDecoration = 'underline'
    this.linkElement.style.fontFamily = 'Courier New'
    this.linkElement.style.fontSize = '10px'
    this.linkElement.style.fontWeight = 'bold'
  }

  private setLinkSprite(): void {
    const { x, y } = this

    console.log(x, y)

    this.linkSprite = this.scene.add.dom(
      x, y, this.linkElement,
    )
      .setOrigin(0.5, 1)
  }

  private bindListeners(): void {
    this.linkSprite.addListener('click')
      .on('click', () => {
        window.open(this.linkElement.href, '_blank')
      })
  }

}