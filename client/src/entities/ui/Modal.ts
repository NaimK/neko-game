import { MODALS_DEPTH } from '~/const/depths'
import { FONT_MAIN, FONT_SIZE_SMALL } from '~/const/fonts'
import NineSlice from '~/plugins/nineslice/NineSlice'
import NineSlicePlugin from '~/plugins/nineslice/NineSlicePlugin'

export default class Modal {
  scene!: Phaser.Scene

  timer!: Phaser.Time.TimerEvent
  
  text!: string

  label!: Phaser.GameObjects.BitmapText

  background!: NineSlice

  callback?: () => void

  constructor(config: {
    scene: Phaser.Scene,
    x: number,
    y: number,
    width: number,
    height: number,
    text: string,
    center?: boolean,
    centerLabel?: boolean,
    type?: 'default' | 'alt',
    hideDelay?: number,
    typewriter?: boolean,
    callback?: () => void,
  }) {
    const { scene, x, y, width, height, text, center, centerLabel, type = 'default', hideDelay = 3000, typewriter = true, callback } = config

    this.scene = scene
    this.text = text
  
    const nineslice =  this.scene.plugins.get('NineSlicePlugin') as NineSlicePlugin

    this.background = nineslice.add(this.scene,
      [
        x,
        y,
        width,
        height,
        {
          key: 'ui-modals',
          frame: type === 'default' ? 'modal-1.png' : 'modal-2.png',
        },
        16,
        16,
      ])
      .setScrollFactor(0)
      .setDepth(MODALS_DEPTH)
    
    this.label = scene.add.bitmapText(
      x, y, FONT_MAIN, '', FONT_SIZE_SMALL,
    )
      // set line height to 1.5
      .setMaxWidth(width - 15)
      .setScrollFactor(0)
      .setDepth(MODALS_DEPTH + 1)

    if (center) {
      this.background.setOrigin(0.5, 0.5)
    }
    if (centerLabel) {
      this.label.setOrigin(0.5, 0.5)
    }

    if (typewriter) {
      this.typewriteBitmapText()
    } else {
      this.label.setText(this.text)
    }

    this.scene.time.addEvent({
      callback: () => {
        this.scene.tweens.add({
          targets: [this.background, this.label],
          alpha: 0,
          duration: 1000,
          ease: 'Power1',
          onComplete: () => {
            this.destroy()
          },
        })
        if (this.callback) {
          this.callback()
        }
      },
      delay: hideDelay,
    })

    return this
  }

  private typewriteBitmapText(): void {
    this.label.setText(this.text)

    const bounds = this.label.getTextBounds(false)
    const wrappedText = bounds['wrappedText'] || this.text

    this.label.setText('')

    const length = wrappedText.length
    let i = 0

    this.timer = this.scene.time.addEvent({
      callback: () => {
        this.label.text += wrappedText[i]
        ++i
      },
      repeat: length - 1,
      delay: 60,
    })
  }

  public destroy(): void {
    this.background.destroy()
    this.label.destroy()
    this.timer?.destroy() 
  }
}
