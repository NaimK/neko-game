export default class Neko extends Phaser.Physics.Arcade.Sprite {
  constructor(
    scene: Phaser.Scene, x: number, y: number,
  ) {
    super(
      scene, x,y , 'neko',
    )
    this.createAnimations()
  }

  private createAnimations(): void {
    this.anims.create({
      key: 'player-idle',
      frameRate: 10,
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-idle-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.anims.create({
      key: 'player-idle-no-weapon',
      frameRate: 10,
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-idle-no-weapon-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.anims.create({
      key: 'player-talk-no-weapon',
      frameRate: 10,
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-talk-no-weapon-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.anims.create({
      key: 'player-walk',
      frameRate: 10,
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 4,
        prefix: 'neko-walking-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.anims.create({
      key: 'player-run',
      frameRate: 10,
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 4,
        prefix: 'neko-running-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.anims.create({
      key: 'player-jump',
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-jumping-',
        suffix: '.png',
      }),
      frameRate: 10,
    })

    this.anims.create({
      key: 'player-double-jump',
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-double-jump-',
        suffix: '.png',
      }),
      frameRate: 10,
    })

    this.anims.create({
      key: 'player-crounch',
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 2,
        prefix: 'neko-crounch-',
        suffix: '.png',
      }),
      frameRate: 10,
    })

    this.anims.create({
      key: 'player-jump-shoot-laser',
      frames: this.anims.generateFrameNames('player', {
        start: 1,
        end: 1,
        prefix: 'neko-idle-',
        suffix: '.png',
      }),
      frameRate: 10,
    })
  }
}