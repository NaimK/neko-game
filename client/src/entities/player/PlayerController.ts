import debounce from 'lodash/debounce'

import PlayerState from './PlayerState'

import { PLAYER_DEPTH } from '~/const/depths'
import { GAME_HEIGHT } from '~/const/sizes'
import EventBus from '~/core/EventBus'
import { BindedKeys } from '~/core/keyBindings'
import StateMachine from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'
import GameState from '~/store/game'

export default class PlayerController {
  public scene: Level

  private sprite!: Phaser.Physics.Arcade.Sprite

  private keys: BindedKeys

  private playerState!: PlayerState

  public direction: 'left'|'right' = 'right'

  public get state (): StateMachine {
    return this.playerState.stateMachine
  }

  private get isJumping(): boolean {
    const jumpStates = ['jump', 'double-jump', 'fall']
    return jumpStates.includes(this.playerState.stateMachine.currentState?.name ?? '')
  }

  constructor(scene: Level, keys: BindedKeys) {
    this.scene = scene
    this.keys = keys
    this.init()
  }

  public init(): void {
    this.setPlayer()
    this.createAnimations()
    this.playerState = new PlayerState(
      this, this.sprite, this.keys,
    )
    this.bindEvents()
  }

  private setPlayer(): void {
    const playerLayer = this.scene.map.getObjectLayer('Objects/player-spawn')
    
    let x: number
    let y: number
    let width: number

    if (import.meta.env.VITE_PLAYER_SPAWN) {
      [x, y] = import.meta.env.VITE_PLAYER_SPAWN.split(',').map((n: number) => n * 16)
      width = 32
    }  else {
      x = playerLayer.objects[0].x ?? 0
      y = playerLayer.objects[0].y ?? 0
      width = playerLayer.objects[0].width ?? 32
    }

    this.sprite = this.scene.physics.add.sprite(
      x + (width * 0.5), y, 'player',
    )
    this.sprite.setSize(24, 34).setDepth(PLAYER_DEPTH)
    this.sprite.setImmovable(true)
  }

  public getPlayer(): Phaser.Physics.Arcade.Sprite {
    return this.sprite
  }

  private createAnimations(): void {
    this.sprite.anims.create({
      key: 'player-idle',
      frameRate: 10,
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-idle-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.sprite.anims.create({
      key: 'player-walk',
      frameRate: 10,
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 4,
        prefix: 'neko-walking-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.sprite.anims.create({
      key: 'player-run',
      frameRate: 10,
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 4,
        prefix: 'neko-running-',
        suffix: '.png',
      }),
      repeat: -1,
    })

    this.sprite.anims.create({
      key: 'player-jump',
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-jumping-',
        suffix: '.png',
      }),
      frameRate: 10,
    })

    this.sprite.anims.create({
      key: 'player-double-jump',
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 6,
        prefix: 'neko-double-jump-',
        suffix: '.png',
      }),
      frameRate: 10,
    })

    this.sprite.anims.create({
      key: 'player-crounch',
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 2,
        prefix: 'neko-crounch-',
        suffix: '.png',
      }),
      frameRate: 10,
    })

    this.sprite.anims.create({
      key: 'player-jump-shoot-laser',
      frames: this.sprite.anims.generateFrameNames('player', {
        start: 1,
        end: 1,
        prefix: 'neko-idle-',
        suffix: '.png',
      }),
      frameRate: 10,
    })
  }

  private bindEvents(): void {
    EventBus.on(
      'player-enemy-collision', () => {
        this.debouncedHandlePlayerEnemyCollision()
      }, this,
    )

    EventBus.on(
      'game-state-changed', (data: { property: string, value: any}) => {
        if (data.property === 'playerHealth' && data.value <= 0) {
          this.playerState.stateMachine.setState('dead')
        }
      }, this,
    )
  }

  public onPlayerCollideWithGround(): void {
    this.scene.sound.play('jump-land')
    this.playerState.stateMachine.setState('idle')
  }

  public debouncedOnPlayerCollideWithGround = debounce(
    this.onPlayerCollideWithGround, 100, {
      leading: true,
      trailing: false,
    },
  )

  public onPlayerCollideWithLava(): void {
    this.takeDamage()
    this.state.setState('jump')
  }

  public onPlayerCollideWithPlatform(): void {
    if (this.isJumping) {
      this.scene.sound.play('jump-land')
      this.playerState.stateMachine.setState('idle')
    }
  }

  public onPlayerCollideWithBridge(): void {
    this.playerState.stateMachine.setState('walk')
  }

  private debouncedHandlePlayerEnemyCollision = debounce(
    this.handlePlayerEnemyCollision, 1000, {
      leading: true,
      trailing: false, 
    },
  )

  private handlePlayerEnemyCollision(): void {
    this.playerState.stateMachine.setState('collide-with-enemy')
  }

  public takeDamage(): void {
    this.playerState.stateMachine.setState('hit')
  }

  public collectCoin(): void {
    GameState.set('coins', GameState.getState().coins + 1)
  }

  public update(dt: number): void {
    if (this.sprite.y > GAME_HEIGHT) {
      const { floor } = GameState.getState()

      if (floor !== 'underground') {
        GameState.set('floor', 'underground')
        EventBus.emit('floor-changed', { floor: 'underground' })
      }
    }

    this.playerState.stateMachine.update(dt)
  }
}
