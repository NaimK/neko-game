import MergedInput, { Player } from 'phaser3-merged-input'

import { PLAYER_HEALTH_DEFAULT } from '~/const/health'
import { PLAYER_JUMP_SPEED, PLAYER_WALK_SPEED } from '~/const/velocities'
import EventBus from '~/core/EventBus'
import { BindedKeys } from '~/core/keyBindings'
import StateMachine from '~/core/StateMachine'
import PlayerController from '~/entities/player/PlayerController'
import Input from '~/scenes/MainInput'
import GameState from '~/store/game'

export default class PlayerState {
  private controller: PlayerController

  private sprite: Phaser.Physics.Arcade.Sprite

  public stateMachine: StateMachine
  
  private keys: BindedKeys

  private mainInput!: Input

  constructor(
    controller: PlayerController, 
    sprite: Phaser.Physics.Arcade.Sprite,
    keys: BindedKeys,
  ) {
    this.controller = controller
    this.sprite = sprite
    this.keys = keys
    this.mainInput = controller.scene.mainInput
    this.stateMachine = new StateMachine(this, 'player')
    this.addStates()
  }

  private addStates(): void {
    this.stateMachine
      .addState('idle', {
        onEnter: this.idleOnEnter,
        onUpdate: this.idleOnUpdate,
      })
      .addState('fall', {
        onEnter: this.fallOnEnter,
        onUpdate: this.fallOnUpdate,
      })
      .addState('walk', {
        onEnter: this.walkOnEnter,
        onUpdate: this.walkOnUpdate,
        onExit: this.walkOnExit,
      })
      .addState('jump', {
        onEnter: this.jumpOnEnter,
        onUpdate: this.jumpOnUpdate,
        onExit: this.jumpOnExit,
      })
      .addState('double-jump', {
        onEnter: this.doubleJumpOnEnter,
        onUpdate: this.doubleJumpOnUpdate,
      })
      .addState('crounch', {
        onEnter: this.crounchOnEnter,
        onUpdate: this.crounchOnUpdate,
      })
      .addState('shoot-laser', {
        onEnter: this.shootLaserOnEnter,
        onUpdate: this.shootLaserOnUpdate,
      })
      .addState('collide-with-enemy', { onEnter: this.collideWithEnemyOnEnter })
      .addState('hit', { onEnter: this.hitOnEnter })
      .addState('dead', {
        onEnter: this.deadOnEnter,
        onUpdate: this.deadOnUpdate,
        onExit: this.deadOnExit,
      })
      .setState('idle')
  }

  private idleOnEnter(): void {
    this.sprite.play('player-idle')
    this.sprite.setVelocityX(0)
  }

  private idleOnUpdate(): void {
    if ((this.mainInput.isGoingLeft || this.mainInput.isGoingRight) && !this.mainInput.justJumped) {
      this.stateMachine.setState('walk')
    }

    if (this.mainInput.justJumped) {
      this.stateMachine.setState('jump')
    }

    if (this.mainInput.isGoingDown) {
      this.stateMachine.setState('crounch')
    }

    if (this.mainInput.justFired) {
      this.setShootLaserState()
    }
  }

  private fallOnEnter(): void {
    this.sprite.play('player-idle')
  }

  private fallOnUpdate(): void {
    if (this.mainInput.isGoingLeft) {
      this.go('left')
    } else if (this.mainInput.isGoingRight) {
      this.go('right')
    } else {
      this.sprite.setVelocityX(0)
    }

    if (this.mainInput.justFired) {
      EventBus.emit('player-shoot-laser')
      this.sprite.play('player-jump-shoot-laser')
      this.sprite.play('player-jump')
    }
  }

  private walkOnEnter(): void{
    this.sprite.play('player-walk')
  }

  private walkOnUpdate(): void {
    if (this.mainInput.isGoingLeft) {
      this.go('left')
    } else if (this.mainInput.isGoingRight) {
      this.go('right')
    } else {
      this.stateMachine.setState('idle')
    }

    if (this.mainInput.justJumped) {
      this.stateMachine.setState('jump')
    } else if (this.mainInput.justFired) {
      console.log('fire')
      this.setShootLaserState()
    }
  }

  private walkOnExit(): void {
    this.sprite.stop()
  }

  private jumpOnEnter(): void {
    this.sprite.play('player-jump')
    this.sprite.setVelocityY(PLAYER_JUMP_SPEED)
    this.controller.scene.sound.play('jump')
  }

  private jumpOnUpdate(): void {
    // if jump key is released, stop jump
    if (!this.mainInput.isJumping) {
      this.sprite.setAccelerationY(300)
    }

    // Handle double jump
    if (this.mainInput.justJumped) {
      this.stateMachine.setState('double-jump')
    }

    if (this.mainInput.isGoingLeft) {
      this.go('left')
    } else if (this.mainInput.isGoingRight) {
      this.go('right')
    } else {
      this.sprite.setVelocityX(0)
    }

    if (this.mainInput.justFired) {
      EventBus.emit('player-shoot-laser')
      this.sprite.play('player-jump-shoot-laser')
      this.sprite.play('player-jump')
    }
  }

  private jumpOnExit(): void {
    this.sprite.setAccelerationY(0)
  }

  private doubleJumpOnEnter(): void {
    this.sprite.play('player-double-jump')
    this.sprite.setVelocityY(PLAYER_JUMP_SPEED)
    this.controller.scene.sound.play('jump')
  }

  private doubleJumpOnUpdate(): void {
    // if jump key is released, stop jump
    if (!this.mainInput.isJumping) {
      this.sprite.setAccelerationY(300)
    }

    if (this.mainInput.isGoingLeft) {
      this.go('left')
    } else if (this.mainInput.isGoingRight) {
      this.go('right')
    } else {
      this.sprite.setVelocityX(0)
    }

    // Handle double jump animation completed
    if (this.sprite.anims.currentAnim?.key === 'player-double-jump' && this.sprite.anims.currentFrame?.index === 6) {
      this.stateMachine.setState('fall')
    }
  }

  private crounchOnEnter(): void {
    this.sprite.play('player-crounch')
  }

  private crounchOnUpdate(): void {
    if (!this.mainInput.isGoingDown) {
      this.stateMachine.setState('idle')
    }

    if (this.mainInput.justFired) {
      this.setShootLaserState()
    }
  }

  private shootLaserOnEnter(): void {
    //
  }

  private shootLaserOnUpdate(): void {
    if (this.mainInput.isGoingDown) {
      this.stateMachine.setState('crounch')
    } else {
      this.stateMachine.setState('idle')
    }
  }

  private collideWithEnemyOnEnter(): void {
    const currentStateName = this.stateMachine.currentState?.name ?? ''

    if (['hit', 'dead'].includes(currentStateName)) return

    this.handlePlayerHit()
  }

  private hitOnEnter(): void {
    this.handlePlayerHit()
  }

  private handlePlayerHit(): void {
    const { playerHealth } = GameState.getState()

    GameState.set('playerHealth', playerHealth - 10)

    this.animateHit()
  }

  private deadOnEnter(): void {
    const { lives } = GameState.getState()

    GameState.set('lives', lives - 1)

    this.sprite.setVelocityX(0)
    this.sprite.setVelocityY(0)

    const angle = this.controller.direction === 'left' ? 90 : -90

    this.sprite.setAngle(angle)
  }

  private deadOnUpdate(): void {    
    if (this.sprite.alpha <= 0) {
      this.stateMachine.setState('idle')
      return
    }

    this.sprite.alpha -= 0.02
    this.sprite.y -= 5
  }

  private deadOnExit(): void {
    const { lives, lastCheckpoint } = GameState.getState()
    
    if (lives <= 0) {
      EventBus.emit('game-over')
    } else {
      GameState.set('playerHealth', PLAYER_HEALTH_DEFAULT)
      this.sprite.alpha = 1
      this.sprite.setAngle(0)
      this.sprite.setPosition(lastCheckpoint.x ?? 0, (lastCheckpoint.y ?? 0) - 20)
      EventBus.emit('player-respawned')
    }
  }

  private setShootLaserState(): void {
    EventBus.emit('player-shoot-laser')
    this.stateMachine.setState('shoot-laser')
  }

  private go(direction: 'left'|'right'): void {
    this.controller.direction = direction

    if (direction === 'left') {
      this.sprite.flipX = true
      this.sprite.setVelocityX(-PLAYER_WALK_SPEED)
    } else if (direction === 'right') {
      this.sprite.flipX = false
      this.sprite.setVelocityX(PLAYER_WALK_SPEED)
    }
  }

  private animateHit(): void {
    const startColor = Phaser.Display.Color.ValueToColor(0xffffff)
    const endColor = Phaser.Display.Color.ValueToColor(0xff0000)
    
    this.controller.scene.tweens.addCounter({
      from: 0,
      to: 100,
      duration: 100,
      repeat: 2,
      yoyo: true,
      ease: Phaser.Math.Easing.Sine.InOut,
      onUpdate: (tween: any) => {
        const value = tween.getValue()
        const colorObject = Phaser.Display.Color.Interpolate.ColorWithColor(
          startColor,
          endColor,
          100,
          value,
        )

        const color = Phaser.Display.Color.GetColor(
          colorObject.r,
          colorObject.g,
          colorObject.b,
        )

        this.sprite.setTint(color)

        if (tween.getValue() === 100) {
          if (!this.stateMachine.isCurrentState('dead')) {
            if (['hit', 'collide-with-enemy'].includes(this.stateMachine.currentState?.name ?? '')) {
              this.stateMachine.setState('idle')
            }
          }
        }
      },
    })
  }
}