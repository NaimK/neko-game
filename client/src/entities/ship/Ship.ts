export default class Ship extends Phaser.Physics.Arcade.Sprite {
  constructor(
    scene: Phaser.Scene, x: number, y: number,
  ) {
    super(
      scene, x,y , 'ship',
    )
    this.createAnimations()
  }

  private createAnimations(): void {
    this.anims.create({
      key: 'ship-fly',
      frameRate: 4,
      frames: this.anims.generateFrameNames('ship', {
        start: 1,
        end: 3,
        prefix: 'ship-fly-',
        suffix: '.png',
      }),
      yoyo: true,
      repeat: -1,
    })
  }
}