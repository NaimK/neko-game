export default class ShipThrust extends Phaser.Physics.Arcade.Sprite {
  constructor(
    scene: Phaser.Scene, x: number, y: number,
  ) {
    super(
      scene, x,y , 'ship-thrust',
    )
    this.createAnimations()
  }

  private createAnimations(): void {
    this.anims.create({
      key: 'ship-thrust',
      frameRate: 5,
      frames: this.anims.generateFrameNames('ship-thrust', {
        start: 1,
        end: 2,
        prefix: 'ship-thrust-',
        suffix: '.png',
      }),
      yoyo: true,
      repeat: -1,
    })
  }
}