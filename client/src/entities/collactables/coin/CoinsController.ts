
import Coin from './Coin'

import BaseEnemy from '~/base/enemies/BaseEnemy'
import EventBus from '~/core/EventBus'
import Level from '~/scenes/levels/Level'

export default class CoinsController {
  private scene: Level

  constructor(scene: Level) {
    this.scene = scene
    this.bindListeners()
  }

  private bindListeners(): void {
    EventBus.on('enemy-killed', this.handleEnemyKilled)
  }

  private handleEnemyKilled = (enemy: BaseEnemy): void => {
    this.spawnCoin(enemy.x, enemy.y)
  }

  private spawnCoin(x: number, y: number): void {
    new Coin(
      this.scene, x, y + 15,
    )
  }
}