import Level from '~/scenes/levels/Level'

export default class Coin extends Phaser.Physics.Arcade.Sprite {
  constructor(
    scene: Level, x: number, y: number,
  ) {
    super(
      scene, x, y, 'coin',
    )    

    this.createAnimations()
    this.setCollisions()

    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
    this.setVelocityX(-10)
    this.setVelocityY(-30)
    this.anims.play('coin-rotate')

    this.setDisappearing()
  }

  private createAnimations(): void {
    this.anims.create({
      key: 'coin-rotate',
      frameRate: 12,
      frames: this.anims.generateFrameNames('coin', {
        start: 0,
        end: 7,
      }),
      repeat: -1,
    })
  }

  private setCollisions(): void {
    this.scene.physics.add.collider(
      this, (this.scene as Level).player, this.collect, undefined, this,
    )
    this.scene.physics.add.collider(
      this, (this.scene as Level).ground,
      this.handleCoinCollidesWithGround, undefined, this,
    )
  }

  private setDisappearing(): void {
    this.scene.time.delayedCall(3000, () => {
      this.scene?.tweens.add({
        targets: this,
        alpha: 0.2,
        duration: 100,
        ease: 'Linear',
        yoyo: true,
        repeat: -1,
      })
    })
    setTimeout(() => {
      this.destroy()
    }, 5000)
  }

  private handleCoinCollidesWithGround(): void {
    this.setVelocityY(-100)
  }

  private collect(): void {
    (this.scene as Level).playerController.collectCoin()
    this.scene.sound.play('coin-pickup')
    this.destroy()
  }
}