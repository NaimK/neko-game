import { ROBOT_BULLET_SPEED } from '@const/velocities'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import { ROBOT_BULLET_DAMAGE } from '~/const/damages'

export default class RobotBullet extends BaseWeapon {    
  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
  ) {
    super(
      scene, x, y, 'robot-bullet', ROBOT_BULLET_DAMAGE, ROBOT_BULLET_SPEED,
    )
    this.createAnimations()
  }

  private createAnimations(): void {    
    const world = this.scene.physics.world
    
    world.scene.anims.create({
      key: 'robot-bullet-shoot',
      frameRate: 10,
      frames: world.scene.anims.generateFrameNames('robot-laser', {
        start: 1,
        end: 5,
        prefix: 'laser-shoot-',
        suffix: '.png', 
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'robot-bullet-explode',
      frameRate: 30,
      frames: world.scene.anims.generateFrameNames('robot-laser', {
        start: 1,
        end: 5,
        prefix: 'laser-explode-',
        suffix: '.png',
      }),
      repeat: 0,
    })
  }

  fire(options: { x: number, y: number, direction?: 'left'|'right' }) {    
    const { x, y, direction } = options

    this.setDefaults(x, y)
    this.anims.play('robot-bullet-shoot')
    this.sounds.shoot.play()
    
    if (direction === 'left') {
      this.setFlipX(true)
    } else {
      this.setFlipX(false)
    }
    this.setVelocityX(direction === 'left' ? -ROBOT_BULLET_SPEED : ROBOT_BULLET_SPEED)
  }

  public explode(): void {
    this.setVelocityX(0)
    this.play('robot-bullet-explode')
    this.sounds.explode.play()
  }
}