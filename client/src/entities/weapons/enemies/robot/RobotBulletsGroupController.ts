
import RobotBulletController from './RobotBulletController'
import RobotBulletState from './RobotBulletState'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import BaseWeaponsGroupController from '~/base/weapons/BaseWeaponsGroupController'
import RobotBulletsGroup from '~/entities/weapons/enemies/robot/RobotBulletsGroup'
import Level from '~/scenes/levels/Level'

export default class RobotBulletsGroupController extends BaseWeaponsGroupController<RobotBulletsGroup, RobotBulletController> {    
  constructor(scene: Level) {
    super(scene, new RobotBulletsGroup(scene))
  }

  public fireRobotBullet(
    controllerClass: new(sprite: BaseWeapon, scene: Level, x?:number, y?:number) => RobotBulletController,
    x: number,
    y: number,
  ): void {  
    this.weapons.fireWeapon(
      controllerClass, RobotBulletState, x, y,
    )
  }
}