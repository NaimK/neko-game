import debounce from 'lodash/debounce'

import RobotBulletState from './RobotBulletState'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import BaseWeaponController from '~/base/weapons/BaseWeaponController'
import WeaponControllerInterface from '~/base/weapons/interfaces/WeaponControllerInterface'
import { StateInterface } from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default class RobotBulletController extends BaseWeaponController<RobotBulletState>  implements WeaponControllerInterface<RobotBulletState>  {
  constructor(
    sprite: BaseWeapon, scene: Level, x?:number, y?:number,
  ) {
    super(
      sprite, scene, RobotBulletState, x, y,
    )
    this.setCustomCollisions()
  }

  // GETTERS

  public get currentState(): StateInterface | undefined {
    return this.weaponState.stateMachine.currentState
  }
  
  private setCustomCollisions(): void {
    this.scene.physics.add.collider(
      this.sprite, this.scene.player,
      debounce(
        () => {
          this.weaponState.stateMachine.setState('explode')
          this.scene.playerController.takeDamage()
        }, 250, {
          leading: true,
          trailing: false,
        },
      ),
    )
  }
}