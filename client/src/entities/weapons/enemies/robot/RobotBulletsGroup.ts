
import RobotBullet from './RobotBullet'
import RobotBulletController from './RobotBulletController'
import RobotBulletState from './RobotBulletState'

import BaseWeaponsGroup from '~/base/weapons/BaseWeaponsGroup'

export default class RobotBulletsGroup extends BaseWeaponsGroup<RobotBulletController, RobotBulletState> {
  controllers: RobotBulletController[] = []

  quantity = 10
  
  constructor(scene: Phaser.Scene) {
    super(
      scene, 10, 'RobotBullet', RobotBullet,
    )
  }
}