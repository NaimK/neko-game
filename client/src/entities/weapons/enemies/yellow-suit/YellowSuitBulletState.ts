import BaseWeapon from '~/base/weapons/BaseWeapon'
import WeaponStateInterface from '~/base/weapons/interfaces/WeaponStateInterface'
import { PRESPAWN_POSITIONS } from '~/const/positions'
import StateMachine from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default class YellowSuitBulletState implements WeaponStateInterface {
  public stateMachine: StateMachine

  private sprite!: BaseWeapon

  private scene: Level

  private startX?: number

  private startY?: number

  constructor(
    scene: Level, sprite: BaseWeapon, startX?: number, startY?: number,
  ) {
    this.scene = scene
    this.sprite = sprite
    this.startX = startX
    this.startY = startY
    this.stateMachine = new StateMachine(this, 'yellow-suit-bullet')
    this.addStates()
  }

  private addStates(): void {
    this.stateMachine.addState('shoot', {
      onEnter: this.shootOnEnter,
      onUpdate: this.shootOnUpdate,
    })
      .addState('explode', { onEnter: this.explodeOnEnter })
      .addState('inactive', { onEnter: this.inactiveOnEnter })
  }

  private shootOnEnter(): void {
    this.handlePlayerShootYellowSuitBullet()
  }
  
  private shootOnUpdate(): void {
    if (!this.scene.cameras.main.worldView.contains(this.sprite.x, this.sprite.y)) {
      this.stateMachine.setState('inactive')
    }
  }

  private explodeOnEnter(): void {
    this.sprite.explode()
    this.sprite.on('animationcomplete', () => {
      this.stateMachine.setState('inactive')
    })
  }

  private inactiveOnEnter(): void {
    this.sprite.setActive(false)
    this.sprite.setVisible(false)
    this.sprite.body.reset(PRESPAWN_POSITIONS.laser.x,PRESPAWN_POSITIONS.laser.y)
  }

  private handlePlayerShootYellowSuitBullet(): void {
    let direction: 'left'|'right' = 'left'
    const { width } = this.sprite
    const { startX, startY } = this
    const playerPosition = this.scene.player.body.position.x
    const bulletPosition = startX

    if (!startX || !startY) {
      console.warn('startX or startY is undefined')
      return
    }

    if (playerPosition > bulletPosition!) {
      direction = 'right'
    }

    this.sprite.fire({
      x: direction === 'left' ? startX : startX + width / 2,
      y: startY + 20,
      direction: direction,
    })
  }
}