
import YellowSuitBullet from './YellowSuitBullet'
import YellowSuitBulletController from './YellowSuitBulletController'
import YellowSuitBulletState from './YellowSuitBulletState'

import BaseWeaponsGroup from '~/base/weapons/BaseWeaponsGroup'

export default class YellowSuitBulletsGroup extends BaseWeaponsGroup<YellowSuitBulletController, YellowSuitBulletState> {
  controllers: YellowSuitBulletController[] = []

  quantity = 10
  
  constructor(scene: Phaser.Scene) {
    super(
      scene, 10, 'YellowSuitBullet', YellowSuitBullet,
    )
  }
}