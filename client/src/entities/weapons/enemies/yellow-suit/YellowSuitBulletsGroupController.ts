import YellowSuitBulletController from './YellowSuitBulletController'
import YellowSuitBulletState from './YellowSuitBulletState'
import YellowSuitBulletsGroup from './YellowSuitsBulletsGroup'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import BaseWeaponsGroupController from '~/base/weapons/BaseWeaponsGroupController'
import Level from '~/scenes/levels/Level'

export default class YellowSuitBulletsGroupController extends BaseWeaponsGroupController<YellowSuitBulletsGroup, YellowSuitBulletController> {    
  constructor(scene: Level) {
    super(scene, new YellowSuitBulletsGroup(scene))
  }

  public fireYellowSuitBullet(
    controllerClass: new(sprite: BaseWeapon, scene: Level, x?:number, y?:number) => YellowSuitBulletController,
    x: number,
    y: number,
  ): void {  
    this.weapons.fireWeapon(
      controllerClass, YellowSuitBulletState, x, y,
    )
  }
}