import { YELLOW_SUIT_BULLET_SPEED } from '@const/velocities'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import { YELLOW_SUIT_BULLET_DAMAGE } from '~/const/damages'

export default class YellowSuitBullet extends BaseWeapon {    
  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
  ) {
    super(
      scene, x, y, 'yellow-suit-bullet', YELLOW_SUIT_BULLET_DAMAGE, YELLOW_SUIT_BULLET_SPEED,
    )
    this.createAnimations()
  }

  private createAnimations(): void {    
    const world = this.scene.physics.world
    
    world.scene.anims.create({
      key: 'yellow-suit-bullet-shoot',
      frameRate: 10,
      frames: world.scene.anims.generateFrameNames('yellow-suit-laser', {
        start: 1,
        end: 4,
        prefix: 'laser-shoot-',
        suffix: '.png', 
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'yellow-suit-bullet-explode',
      frameRate: 30,
      frames: world.scene.anims.generateFrameNames('yellow-suit-laser', {
        start: 1,
        end: 4,
        prefix: 'laser-explode-',
        suffix: '.png',
      }),
      repeat: 0,
    })
  }

  fire(options: { x: number, y: number, direction?: 'left'|'right' }) {    
    const { x, y, direction } = options

    this.setDefaults(
      x, y, 1,
    )
    this.anims.play('yellow-suit-bullet-shoot')
    this.sounds.shoot.play()
    
    if (direction === 'left') {
      this.setFlipX(true)
    } else {
      this.setFlipX(false)
    }
    this.setVelocityX(direction === 'left' ? -YELLOW_SUIT_BULLET_SPEED : YELLOW_SUIT_BULLET_SPEED)
  }

  public explode(): void {
    this.setVelocityX(0)
    this.play('yellow-suit-bullet-explode')
    this.sounds.explode.play()
  }
}