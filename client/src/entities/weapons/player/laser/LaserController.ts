
import LaserState from './LaserState'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import BaseWeaponController from '~/base/weapons/BaseWeaponController'
import WeaponControllerInterface from '~/base/weapons/interfaces/WeaponControllerInterface'
import { StateInterface } from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default class LaserController extends BaseWeaponController<LaserState>  implements WeaponControllerInterface<LaserState>  {
  constructor(sprite: BaseWeapon, scene: Level) {
    super(
      sprite, scene, LaserState,
    )
    // this.setCustomCollisions()
  }

  // GETTERS

  public get currentState(): StateInterface | undefined {
    return this.weaponState.stateMachine.currentState
  }

  // WIP
  // TODO: Make collisions between player bullets and enemies bullets work
  // private setCustomCollisions(): void {
  //   // loop through enemies bullets and add collision
  //   this.scene.enemiesBullets.forEach((enemyBullet) => {
  //     this.scene.physics.add.collider(
  //       this.sprite, enemyBullet,
  //       (sprite, bullet) => {
  //         this.debouncedHandleLaserCollidesWithBullet(sprite, bullet)
  //       }, undefined, this,
  //     )
  //   })
  // }

  // private debouncedHandleLaserCollidesWithBullet = debounce(
  //   (sprite: Phaser.GameObjects.GameObject, bullet: Phaser.GameObjects.GameObject) => {
  //     this.handleLaserCollidesWithBullet(sprite, bullet)
  //   }, 250, {
  //     leading: true,
  //     trailing: false, 
  //   },
  // )

  // private handleLaserCollidesWithBullet(sprite, bullet): void {
  //   console.log('handleLaserCollidesWithBullet')
  //   console.log('sprite', sprite)
  //   console.log('bullet', bullet)
  //   this.weaponState.stateMachine.setState('explode')
  // }
}