
import Laser from './Laser'
import LaserController from './LaserController'
import LaserState from './LaserState'

import BaseWeaponsGroup from '~/base/weapons/BaseWeaponsGroup'

export default class LasersGroup extends BaseWeaponsGroup<LaserController, LaserState> {
  controllers: LaserController[] = []

  quantity = 10
  
  constructor(scene: Phaser.Scene) {
    super(
      scene, 10, 'laser', Laser,
    )
  }
}