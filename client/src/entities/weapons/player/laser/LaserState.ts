import BaseWeapon from '~/base/weapons/BaseWeapon'
import WeaponStateInterface from '~/base/weapons/interfaces/WeaponStateInterface'
import { PRESPAWN_POSITIONS } from '~/const/positions'
import StateMachine from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default class LaserState implements WeaponStateInterface {
  public stateMachine: StateMachine

  private sprite!: BaseWeapon

  private scene: Level

  constructor(scene: Level, sprite: BaseWeapon) {
    this.scene = scene
    this.sprite = sprite
    this.stateMachine = new StateMachine(this, 'laser')
    this.addStates()
  }

  private addStates(): void {
    this.stateMachine.addState('shoot', {
      onEnter: this.shootOnEnter,
      onUpdate: this.shootOnUpdate,
    })
      .addState('explode', { onEnter: this.explodeOnEnter })
      .addState('inactive', { onEnter: this.inactiveOnEnter })
  }

  private shootOnEnter(): void {
    this.handlePlayerShootLaser()
  }
  
  private shootOnUpdate(): void {
    if (!this.scene.cameras.main.worldView.contains(this.sprite.x, this.sprite.y)) {
      this.stateMachine.setState('inactive')
    }
  }

  private explodeOnEnter(): void {
    this.sprite.explode()
    this.sprite.on('animationcomplete', () => {
      this.stateMachine.setState('inactive')
    })
  }

  private inactiveOnEnter(): void {
    this.sprite.setActive(false)
    this.sprite.setVisible(false)
    this.sprite.body.reset(PRESPAWN_POSITIONS.laser.x,PRESPAWN_POSITIONS.laser.y)
  }

  private handlePlayerShootLaser(): void {
    const { x, y, width } = this.scene.player
    const { direction } = this.scene.playerController

    this.sprite.fire({
      x: direction === 'left' ? x - width /2 : x + width /2,
      y: this.scene.playerController.state.currentState?.name === 'crounch' ? y + 10 : y + 5,
      direction: direction,
    })
  }
}