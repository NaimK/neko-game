import { LASER_SPEED } from '@const/velocities'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import { LASER_DAMAGE } from '~/const/damages'

export default class Laser extends BaseWeapon {
  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
  ) {
    super(
      scene, x, y, 'laser', LASER_DAMAGE, LASER_SPEED,
    )
    this.createAnimations()
  }

  private createAnimations(): void {    
    const world = this.scene.physics.world
    
    world.scene.anims.create({
      key: 'laser-shoot',
      frameRate: 10,
      frames: world.scene.anims.generateFrameNames('laser', {
        start: 1,
        end: 6,
        prefix: 'laser-shoot-',
        suffix: '.png', 
      }),
      repeat: -1,
    })

    world.scene.anims.create({
      key: 'laser-explode',
      frameRate: 30,
      frames: world.scene.anims.generateFrameNames('laser', {
        start: 1,
        end: 9,
        prefix: 'laser-explode-',
        suffix: '.png',
      }),
      repeat: 0,
    })
  }

  fire(options: { x: number, y: number, direction?: 'left'|'right' }) {
    const { x, y, direction } = options

    this.setDefaults(x, y)

    this.anims.play('laser-shoot')
    this.sounds.shoot.play()

    if (direction === 'left') {
      this.setFlipX(true)
    } else {
      this.setFlipX(false)
    }
    this.setVelocityX(direction === 'left' ? -LASER_SPEED : LASER_SPEED)
  }

  public explode(): void {
    this.setVelocityX(0)
    this.play('laser-explode')
    this.sounds.explode.play()
  }
}