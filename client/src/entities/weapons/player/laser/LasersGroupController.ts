
import LaserController from './LaserController'
import LaserState from './LaserState'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import BaseWeaponsGroupController from '~/base/weapons/BaseWeaponsGroupController'
import LasersGroup from '~/entities/weapons/player/laser/LasersGroup'
import Level from '~/scenes/levels/Level'

export default class LasersGroupController extends BaseWeaponsGroupController<LasersGroup, LaserController> {    
  constructor(scene: Level) {
    super(scene, new LasersGroup(scene))
  }

  public fireLaser(controllerClass: new(sprite: BaseWeapon, scene: Level) => LaserController): void {
    this.weapons.fireWeapon(controllerClass, LaserState) 
  }
}