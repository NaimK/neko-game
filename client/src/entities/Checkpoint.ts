import { CHECKPOINTS_DEPTH } from '~/const/depths'

export default class Checkpoint extends Phaser.Physics.Arcade.Sprite {
  public id: number
  
  constructor(
    scene: Phaser.Scene, x: number, y: number, id: number,
  ) {
    super(
      scene, x, y, 'checkpoint',
    )
    this.id = id
    this.scene.add.existing(this)
    this.scene.physics.add.existing(this)
    this.setSize(16, 96)
    this.setImmovable(true)
    this.setDepth(CHECKPOINTS_DEPTH)
    // This is not working as expected
    this.setGravity(0)
    // So we have to use this workaround
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.body.allowGravity = false

    this.createAnimations()
    this.anims.play('checkpoint-activated')
  }

  private createAnimations(): void {
    const world = this.scene.physics.world
    
    world.scene.anims.create({
      key: 'checkpoint-activated',
      frameRate: 10,
      frames: world.scene.anims.generateFrameNames('checkpoint', {
        start: 1,
        end: 5,
        prefix: 'checkpoint-activated-',
        suffix: '.png', 
      }),
      repeat: -1,
      yoyo: true,
    })
  }

}