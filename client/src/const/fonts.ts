export const FONT_MAIN = 'pixel-operator'
export const FONT_ALT = 'metal-slug'

export const FONT_SIZE_SMALL = 8
export const FONT_SIZE_MEDIUM = 16
export const FONT_SIZE_LARGE = 24