export const GAME_WIDTH = 530
export const GAME_HEIGHT = 240

export const LEVEL_WIDTH = 240 * 16
export const LEVEL_HEIGHT = GAME_HEIGHT * 3

export const TILE_SIZE = 16

export const UI_PADDING = 20
export const UI_PADDING_SM = 8