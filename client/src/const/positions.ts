// PRESPAWN POSITIONS
// We don't want to store them in the same position 
// so that they won't trigger collisions outside of the screen

export const PRESPAWN_POSITIONS = {
  laser: {
    x: -100,
    y: -100, 
  },
  enemy: {
    x: -500,
    y: -100, 
  },
}
