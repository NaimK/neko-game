export const PLAYER_WALK_SPEED = 175
export const PLAYER_JUMP_SPEED = -215
export const LASER_SPEED = 300

// enemies
export const ROBOT_RUN_SPEED = 60
export const ROBOT_BULLET_SPEED = 250

export const YELLOW_SUIT_RUN_SPEED = 100
export const YELLOW_SUIT_BULLET_SPEED = 300