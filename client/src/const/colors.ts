export const COLOR_BACKGROUND = '#00003c'
export const COLOR_BACKGROUND_Ox = 0x00003c

export const COLOR_PRIMARY = '#750e97'
export const COLOR_PRIMARY_Ox = 0x750e97

export const COLOR_SECONDARY = '#00a8c2'
export const COLOR_SECONDARY_Ox = 0x00a8c2

export const COLOR_TERTIARY = '#00c38f'
export const COLOR_TERTIARY_Ox = 0x00c38f

export const COLOR_WHITE = '#ffffff'
export const COLOR_WHITE_Ox = 0xffffff