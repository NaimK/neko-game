// export const GROUND_DEPTH = 1
// export const FOREFRONT_SET_DEPTH = 0
// export const PLATFORMS_DEPTH = 10
export const PLAYER_DEPTH = 50
export const BRIDGES_DEPTH = 100
export const PIPES_DEPTH = 150
export const CHECKPOINTS_DEPTH = 200
export const MODALS_DEPTH = 300