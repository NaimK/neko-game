import { PLAYER_HEALTH_DEFAULT, PLAYER_LIVES_DEFAULT } from '~/const/health'
import EventBus from '~/core/EventBus'
import { CypherNekoNFT } from '~/types/nft'
import { User } from '~/types/user'

interface GameState {
  level: string
  score: number
  coins: number
  enemiesKilled: number
  playerHealth: number
  lives: number
  floor: string
  isPaused: boolean
  lastCheckpoint: {
    id: string | null
    x: number | null
    y: number | null
  }
  levelEnd: {
    x: number | null
    y: number | null
  }
  wallet: {
    address: string | null
    chainId: number | null
  }
  nfts: CypherNekoNFT[]
  hasRefreshedNfts: boolean
  isLoggedIn: boolean
  isDemo: boolean
  gameIsOver: boolean
  user: User | null
  completionTime: number
  gamepad: {
    connected: boolean
    name: string | null
  }
}

const state: GameState = {
  level: 'level-1',
  score: 0,
  coins: 0,
  enemiesKilled: 0,
  playerHealth: PLAYER_HEALTH_DEFAULT,
  lives: PLAYER_LIVES_DEFAULT,
  floor: 'ground',
  isPaused: false,
  lastCheckpoint: {
    id: null,
    x: null,
    y: null,
  },
  levelEnd: {
    x: null,
    y: null,
  },
  wallet: {
    address: null,
    chainId: null,
  },
  nfts: [],
  hasRefreshedNfts: false,
  isLoggedIn: false,
  isDemo: false,
  gameIsOver: false,
  user: null,
  completionTime: 0,
  gamepad: {
    connected: false,
    name: null,
  },
}

const updateState = (newState: typeof state) => {
  Object.assign(state, newState)
}

const get = <T>(property: keyof typeof state): T => {
  return state[property] as T
}

const set = (property: keyof typeof state, value: any) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  state[property] = value

  EventBus.emit('game-state-changed', {
    property,
    value, 
  })
}

const getState = () => state

export default {
  updateState,
  get,
  set,
  getState,
}