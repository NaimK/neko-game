
import BaseWeapon from './BaseWeapon'
import WeaponControllerInterface from './interfaces/WeaponControllerInterface'
import WeaponStateInterface from './interfaces/WeaponStateInterface'

import Level from '~/scenes/levels/Level'

export default class BaseWeaponsGroup
  <C extends WeaponControllerInterface<WeaponStateInterface>, S extends WeaponStateInterface> 
  extends Phaser.Physics.Arcade.Group {
  public controllers: C[] = []

  constructor(
    scene: Phaser.Scene,
    frameQuantity = 10,
    key: string,
    classType: typeof BaseWeapon,
  ) {
    super(scene.physics.world, scene)

    this.createMultiple({
      frameQuantity,
      key,
      active: false,
      visible: false,
      classType,
    })
  }

  public fireWeapon(
    controllerClass: new(sprite: BaseWeapon, scene: Level) => C, state: new (scene: Level, sprite: BaseWeapon, x?: number, y?:number) => S, x?: number, y?:number,
  ): void {
    const weapon: BaseWeapon = this.getFirstDead(false)
    const currentIteration = this.countActive(true)

    if (weapon) {
      if (!this.controllers[currentIteration]) {
        this.createController(
          controllerClass, weapon, x, y,
        )
      } else {
        const nextAvailableController = this.findNextAvailableController()

        if (nextAvailableController) {
          this.reallocateController(
            nextAvailableController, weapon, state, x, y,
          )
        } else {
          this.createController(
            controllerClass, weapon, x, y,
          )
        }
      }
    }
  }

  private findNextAvailableController(): C | undefined {
    return this.controllers.find((controller) => {
      return controller.currentState?.name === 'inactive'
    })
  }

  private reallocateController(
    controller: C, weapon: BaseWeapon, state: new (scene: Level, sprite: BaseWeapon) => S, x?: number, y?:number,
  ): void {
    controller.bindWeapon(weapon)
    controller.initWeaponState(
      state, x, y,
    )
  }

  private createController(
    controllerClass: new(sprite: BaseWeapon, scene: Level, x?: number, y?: number) => C,
    weapon: BaseWeapon,
    x?: number, 
    y?: number,
  ): void {
    const newController = new controllerClass(
      weapon, this.scene as Level, x, y,
    )

    this.controllers.push(newController)
  }

  // add update method
  public update(dt: number): void {
    this.controllers.forEach((controller) => {
      controller.update(dt)
    })
  }
}