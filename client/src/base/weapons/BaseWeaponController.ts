import debounce from 'lodash/debounce'

import BaseWeapon from './BaseWeapon'
import WeaponStateInterface from './interfaces/WeaponStateInterface'

import BaseEnemy from '~/base/enemies/BaseEnemy'
import { StateInterface } from '~/core/StateMachine'
import Laser from '~/entities/weapons/player/laser/Laser'
import Level from '~/scenes/levels/Level'

export default class BaseWeaponController<T extends WeaponStateInterface> {
  public scene: Level

  public sprite!: BaseWeapon

  public weaponState!: T

  constructor(
    sprite: BaseWeapon,
    scene: Level,
    weaponStateClass: new(scene: Level, sprite: BaseWeapon, x?:number, y?:number) => T,
    x?: number,
    y?: number,
  ) {
    this.bindWeapon(sprite)
    this.scene = scene
    this.init(
      weaponStateClass, x, y,
    )
  }

  // GETTERS

  public get currentState(): StateInterface | undefined {
    return this.weaponState.stateMachine.currentState
  }

  private init(
    weaponStateClass: new(scene: Level, sprite: BaseWeapon) => T, x?: number, y?: number,
  ): void {
    this.setCollisions()
    this.initWeaponState(
      weaponStateClass, x, y,
    )
  }

  public bindWeapon(weapon: BaseWeapon): void {
    this.sprite = weapon
    this.sprite.setController(this)
  }

  public initWeaponState(
    weaponStateClass: new(scene: Level, sprite: BaseWeapon, x?:number, y?:number) => T, x?: number, y?: number,
  ): void {
    this.weaponState = new weaponStateClass(
      this.scene, this.sprite, x, y,
    )
    this.weaponState.stateMachine.setState('shoot')
  }

  private setCollisions(): void {
    this.scene.physics.add.collider(
      this.sprite, this.scene.ground,
      this.handleWeaponCollidesWithGround, undefined, this,
    )
    // loop through enemies and add collision
    this.scene.enemies.forEach((enemy) => {
      this.scene.physics.add.collider(
        this.sprite, enemy,
        this.debouncedHandleWeaponCollidesWithEnemy, undefined, this,
      )
    })
  }

  private debouncedHandleWeaponCollidesWithEnemy = debounce(
    this.handleWeaponCollidesWithEnemy, 250, {
      leading: true,
      trailing: false, 
    },
  )
  
  public handleWeaponCollidesWithEnemy(sprite: Phaser.GameObjects.GameObject, enemy: Phaser.GameObjects.GameObject): void {
    if (sprite instanceof Laser) {
      // if laser, this is a player weapon
      this.weaponState.stateMachine.setState('explode')
      ;(enemy as BaseEnemy).takeDamage(this.sprite.damage)
    }
  }

  private handleWeaponCollidesWithGround(): void {
    this.weaponState.stateMachine.setState('explode')
  }

  public update(dt: number): void {
    this.weaponState.stateMachine.update(dt)
  }
}