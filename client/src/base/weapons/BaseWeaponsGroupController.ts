
import WeaponControllerInterface from './interfaces/WeaponControllerInterface'
import { WeaponsGroupInterface } from './interfaces/WeaponsGroupInterface'
import WeaponStateInterface from './interfaces/WeaponStateInterface'

import Level from '~/scenes/levels/Level'

// TODO: Clarify and clean up this type

export default class BaseWeaponsGroupController
  <
    G extends WeaponsGroupInterface<WeaponControllerInterface<WeaponStateInterface>,
    WeaponStateInterface
  >, 
    C extends WeaponControllerInterface<WeaponStateInterface>
  > {  
  protected scene: Level
  
  public weapons!: G
  
  constructor(scene: Level,  weapons: G) {
    this.scene = scene
    this.weapons = weapons
  }

  public update(dt: number): void {
    this.weapons.controllers.forEach((controller) => {
      controller.update(dt)
    })
  }
}