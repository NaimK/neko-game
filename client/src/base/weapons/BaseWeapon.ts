import BaseWeaponController from './BaseWeaponController'
import WeaponStateInterface from './interfaces/WeaponStateInterface'

// TODO: This class might have to be abstract 

export default class BaseWeapon extends Phaser.Physics.Arcade.Sprite {  
  public controller?: BaseWeaponController<WeaponStateInterface>

  public damage: number

  public speed: number

  protected sounds = {
    shoot: this.scene.sound.add('beam'),
    explode: this.scene.sound.add('explosion'),
  }

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    key: string,
    damage: number,
    speed: number,
  ) {
    super(
      scene, x, y, key,
    )
    this.damage = damage
    this.speed = speed
  }

  // This method has to be implemented in the child class
  public fire(options: { x: number, y: number, direction?: 'left'|'right' }) {    
    //
  }

  // This method has to be implemented in the child class
  public explode(): void {
    //
  }

  protected setDefaults(
    x: number, y: number, scale = 0.5,
  ): void {
    this.body.reset(x, y)
    this.setActive(true)
    this.setVisible(true)
    this.setImmovable(true)
    this.setSize(8, 8)
    this.scale = scale
    // This is not working as expected
    this.setGravityY(0)
    // So we have to use this workaround
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.body.allowGravity = false
  }

  public setController(controller: BaseWeaponController<WeaponStateInterface>): void {
    this.controller = controller
  }

  destroy(): void {
    // this.setActive(false)
    // this.setVisible(false)
    // this.isFiring = false
    // this.setVelocityX(0)
  }
}