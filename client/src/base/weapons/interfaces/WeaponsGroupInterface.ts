
import WeaponControllerInterface from './WeaponControllerInterface'
import WeaponStateInterface from './WeaponStateInterface'
import BaseWeapon from '../BaseWeapon'

import Level from '~/scenes/levels/Level'

export interface WeaponsGroupInterface<C extends WeaponControllerInterface<WeaponStateInterface>, S extends WeaponStateInterface> {
  controllers: C[]
  fireWeapon(controllerClass: new(sprite: BaseWeapon, scene: Level) => C, state: new (scene: Level, sprite: BaseWeapon, x?:number, y?:number) => S): void
  update(dt: number): void
}