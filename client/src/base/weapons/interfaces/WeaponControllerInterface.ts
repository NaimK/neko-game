
import WeaponStateInterface from './WeaponStateInterface'

import BaseWeapon from '~/base/weapons/BaseWeapon'
import { StateInterface } from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default interface WeaponControllerInterface<T extends WeaponStateInterface> {
  scene: Level
  sprite: BaseWeapon
  weaponState: T

  currentState: StateInterface | undefined
  bindWeapon(weapon: BaseWeapon): void
  initWeaponState(weaponStateClass: new(scene: Level, sprite: BaseWeapon) => T, x?: number, y?: number): void
  handleWeaponCollidesWithEnemy(_: Phaser.GameObjects.GameObject, enemy: Phaser.GameObjects.GameObject): void
  update(dt: number): void
}
