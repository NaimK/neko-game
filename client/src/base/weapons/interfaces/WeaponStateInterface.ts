import StateMachine from '~/core/StateMachine'

export default interface WeaponStateInterface {
  stateMachine: StateMachine
}