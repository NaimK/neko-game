import BaseEnemyController from './BaseEnemyController'
import EnemyStateInterface from './interfaces/EnemyStateInterface'

export default class BaseEnemy extends Phaser.Physics.Arcade.Sprite {
  private health!: number

  public controller?: BaseEnemyController<EnemyStateInterface>

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    texture: string,
    health: number,
  ) {
    super(
      scene, x, y, texture,
    )
    this.setHealth(health)

    setTimeout(() => {
      this.setImmovable(true)
    }, 100)
  }

  public setHealth(health: number): void {
    this.health = health
  }

  public spawn(x: number, y: number): void {
    this.setActive(true)
    this.setVisible(true)
    this.setPosition(x, y)
  }

  public takeDamage(damage: number): void {
    this.health -= damage
    this.controller?.state.stateMachine.setState('hurt')

    if (this.health <= 0) {
      setTimeout(() => {
        this.controller?.state.stateMachine.setState('dead')
      }, 250)
    }
  }

  public setController(controller: BaseEnemyController<EnemyStateInterface>): void {
    this.controller = controller
  }
}