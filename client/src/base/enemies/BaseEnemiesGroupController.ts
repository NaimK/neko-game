
import BaseEnemy from './BaseEnemy'
import { EnemiesGroupInterface } from './interfaces/EnemiesGroupInterface'
import EnemyControllerInterface from './interfaces/EnemyControllerInterface'
import EnemyStateInterface from './interfaces/EnemyStateInterface'

import Level from '~/scenes/levels/Level'

export default class BaseEnemiesGroupController
  <G extends EnemiesGroupInterface<EnemyControllerInterface<EnemyStateInterface>>, C extends EnemyControllerInterface<EnemyStateInterface>> {  
  protected scene: Level
  
  public enemies!: G

  protected type: 'robot' | 'yellow-suit'

  constructor(
    scene: Level, enemies: G, controllerClass: new(sprite: BaseEnemy, scene: Level, x: number, y: number) => C,
    type: 'robot' | 'yellow-suit',
  ) {
    this.scene = scene
    this.enemies = enemies
    this.type = type

    this.spawnEnemies(controllerClass)
  }

  private spawnEnemies(controllerClass: new(sprite: BaseEnemy, scene: Level, x: number, y: number) => C): void {
    if (import.meta.env.VITE_PREVENT_ENEMIES_SPAWN === 'true') {
      return
    }
    
    const enemiesLayer = this.scene.map.getObjectLayer('Objects/enemies-spawn')
  
    enemiesLayer.objects.forEach((enemy) => {
      const enemyType = enemy.name.split('enemy-spawn-')[1]
      
      if (enemyType === this.type &&  enemy.x && enemy.y) {
        this.spawnEnemy(
          enemy.x, enemy.y, controllerClass,
        )
      }
    })
  }
  
  protected spawnEnemy<C extends EnemyControllerInterface<EnemyStateInterface>>(
    x: number, y: number, controllerClass: new(sprite: BaseEnemy, scene: Level, x: number, y: number) => C,
  ): void {
    this.enemies.spawnEnemy(
      x, y, controllerClass,
    )
  }

  // add update method
  public update(dt: number): void {
    this.enemies.update(dt)
  }
}