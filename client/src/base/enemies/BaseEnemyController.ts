
import BaseEnemy from './BaseEnemy'
import EnemyStateInterface from './interfaces/EnemyStateInterface'

import { GAME_HEIGHT } from '~/const/sizes'
import EventBus from '~/core/EventBus'
import { StateInterface } from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default class BaseEnemyController<T extends EnemyStateInterface> {
  protected scene: Level

  public sprite!: BaseEnemy

  public state!: T

  constructor(
    sprite: BaseEnemy, scene: Level, x: number, y: number, enemyStateClass: new(scene: Level, enemy: BaseEnemy) => T,
  ) {
    this.bindEnemy(sprite)
    this.scene = scene
    this.init(
      x, y, enemyStateClass,
    )
  }

  // GETTERS

  public get currentState(): StateInterface | undefined {
    return this.state.stateMachine.currentState
  }

  public get isOnDifferentFloorThanPlayer(): boolean {
    const playerY = this.scene.player.y
    const enemyY = this.sprite.y
    
    if (playerY > GAME_HEIGHT && enemyY < GAME_HEIGHT) return true
    if (playerY < GAME_HEIGHT && enemyY > GAME_HEIGHT) return true
    return false
  }

  public get isMoreThanOneFloorBelowPlayer(): boolean {
    const playerY = this.scene.player.y
    const enemyY = this.sprite.y

    return (enemyY - playerY) > GAME_HEIGHT / 3
  }

  public get isWithinShootingRangeX(): boolean {
    const playerX = this.scene.player.x
    const robotX = this.sprite.x

    if (playerX < robotX) {
      return robotX - playerX < 150 && robotX - playerX > 50
    } else {
      return playerX - robotX < 150 && playerX - robotX > 50
    }
  }

  public get isWithinShootingRangeY(): boolean {
    const playerY = this.scene.player.y
    const robotY = this.sprite.y

    if (playerY < robotY) {
      return robotY - playerY < 25
    } else {
      return playerY - robotY < 25
    }
  }
  
  private init(
    x: number, y: number, enemyStateClass: new(scene: Level, enemy: BaseEnemy) => T,
  ): void {
    this.setCollisions()
    this.spawn(x, y)
    this.initState(enemyStateClass)
  }

  public bindEnemy(enemy: BaseEnemy): void {
    this.sprite = enemy
    this.sprite.setController(this)
  }
  
  public initState(enemyStateClass: new(scene: Level, enemy: BaseEnemy) => T): void {
    this.state = new enemyStateClass(this.scene, this.sprite)
  }

  private setCollisions(): void {
    this.scene.physics.add.collider(
      this.sprite, (this.scene as Level).ground,
      this.handleEnemyCollidesWithGround, undefined, this,
    )
    this.scene.physics.add.collider(
      this.sprite, (this.scene as Level).player,
      this.handleEnemyCollidesWithPlayer, undefined, this,
    )
  }

  private handleEnemyCollidesWithGround() : void {
    //
  }
  
  private handleEnemyCollidesWithPlayer() : void {
    EventBus.emit('player-enemy-collision')
  }

  public spawn(x: number, y: number): void {
    this.sprite.setActive(true)
    this.sprite.setVisible(true)
    this.sprite.body.reset(x, y)
    this.sprite.setOrigin(0, 0)

    if (this.sprite.texture.key === 'robot') {
      this.sprite.setSize(24, 28)
      this.sprite.setOffset(2, 6)
    } else if (this.sprite.texture.key === 'yellow-suit') {
      this.sprite.setSize(24, 32)
      this.sprite.setOffset(3, 6)
    }
    
  }

  public update(dt: number): void {
    // check if enemy is within camera bounds
    if (Phaser.Geom.Rectangle.Overlaps(this.scene.cameras.main.worldView, this.sprite.getBounds())) {
      const playerX = this.scene.player.body.position.x
      const enemyX = this.sprite.body.position.x
      const currentEnemyState = this.currentState?.name

      if (currentEnemyState === 'hurt') {
        return
      }

      if (!currentEnemyState) {
        this.state.stateMachine.setState('idle')
      } else {
        if (playerX < enemyX - 25 && !['run-left', 'stand-shoot'].includes(currentEnemyState)) {
          this.state.stateMachine.setState('run-left')
        } else if (playerX > enemyX + 25 && !['run-right', 'stand-shoot'].includes(currentEnemyState)) {
          this.state.stateMachine.setState('run-right')
        }
      }
    }
  }
}