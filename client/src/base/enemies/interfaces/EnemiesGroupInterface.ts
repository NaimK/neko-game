
import EnemyControllerInterface from './EnemyControllerInterface'
import EnemyStateInterface from './EnemyStateInterface'
import BaseEnemy from '../BaseEnemy'

import Level from '~/scenes/levels/Level'

export interface EnemiesGroupInterface<C extends EnemyControllerInterface<EnemyStateInterface>> {
  spawnEnemy(x: number, y: number, controllerClass: new (sprite: BaseEnemy, scene: Level, x: number, y: number) => C): void
  update(dt: number): void
}