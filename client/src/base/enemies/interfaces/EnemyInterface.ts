import EnemyStateInterface from './EnemyStateInterface'
import BaseEnemyController from '../BaseEnemyController'

export default interface EnemyInterface<T extends EnemyStateInterface> {
  controller?: BaseEnemyController<T>
  spawn(x: number, y: number): void
  takeDamage(damage: number): void
  setController(controller: BaseEnemyController<T>): void
}