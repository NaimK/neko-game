import EnemyStateInterface from './EnemyStateInterface'
import BaseEnemy from '../BaseEnemy'

import { StateInterface } from '~/core/StateMachine'
import Level from '~/scenes/levels/Level'

export default interface EnemyControllerInterface<T> {
  sprite: BaseEnemy
  state: EnemyStateInterface
  update(dt: number): void
  spawn(x: number, y: number): void
  bindEnemy(enemy: BaseEnemy): void
  initState(enemyStateClass: new(scene: Level, enemy: BaseEnemy) => T): void
  currentState: StateInterface | undefined
}