import StateMachine from '~/core/StateMachine'

export default interface EnemyStateInterface {
  stateMachine: StateMachine
}