
import BaseEnemy from './BaseEnemy'
import EnemyControllerInterface from './interfaces/EnemyControllerInterface'
import EnemyStateInterface from './interfaces/EnemyStateInterface'

import Level from '~/scenes/levels/Level'

export default class BaseEnemiesGroup<C extends EnemyControllerInterface<EnemyStateInterface>> extends Phaser.Physics.Arcade.Group {
  public controllers: C[] = []

  constructor(
    scene: Phaser.Scene,
    frameQuantity = 10,
    key: string,
    classType: typeof BaseEnemy,
  ) {
    super(scene.physics.world, scene)

    this.createMultiple({
      frameQuantity,
      key,
      active: false,
      visible: false,
      classType,
    })
  }

  public spawnEnemy(
    x: number, y: number, controllerClass: new(sprite: BaseEnemy, scene: Level, x: number, y: number) => C,
  ): void {
    const enemy: BaseEnemy = this.getFirstDead(false)
    const currentIteration = this.countActive(true)

    if (enemy) {
      if (!this.controllers[currentIteration]) {
        this.createController(
          controllerClass, enemy, x, y,
        )
      } else {
        const nextAvailableController = this.findNextAvailableController()

        if (nextAvailableController) {
          this.reallocateController(nextAvailableController, enemy)
        } else {
          this.createController(
            controllerClass,enemy, x, y,
          )
        }
      }
    }
  }

  private findNextAvailableController(): C | undefined {
    return this.controllers.find((controller) => {
      return controller.currentState?.name === 'inactive'
    })
  }

  private reallocateController(controller: C, enemy: BaseEnemy): void {
    controller.bindEnemy(enemy)
    controller.initState(controller.state as any)
  }

  private createController(
    controllerClass: new(sprite: BaseEnemy, scene: Level, x: number, y: number) => C,
    enemy: BaseEnemy,
    x: number,
    y: number,
  ): void {
    this.controllers.push(new controllerClass(
      enemy, this.scene as Level, x, y,
    ))
  }

  // add update method
  public update(dt: number): void {
    this.controllers.forEach((controller) => {
      controller.update(dt)
    })
  }
}