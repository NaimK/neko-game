// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as Sentry from '@sentry/browser'
import { Game } from 'phaser'
import VirtualJoystickPlugin from 'phaser3-rex-plugins/plugins/virtualjoystick-plugin.js'

import NineSlicePlugin from './plugins/nineslice/NineSlicePlugin'
import MainInput from './scenes/MainInput'
import MergedInputScene from './scenes/MergedInput'
import VirtualInput from './scenes/VirtualInput'

import { GAME_HEIGHT, GAME_WIDTH } from '~/const/sizes'
import AuthPlugin from '~/plugins/auth'
import LocalizationPlugin from '~/plugins/localization'
import NftContractPlugin from '~/plugins/nft-contract'
import UserAgentPlugin from '~/plugins/user-agent'
import WalletPlugin from '~/plugins/wallet'
import Level1Intro from '~/scenes/cutscenes/Level1Intro'
import DemoRegister from '~/scenes/DemoRegister'
import GameOver from '~/scenes/GameOver'
import LevelEnd from '~/scenes/LevelEnd'
import Level from '~/scenes/levels/Level'
import MainMenu from '~/scenes/MainMenu'
import MainMenuNfts from '~/scenes/MainMenuNfts'
import MainMenuNickname from '~/scenes/MainMenuNickname'
import MainMenuScores from '~/scenes/MainMenuScores'
import Pause from '~/scenes/Pause'
import Preloader from '~/scenes/Preloader'
import SplashScreen from '~/scenes/SplashScreen'
import UI from '~/scenes/UI'

const isDev = import.meta.env.DEV
const sentryDns = import.meta.env.VITE_SENTRY_DNS

if (isDev) {
  if (import.meta.env.VITE_MOCK_SERVER === 'true') {
  // SERVER MOCK
    await import('./mock-server').then(({ default: mockServer }) => mockServer())
  }
} else {
  Sentry.init({
    dsn: sentryDns,
    integrations: [
      new Sentry.BrowserTracing({
        // Set `tracePropagationTargets` to control for which URLs distributed tracing should be enabled
        tracePropagationTargets: ['localhost'],
      }),
      new Sentry.Replay(),
    ],
    // Performance Monitoring
    tracesSampleRate: 0.2, // Capture 100% of the transactions, reduce in production!
    // Session Replay
    replaysSessionSampleRate: 0.1, // This sets the sample rate at 10%. You may want to change it to 100% while in development and then sample at a lower rate in production.
    replaysOnErrorSampleRate: 1.0, // If you're not already sampling the entire session, change the sample rate to 100% when sampling sessions where errors occur.
  })
}

const config: Phaser.Types.Core.GameConfig = {
  dom: { createContainer: true },
  type: Phaser.AUTO,
  parent: 'app',
  width: GAME_WIDTH,
  height: GAME_HEIGHT,
  physics: {
    default: 'arcade',
    arcade: {
      debug: import.meta.env.VITE_DEBUG_MODE === 'true' ? true : false,
      gravity: { y: 200 },
    },
  },
  input: { gamepad: true },
  audio: { disableWebAudio: true },
  plugins: {
    global: [
      {
        key: 'UserAgentPlugin',
        plugin: UserAgentPlugin,
        start: true,
      },
      {
        key: 'LocalizationPlugin',
        plugin: LocalizationPlugin,
        start: true,
        mapping: 'localization',
        data: { 
          translationFolder: 'assets/translations',
          defaultLocale: 'en',
        },
      },
      {
        key: 'NineSlicePlugin',
        plugin: NineSlicePlugin,
        start: true,
      },
      {
        key: 'rexVirtualJoystick',
        plugin: VirtualJoystickPlugin,
        start: true,
      },
      {
        key: 'AuthPlugin',
        plugin: AuthPlugin,
        start: true,
      },
      {
        key: 'WalletPlugin',
        plugin: WalletPlugin,
        start: true,    
        mapping: 'ethers',
      },
      {
        key: 'NftContractPlugin',
        plugin: NftContractPlugin,
        start: true,
      },
    ],
  },
  scene: [
    Preloader,
    SplashScreen,
    MainMenu,
    MainMenuNickname,
    MainMenuScores,
    MainMenuNfts,
    Level1Intro,
    Level,
    UI,
    MainInput,
    MergedInputScene,
    VirtualInput,
    Pause,
    GameOver,
    LevelEnd,
    DemoRegister,
  ],
  scale: {
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
  },
  render: { pixelArt: true },
}

export default new Game(config)
