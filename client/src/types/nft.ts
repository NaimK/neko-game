
export interface CypherNekoNFT {
  tokenId: string
  tokenUri: string
  metadata: {
    name: string
    description: string
    image: string
    attributes: {
      trait_type: string
      value: string
    }[]
  }
  name: string
  symbol: string
}

// {
//    "chain": "0xaa36a7",
// 		"contractType": "ERC721",
// 		"tokenAddress": "0x258A2Cdb3df514A0A9Db953Fe0D0b16C7E953f55",
// 		"tokenId": "1",
// 		"tokenUri": "https://ipfs.moralis.io:2053/ipfs/QmQJy3XxQJ8t37eSjBDKGE5WEYkK219LRifhJ7MfUqDcid/metadata.json",
// 		"metadata": {
// 			"name": "Genesis",
// 			"description": "The Original Neko NFT",
// 			"image": "ipfs://https://ipfs.moralis.io:2053/ipfs/QmZGTxcUmQYBqUja1J1f34USTfk8BWMhcTi6PSxAmGQvp1/genesis.gif",
// 			"attributes": [
// 				{
// 					"trait_type": "Generation",
// 					"value": "1"
// 				},
// 				{
// 					"trait_type": "Eyes",
// 					"value": "Purple"
// 				},
// 				{
// 					"trait_type": "Ears",
// 					"value": "Red"
// 				},
// 				{
// 					"trait_type": "Coat",
// 					"value": "White"
// 				}
// 			]
// 		},
// 		"name": "CypherNekoNFT",
// 		"symbol": "CNK",
// 		"amount": 1,
// 		"blockNumberMinted": "3351890",
// 		"blockNumber": "3351890",
// 		"ownerOf": "0x70651effb37f0369829d6b41e7f44e7e71a78ddd",
// 		"tokenHash": "2e87674f9b3e2df987691ed669bc0292",
// 		"lastMetadataSync": "2023-04-24T14:22:53.886Z",
// 		"lastTokenUriSync": "2023-04-24T13:54:07.075Z",
// 		"possibleSpam": false
//   }
// }
