export interface User {
  id: number
  accountAddress: string
  email?: string
  name?: string
  createdAt?: string
  updatedAt?: string
}