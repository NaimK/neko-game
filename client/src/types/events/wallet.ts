import { BrowserProvider } from 'ethers/types/providers/provider-browser'

export interface WalletConnectedDTO {
  address: string
  chainId: number
  provider: BrowserProvider
}