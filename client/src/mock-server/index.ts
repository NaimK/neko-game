import { createServer } from 'miragejs'

const mockServer = () => {
  const backendUrl = import.meta.env.VITE_BACKEND_URL

  createServer({
    routes() {
      // Allow unhandled requests on the current domain to pass through
      this.passthrough()

      // GET Requests
      
      this.get(`${backendUrl}/api/auth/check`, () => { 
        return { message: 'Authorized' }
      }),

      this.get(`${backendUrl}/api/whitelisted/restrict`, () => {
        return {
          message: 'OK',
          restrict: true, 
        }
      })

      this.get(`${backendUrl}/api/user`, () => {
        return {
          'id':1,
          'accountAddress':'0x70651effb37F0369829D6B41e7f44E7E71A78Ddd',
          'email':null,
          'name':'FRINK',
          'createdAt':'2023-04-15T13:48:32.341Z',
          'updatedAt':'2023-04-17T11:53:13.740Z', 
        }
      }),

      this.get(`${backendUrl}/api/scores/best`, () => [
        {
          'score': 804,
          'username': 'NAIM84AZER',
          'rank': 1,
        },
        {
          'score': 50,
          'username': 'NAIM84',
          'rank': 2,
        },
        {
          'score': 50,
          'username': 'NAIM84',
          'rank': 3,
        },
        {
          'score': 804,
          'username': 'NAIM84',
          'rank': 4,
        },
        {
          'score': 50,
          'username': 'NAIM84',
          'rank': 5,
        },
        {
          'score': 50,
          'username': 'NAIM84',
          'rank': 6,
        },
        {
          'score': 50,
          'username': 'NAIM84',
          'rank': 7,
        },
        {
          'score': 50,
          'username': 'NAIM84',
          'rank': 8,
        },
      ])

      this.get(`${backendUrl}/api/scores/me/best`, () => (
        {          
          'score': 809,
          'position': 1,
        }
      ))

      this.get(`${backendUrl}/api/nfts/me`, () => ([
        {
          'chain': '0xaa36a7',
          'contractType': 'ERC721',
          'tokenAddress': '0xf9c29e5e98ba8461005f932d438579cb9704e24c',
          'tokenId': '1',
          'tokenUri': 'https://ipfs.moralis.io:2053/ipfs/QmQJy3XxQJ8t37eSjBDKGE5WEYkK219LRifhJ7MfUqDcid/metadata.json',
          'metadata': {
            'name': 'Genesis',
            'description': 'The Original Neko NFT',
            'image': 'https://ipfs.moralis.io:2053/ipfs/QmZGTxcUmQYBqUja1J1f34USTfk8BWMhcTi6PSxAmGQvp1/genesis.gif',
            'attributes': [
              {
                'trait_type': 'Generation',
                'value': '1',
              },
              {
                'trait_type': 'Eyes',
                'value': 'Purple',
              },
              {
                'trait_type': 'Ears',
                'value': 'Red',
              },
              {
                'trait_type': 'Coat',
                'value': 'White',
              },
            ],
          },
          'name': 'CypherNekoNFT',
          'symbol': 'CNK',
          'amount': 1,
          'blockNumberMinted': '3379178',
          'blockNumber': '3379178',
          'ownerOf': '0x70651effb37f0369829d6b41e7f44e7e71a78ddd',
          'tokenHash': '733381dcfa9a9f4d826ddee35a2c353f',
          'lastMetadataSync': '2023-04-28T15:12:07.913Z',
          'lastTokenUriSync': '2023-04-28T15:12:05.331Z',
          'possibleSpam': false,
        },
        {
          'chain': '0xaa36a7',
          'contractType': 'ERC721',
          'tokenAddress': '0xf9c29e5e98ba8461005f932d438579cb9704e24c',
          'tokenId': '0',
          'tokenUri': 'https://ipfs.moralis.io:2053/ipfs/QmQJy3XxQJ8t37eSjBDKGE5WEYkK219LRifhJ7MfUqDcid/metadata.json',
          'metadata': {
            'name': 'Genesis',
            'description': 'The Original Neko NFT',
            'image': 'https://ipfs.moralis.io:2053/ipfs/QmZGTxcUmQYBqUja1J1f34USTfk8BWMhcTi6PSxAmGQvp1/genesis.gif',
            'attributes': [
              {
                'trait_type': 'Generation',
                'value': '1',
              },
              {
                'trait_type': 'Eyes',
                'value': 'Purple',
              },
              {
                'trait_type': 'Ears',
                'value': 'Red',
              },
              {
                'trait_type': 'Coat',
                'value': 'White',
              },
            ],
          },
          'name': 'CypherNekoNFT',
          'symbol': 'CNK',
          'amount': 1,
          'blockNumberMinted': '3379115',
          'blockNumber': '3379115',
          'ownerOf': '0x70651effb37f0369829d6b41e7f44e7e71a78ddd',
          'tokenHash': 'b8a0111a1f80e0b575461eb804658c26',
          'lastMetadataSync': '2023-04-28T14:59:25.629Z',
          'lastTokenUriSync': '2023-04-28T14:59:05.968Z',
          'possibleSpam': false,
        },
      ])),

      this.get(`${backendUrl}/api/whitelisted/*`, () => (
        {
          'message': 'OK',
          'whitelisted': true,
        })),

      // POST Requests

      this.post(`${backendUrl}/api/plays`, () => {
        return { message: 'OK' }
      })

      this.post(`${backendUrl}/api/plays/stats`, () => {
        return { message: 'OK' }
      })

      this.post(`${backendUrl}/api/checkpoints`, () => {
        return { message: 'OK' }
      })

      this.post(`${backendUrl}/api/scores`, () => {
        return { message: 'OK' }
      })

      // PATCH Requests

      this.patch(`${backendUrl}/api/user`, () => {
        return {
          'id':1,
          'accountAddress':'0x70651effb37F0369829D6B41e7f44E7E71A78Ddd',
          'email':null,
          'name':'FRINK',
          'createdAt':'2023-04-15T13:48:32.341Z',
          'updatedAt':'2023-04-17T11:53:13.740Z', 
        }
      })
      
    },
  })
  console.log('MirageJS server is running')
}

export default  mockServer