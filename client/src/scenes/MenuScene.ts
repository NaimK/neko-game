import BaseScene from './BaseScene'

import { FONT_MAIN, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH } from '~/const/sizes'
import ModalsController from '~/controllers/ModalsController'
import NineSlice from '~/plugins/nineslice/NineSlice'
import NineSlicePlugin from '~/plugins/nineslice/NineSlicePlugin'

export default class MenuScene extends BaseScene {
  static music: Phaser.Sound.BaseSound

  protected nineslice!: NineSlicePlugin

  public modalsController!: ModalsController

  protected buttons: any[] = []

  protected selectedButtonIndex = 0

  protected lastInputCheck = 0

  constructor(key: string) {
    super(key)
    this.modalsController = new ModalsController(this)
  }

  protected setBackground(animate: boolean): void {
    const y = GAME_HEIGHT / 2 - 80
  
    const menuBg = this.add.tileSprite(
      0, y, GAME_WIDTH * 2, 160, 'menu-bg-back',
    ).setOrigin(0, 0)
    const menuBgFar = this.add.tileSprite(
      0, y, GAME_WIDTH * 2, 160, 'menu-bg-far',
    ).setOrigin(0, 0)
    const menuBgMiddle = this.add.tileSprite(
      0, y, GAME_WIDTH * 2, 160, 'menu-bg-middle',
    ).setOrigin(0, 0)
    const menuBgNear = this.add.tileSprite(
      0, y, GAME_WIDTH * 2, 160, 'menu-bg-near',
    ).setOrigin(0, 0)
    const menuBgFront = this.add.tileSprite(
      0, y, GAME_WIDTH * 2, 160, 'menu-bg-front',
    ).setOrigin(0, 0)

    this.add.rectangle(
      0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000,
    ).setOrigin(0, 0).setAlpha(0.5)

    if (animate) {
      this.tweens.add({
        targets: menuBgFront,
        x: -96,
        duration: 4500,
        ease: 'Linear',
        repeat: -1,
      })
      this.tweens.add({
        targets: menuBgNear,
        x: -112,
        duration: 7000,
        ease: 'Linear',
        repeat: -1,
      })
      this.tweens.add({
        targets: menuBgMiddle,
        x: -96,
        duration: 12000,
        ease: 'Linear',
        repeat: -1,
      })
    }
  }

  protected create(): void {
    super.create()

    this.nineslice = this.plugins.get('NineSlicePlugin') as NineSlicePlugin
    
    // Start by fade in
    this.cameras.main.fadeIn(
      MenuScene.FADE_DURATION, 0, 0, 0,
    )
  }

  protected addText(
    text: string, x: number, y: number,
  ): void {
    this.add.bitmapText(
      x, y, FONT_MAIN, text, FONT_SIZE_SMALL,
    ).setOrigin(0.5, 0.5)
  }

  protected addButton(config: {
    scene: Phaser.Scene,
    text: string,
    width?: number,
    height?: number,
    x: number,
    y: number,
    onClick: () => void,
    disabled?: boolean,
  }): { 
    states: { default: NineSlice, disabled: NineSlice },
    label: Phaser.GameObjects.BitmapText } {
    
    const { scene, text, width = 80, height = 24, x, y, onClick, disabled } = config

    const button = this.nineslice.add(this,
      [
        x,
        y,
        width,
        height,
        {
          key: 'ui-buttons',
          frame: 'button-1.png',
        },
        7,
        7,
      ])
      .setOrigin(0.5, 0.5)
      .setInteractive()
      .setAlpha(disabled ? 0 : 1)

    const buttonHover = this.nineslice.add(this,
      [
        x,
        y,
        width,
        height,
        {
          key: 'ui-buttons',
          frame: 'button-1-hover.png',
        },
        7,
        7,
      ])
      .setOrigin(0.5, 0.5)
      .setAlpha(0)

    const buttonDisabled = this.nineslice.add(this,
      [
        x,
        y,
        width,
        height,
        {
          key: 'ui-buttons',
          frame: 'button-1-disabled.png',
        },
        7,
        7,

      ])
      .setOrigin(0.5, 0.5)
      .setAlpha(disabled ? 1 : 0)

    const label = scene.add.bitmapText(
      x,
      y,
      FONT_MAIN,
      text,
      FONT_SIZE_SMALL,
    ).setOrigin(0.5, 0.5)

    // Add event listeners
    button.addListener('pointerover', () => {
      buttonHover.setAlpha(1)
    })
    button.addListener('pointerout', () => {
      buttonHover.setAlpha(0)
    })
    button.addListener('pointerdown', () => {
      button.removeListener('pointerdown')
      buttonDisabled.setAlpha(1) 
      this.sound.play('ui-bip')
      onClick()
    })

    const buttonData = {
      states: {
        hover: buttonHover,
        disabled: buttonDisabled,
        default: button,
      },
      label,
    }

    this.buttons.push(buttonData)

    return buttonData
  }

  protected resetButtons(): void {
    this.buttons = []
    this.selectedButtonIndex = 0
  }

  protected fadeOutMusic(): void {
    this.tweens.add({
      targets:  MenuScene.music,
      volume:   0,
      duration: MenuScene.FADE_DURATION,
    })

    setTimeout(() => {
      MenuScene.music.stop()
    }, MenuScene.FADE_DURATION)
  }

  protected checkInputsPressed(): void {
    if (this.buttons.length === 0) {
      return
    }

    if (this.mainInput.pressedMenuValidation) {
      this.buttons[this.selectedButtonIndex].states.default.emit('pointerdown')
    } 
    
    if (this.time.now - this.lastInputCheck < 100) {
      return
    } 

    if (this.mainInput.isGoingDown || this.mainInput.isGoingRight) {
      this.selectNextButton()
    } else if (this.mainInput.isGoingUp || this.mainInput.isGoingLeft) {
      this.selectPreviousButton()
    }

    this.lastInputCheck = this.time.now
  }

  protected selectPreviousButton(): void {
    this.selectedButtonIndex = (this.selectedButtonIndex - 1 + this.buttons.length) % this.buttons.length

    if (this.buttons[this.selectedButtonIndex].states.disabled.alpha === 1) {
      this.selectPreviousButton()
      return
    }
    
    this.buttons.forEach((button, index) => {
      if (index === this.selectedButtonIndex) {
        button.states.default.setAlpha(0)
        button.states.hover.setAlpha(1)
      } else {
        button.states.default.setAlpha(1)
        button.states.hover.setAlpha(0)
      }
    })
  }
  
  protected selectNextButton(): void {
    this.selectedButtonIndex = (this.selectedButtonIndex + 1) % this.buttons.length

    if (this.buttons[this.selectedButtonIndex].states.disabled.alpha === 1) {
      this.selectNextButton()
      return
    }

    this.buttons.forEach((button, index) => {
      if (index === this.selectedButtonIndex) {
        button.states.default.setAlpha(0)
        button.states.hover.setAlpha(1)
      } else {
        button.states.default.setAlpha(1)
        button.states.hover.setAlpha(0)
      }
    })
  }

  protected selectFirstButton(): void {
    this.selectedButtonIndex = 0
    this.buttons[0].states.default.setAlpha(1)
    this.buttons[0].states.hover.setAlpha(1)
  }

  update() {
    this.checkInputsPressed()
  }
}