import MergedInput, { KeyCode, Player } from 'phaser3-merged-input'

import EventBus from '~/core/EventBus'
import GameState from '~/store/game'

const PLAYER_INDEX = 0

const buttonsKeysMap = {
  UP: 'UP',
  DOWN: 'DOWN',
  LEFT: 'LEFT',
  RIGHT: 'RIGHT',
  RC_W: 'SHIFT',
  RC_E: 'CTRL',
  RC_S: 'SPACE',
  START: 'ENTER',
  SELECT: 'ESC',
}

export default class MergedInputScene extends Phaser.Scene {
  private mergedInput!: MergedInput

  public player!: Player

  /**
    * The Input scene is designed to run in the background and handle input.
    * We're thus able to reference the MergedInput plugin instance on this scene from multiple scenes within our project.
    * We could also use events in the global events registry to listen to input events on other scenes.
    * However, by making the plugin scene based, we're free to have multiple instances set up if we wish.
    *
    *  @extends Phaser.Scene
    */
  constructor() {
    super('mergedInput')
  }

  // GETTERS

  public get isGoingLeft(): boolean {
    return this.player.direction.LEFT > 0
  }

  public get isGoingRight(): boolean {
    return this.player.direction.RIGHT > 0
  }

  public get isGoingUp(): boolean {
    return this.player.direction.UP > 0
  }

  public get isGoingDown(): boolean {
    return this.player.direction.DOWN > 0
  }

  public get justFired(): boolean {
    return this.player.interaction_mapped.isPressed('RC_S')
  }

  public get pressedMenuValidation(): boolean {
    return this.player.interaction_mapped.isPressed('RC_S') || this.player.interaction_mapped.isPressed('START')
  }

  public get pressedMenuCancel(): boolean {
    return this.player.interaction_mapped.isPressed('RC_E')
  }

  public get isJumping(): boolean {

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.player.buttons_mapped.RC_E > 0
  }

  public get justJumped(): boolean {

    return this.player.interaction_mapped.isPressed('RC_E')
  }

  public get justPressedPause(): boolean {
    return this.player.interaction_mapped.isPressed('START')
  }

  // LIFECYCLE

  protected preload(): void {
    this.load.scenePlugin('mergedInput', MergedInput)
  }

  protected create(): void {
    this.player = this.mergedInput.addPlayer(0)

    this.defineKeys()

    this.bindListeners()

    EventBus.emit('input-ready', {
      type: 'mergedInput',
      input: this.mergedInput,
    })
  }

  update() {
    this.checkInputsPressed()
  }

  // METHODS

  protected bindListeners(): void {
    // On gamepad connected
    this.input.gamepad?.once('connected', (pad: Phaser.Input.Gamepad.Gamepad) => {      
      const padName = this.getPadName(pad.id)
      
      EventBus.emit('text-message', {
        text: `Gamepad connected: ${padName}`,
        typewriter: false,
        size: 'small',
        hideDelay: 2000,
        debounced: true,
      })

      GameState.set('gamepad', {
        connected: true,
        name: padName,
      })
    })

    // On gamepad disconnected
    this.input.gamepad?.once('disconnected', (pad: Phaser.Input.Gamepad.Gamepad) => {
      EventBus.emit('text-message', {
        text: `Gamepad disconnected: ${this.getPadName(pad.id)}`,
        typewriter: false,
        size: 'small',
        hideDelay: 2000,
        debounced: true,
      })

      GameState.set('gamepad', {
        connected: false,
        name: null,
      })
    })
  }

  private getPadName(padId: string): string {
    if (padId.includes('Xbox')) {
      return 'xbox'
    }
    if (padId.includes('DUALSHOCK')) {
      return 'ps'
    }
    if (padId.includes('STANDARD') || padId.includes('gamepad')) {
      return 'standard'
    }
    return 'unknown'
  }

  private defineKeys(): void {
    Object.entries(buttonsKeysMap).forEach(([key, value]) => {
      this.mergedInput.defineKey(
        PLAYER_INDEX, key, value as KeyCode, 
      )
    })
  }

  private checkInputsPressed(): void {
    const keys = Object.keys(buttonsKeysMap)

    keys.forEach((key) => {
      if (['UP', 'DOWN', 'LEFT', 'RIGHT'].includes(key)) {
        if (this.player.direction[key as 'UP'| 'DOWN'|'LEFT'|'RIGHT'] > 0) {
          EventBus.emit('direction-pressed', key)
          return
        }
      }
      if (this.player.interaction_mapped.isPressed(key)) {
        EventBus.emit('input-pressed', key)
      }
    })
  }
}