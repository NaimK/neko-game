import BaseScene from '../BaseScene'

import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import ModalsController from '~/controllers/ModalsController'
import EventBus from '~/core/EventBus'
import Ship from '~/entities/ship/Ship'
import ShipThrust from '~/entities/ship/ShipThrust'

export default class Level1Intro extends BaseScene {
  public modalsController!: ModalsController

  private ship!: Ship

  private shipThrust!: ShipThrust

  private shipStartX: number = UI_PADDING  * 2

  private shipStartY: number = GAME_HEIGHT - UI_PADDING

  private skylineA!: Phaser.GameObjects.TileSprite

  private bgFar!: Phaser.GameObjects.TileSprite

  private leaveTimer!: Phaser.Time.TimerEvent

  constructor() {
    super('level-1-intro')
    this.modalsController = new ModalsController(this)
  }

  // LIFECYCLE HOOKS

  protected async create(): Promise<void> {
    super.create()
    
    this.playMusic()
    this.addSkyline()
    this.animateSkyline()
    this.addShip()
    this.playShipAnimation()
    this.flyIn()
    this.showPressAnyKey(this.t('press_key_to_skip'))

    this.leaveTimer = this.time.addEvent({
      delay: 20000,
      callback: this.leave,
      callbackScope: this,
      loop: false,
    })

    await this.wait(5000)

    this.modalsController.showModal({
      text: this.t('level_1_intro_cut'),
      size: 'big',
      type: 'alt',
      hideDelay: 15000,
    })
  }

  // METHODS

  protected bindListeners(): void {
    super.bindListeners()

    EventBus.on('input-pressed', (key: string) => {
      if (this.scene.isActive()) {
        this.handleAnyKey()
      }
    })
    EventBus.on('pointer-down', () => {
      this.handleAnyKey()
    })

    this.events.on('shutdown', function() {
      // Faire quelque chose lorsque la scène est quittée
      EventBus.emit('intro-shutdown', 'level-1-intro')
    })
  }

  protected removeListeners(): void {
    EventBus.off('pointer-down')
  }

  private async handleAnyKey(): Promise<void> {
    this.removeListeners()
    this.leave()
    this.leaveTimer.remove()
  }

  private playMusic(): void {
    this.sound.play('music-level-1', {
      loop: true,
      volume: 0.4, 
    })
  }

  private addSkyline(): void {
    this.skylineA = this.scene.scene.add.tileSprite(
      0, 0, GAME_WIDTH * 16, GAME_HEIGHT, 'skyline-a',
    )
    this.bgFar = this.scene.scene.add.tileSprite(
      0, GAME_HEIGHT - 124, GAME_WIDTH * 16, 124, 'buildings-bg',
    )

    this.skylineA.setOrigin(0, 0).setAlpha(0.7)
    this.bgFar.setOrigin(0, 0)
  }

  private animateSkyline(): void {
    this.scene.scene.tweens.add({
      targets: this.skylineA,
      x: -1 * this.skylineA.width,
      ease: 'Linear',
      duration: 180000,
      repeat: -1,
      yoyo: false,
    })

    this.scene.scene.tweens.add({
      targets: this.bgFar,
      x: (- 1 * this.bgFar.width) + GAME_WIDTH,
      ease: 'Linear',
      duration: 50000,
      repeat: -1,
      yoyo: false,
    })
  }

  private addShip(): void {
    this.ship = new Ship(
      this, -200, this.shipStartY,
    )
      .setOrigin(0, 1)

    this.shipThrust = new ShipThrust(
      this, -200, this.shipStartY - 37,
    )

    this.add.existing(this.shipThrust)
    this.add.existing(this.ship)
  }

  private async playShipAnimation(): Promise<void> {
    this.ship.play('ship-fly')
    this.shipThrust.play('ship-thrust')
  }

  private async flyIn(): Promise<void> {
    this.scene.scene.tweens.add({
      targets: [this.ship, this.shipThrust],
      x: this.shipStartX,
      ease: 'Linear',
      duration: 3000,
    })
  }

  private async leave(): Promise<void> {
    await this.wait(1000)

    this.sound.play('swoosh', { volume: 0.5 })

    if (this.modalsController.modal) {
      this.modalsController.clearModal()
    }

    this.flyAway()

    setTimeout(() => {
      this.tweens.killAll()
    }, 3000)

    this.goToScene('level')
  }

  private async flyAway(): Promise<void> {
    this.scene.scene.tweens.add({
      targets: [this.ship, this.shipThrust],
      x: GAME_WIDTH + 500,
      ease: 'Linear',
      duration: 3000,
    })
  }

  private wait(ms: number): Promise<void> {
    return new Promise(resolve => {
      setTimeout(resolve, ms)
    })
  }
}