import AssetsController from '~/controllers/AssetsController'

export default class Preloader extends Phaser.Scene {
  constructor() {
    super('preloader')
  }

  protected preload(): void {
    this.loadAssets()
  }

  protected create(): void {
    this.scene.start(import.meta.env.VITE_START_SCENE ?? 'splash-screen')
    this.scene.launch('mainInput')
  }

  private loadAssets(): void {
    const assetsController = new AssetsController(this)
    assetsController.loadAssets()
  }
}