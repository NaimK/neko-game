import VirtualJoyStick from 'phaser3-rex-plugins/plugins/virtualjoystick'
import VirtualJoystickPlugin from 'phaser3-rex-plugins/plugins/virtualjoystick-plugin.js'

import { COLOR_PRIMARY_Ox, COLOR_SECONDARY_Ox } from '~/const/colors'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import EventBus from '~/core/EventBus'

export default class VirtualInputScene extends Phaser.Scene {
  private pressedButton : string | null = null

  private justPressedButton: string | null = null

  private joystick?: VirtualJoyStick

  private buttons: any[] = []

  constructor() {
    super('virtualInput')
  }

  // GETTERS

  public get isGoingLeft(): boolean {
    if (!this.joystick) return false

    return this.joystick?.left ?? false
  }

  public get isGoingRight(): boolean {
    return this.joystick?.right ?? false
  }

  public get isGoingUp(): boolean {
    return this.joystick?.up ?? false
  }

  public get isGoingDown(): boolean {
    return this.joystick?.down ?? false
  }

  public get justFired(): boolean {
    return this.justPressedButton === 'RC_S'
  }

  public get isJumping(): boolean {
    return this.pressedButton === 'RC_W'
  }

  public get justJumped(): boolean {
    return this.justPressedButton === 'RC_W'
  }

  // LYFECYCLE

  create() {
    this.addVirtualJoystick()
    this.addButtons()
    this.hideControls()
    this.bindListeners()
  }

  update() {
    if (this.justPressedButton) {
      setTimeout(() => {
        this.justPressedButton = null
      }, 5)
    }

    this.checkInputsPressed()
  }

  // METHODS

  private bindListeners(): void {
    EventBus.on('input-pressed', (key: string) => {
      this.pressedButton = key
      this.justPressedButton = key
    })
  
    EventBus.on('level-started', () => {
      this.showControls()
    })
    EventBus.on('level-ended', () => {
      this.hideControls()
    })
  }

  private addVirtualJoystick(): void {
    this.input.addPointer(2)

    const virtualJoystick = this.plugins.get('rexVirtualJoystick') as VirtualJoystickPlugin

    const baseDiameter = 80
    const thumbDiameter = 40

    const base = this.add.circle(
      0, 0, baseDiameter, COLOR_PRIMARY_Ox,
    )
      .setOrigin(0.5, 0.5)
      .setAlpha(0.1)

    const thumb = this.add.circle(
      0, 0, thumbDiameter, COLOR_SECONDARY_Ox,
    ).setAlpha(0.3)

    this.joystick = virtualJoystick.add(this, {
      x: baseDiameter,
      y: GAME_HEIGHT - baseDiameter,
      radius: thumbDiameter,
      base,
      thumb,
      dir: '4dir',
    })

    this.joystick.createCursorKeys()
  }

  private addButtons(): void {
    const radius = 25
    const buttonSize = radius * 2
    const y = GAME_HEIGHT * 3 / 4
    const gap = 25

    this.addButton(
      'RC_S', GAME_WIDTH - UI_PADDING - radius - buttonSize - gap, y, radius,
    )
    this.addButton(
      'RC_W', GAME_WIDTH - UI_PADDING - radius, y, radius,
    )

    this.input.on('pointerup', () => {
      this.pressedButton = null
    })
  }

  private addButton(
    key: string, x: number, y: number, radius = 25, 
  ): void {
    const graphics = this.add.graphics()
    graphics.fillStyle(COLOR_PRIMARY_Ox, 0.5)
    graphics.fillCircle(
      x, y, radius,
    )

    const button = this.add.rectangle(
      x, y, radius * 3, radius * 5, COLOR_PRIMARY_Ox,
    )
      .setOrigin(0.5, 0.5)
      .setAlpha(0.01)
      .setInteractive()

    button.on('pointerdown', () => {
      EventBus.emit('input-pressed', key)
    })

    this.buttons.push(button)
    this.buttons.push(graphics)
  }

  private hideControls(): void {
    this.joystick?.setVisible(false)
    this.buttons.forEach((button) => {
      button.setVisible(false)
    })
  }

  private showControls(): void {
    this.joystick?.setVisible(true)
    this.buttons.forEach((button) => {
      button.setVisible(true)
    })
  }

  private checkInputsPressed(): void {
    // if there's a direction pressed, emit it
    if (this.isGoingLeft) {
      EventBus.emit('direction-pressed', 'LEFT')
    }
    if (this.isGoingRight) {
      EventBus.emit('direction-pressed', 'RIGHT')
    }
    if (this.isGoingUp) {
      EventBus.emit('direction-pressed', 'UP')
    }
    if (this.isGoingDown) {
      EventBus.emit('direction-pressed', 'DOWN')
    }
  }
}