
import BaseScene from '../BaseScene'

import { LEVEL_HEIGHT, LEVEL_WIDTH } from '~/const/sizes'
import AttacksController from '~/controllers/AttacksController'
import CameraController from '~/controllers/CameraController'
import CheckpointsController from '~/controllers/CheckpointsController'
import EnemiesController from '~/controllers/EnemiesController'
import MapController from '~/controllers/MapController'
import ModalsController from '~/controllers/ModalsController'
import PlayerCollisionsController from '~/controllers/PlayerCollisionsController'
import { post } from '~/core/api'
import EventBus from '~/core/EventBus'
import CoinsController from '~/entities/collactables/coin/CoinsController'
import PlayerController from '~/entities/player/PlayerController'
import GameState from '~/store/game'

export default class Level extends BaseScene {  
  private mapController: MapController

  private collisionsController: PlayerCollisionsController

  private cameraController: CameraController

  private attacksController!: AttacksController

  private enemiesController!: EnemiesController

  private checkpointsController!: CheckpointsController

  public map!: Phaser.Tilemaps.Tilemap

  public player!: Phaser.Physics.Arcade.Sprite

  public playerController!: PlayerController
  
  public modalsController!: ModalsController

  private levelEnd!: { x: number, y: number}

  constructor() {
    super('level')

    this.mapController = new MapController(this)
    this.collisionsController = new PlayerCollisionsController(this)
    this.cameraController = new CameraController(this)
    this.modalsController = new ModalsController(this)
  }

  // GETTERS

  public get ground(): Phaser.Tilemaps.TilemapLayer {
    return this.mapController.ground
  }

  public get lava(): Phaser.Tilemaps.TilemapLayer {
    return this.mapController.lava
  }

  public get platforms(): Phaser.Tilemaps.TilemapLayer {
    return this.mapController.platforms
  }

  public get bridges(): Phaser.Tilemaps.TilemapLayer {
    return this.mapController.bridges
  }

  public get doors(): Phaser.Tilemaps.TilemapLayer {
    return this.mapController.doors
  }

  public get enemies(): Phaser.GameObjects.Sprite[] {
    return this.enemiesController.sprites
  }

  public get enemiesBullets(): Phaser.GameObjects.Sprite[] {
    return this.attacksController.enemiesBullets
  }

  // METHODS

  protected preload() {
    super.preload()
  }

  protected create(): void {
    this.scene.launch('ui')
    this.physics.world.gravity.y = 400

    this.setMap()
    this.setObjects()
    this.collisionsController.setCollisions()
    this.cameraController.setCamera(this.player)
    this.attacksController = new AttacksController(this)
    this.enemiesController = new EnemiesController(this)
    this.checkpointsController = new CheckpointsController(this)
    new CoinsController(this)
    this.setBoundaries()
    this.createPlay()
    this.bindListeners()

    // Do not move this line to the top of the method
    super.create()

    EventBus.emit('level-started')
  }

  private setMap(): void {
    this.mapController.setAll()
    this.map = this.mapController.getMap()
  }

  private setObjects(): void {
    this.playerController = new PlayerController(this, this.keys)
    this.player = this.playerController.getPlayer()
  }

  private setBoundaries(): void {
    this.physics.world.setBounds(
      0, 0, LEVEL_WIDTH, LEVEL_HEIGHT,
    )
    this.player.setCollideWorldBounds(true)
  }

  protected bindListeners(): void {
    super.bindListeners()

    EventBus.on('game-over', () => {
      this.scene.start('game-over')
    })
  }

  private async createPlay(): Promise<void> {
    if (GameState.get('isDemo')) {
      return
    }

    try {
      await post('/api/plays', { level: GameState.get('level') })
    } catch (error) {
      console.error(error)
    }
  }
  
  update(_: number, dt: number): void {
    this.playerController.update(dt)
    this.attacksController.update(dt)
    this.enemiesController.update(dt)
    this.checkpointsController.update(dt)
  }

  public shutdown(): void {
    // TODO: Free all resources here
  }
}