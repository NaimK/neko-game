// Reference available at: https://github.com/photonstorm/phaser3-examples/blob/master/public/src/input/keyboard/enter%20name.js

import MenuScene from './MenuScene'

import { COLOR_PRIMARY_Ox } from '~/const/colors'
import { FONT_ALT, FONT_MAIN, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import { patch } from '~/core/api'
import GameState from '~/store/game'
import { User } from '~/types/user'

export default class MainMenuNickname extends MenuScene {
  private chars = [
    [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' ],
    [ 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T' ],
    [ 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4' ],
    [ '5', '6', '7', '8', '9', '0', '.', '', '<', '>' ],
  ]

  private cursor = {
    x: 0,
    y: 0,
  }

  private textInput!: Phaser.GameObjects.BitmapText

  private focusBlock!: Phaser.GameObjects.Image

  private nickname = ''

  private nicknameLabel!: Phaser.GameObjects.BitmapText

  private nicknameMinLength = 5

  private nicknameMaxLength = 10

  private letterSpacing = 69

  private blockSpacingX = 53

  private blockSpacingY = 32

  private columns = 10

  private rows = 4
  
  constructor() {
    super('main-menu-nickname')
  }

  protected create(): void {
    super.create()
    this.showPrompt()
    this.setTextInput()
    this.setFocus()
    this.setKeyboard()
  }

  private showPrompt(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2,
      UI_PADDING * 1.5,
      FONT_MAIN,
      this.t('menu_nickname_prompt'),
      FONT_SIZE_SMALL,
    ).setOrigin(0.5)
  }

  private setFocus(): void {
    this.focusBlock = this.add.image(
      this.textInput.x, this.textInput.y, 'keyboard-focus-block',
    )
      .setScale(0.6)
      .setOrigin(0.25)
  }

  setTextInput(): void {
    this.textInput = this.add.bitmapText(
      UI_PADDING,
      UI_PADDING * 3,
      FONT_ALT,
      'ABCDEFGHIJ\n\nKLMNOPQRST\n\nUVWXYZ1234\n\n567890.',
      FONT_SIZE_MEDIUM,
    )
      .setLetterSpacing(this.letterSpacing)
      .setOrigin(0, 0)

    this.textInput.setInteractive()

    this.nicknameLabel = this.add.bitmapText(
      UI_PADDING,
      GAME_HEIGHT - UI_PADDING,
      FONT_MAIN,
      this.nickname,
      FONT_SIZE_MEDIUM,
    )
      .setTint(COLOR_PRIMARY_Ox)
      .setOrigin(0, 1)
  }

  setKeyboard(): void {
    const { cursor, textInput, focusBlock } = this
    let { nickname } = this

    this.add.image(
      // position on the right of the last letter
      this.textInput.x + (this.blockSpacingX * 8),
      this.textInput.y + (this.blockSpacingY * (this.rows - 1)) - 2,
      'keyboard-rub',
    )
      .setOrigin(0)
      .setScale(0.08)
      
    this.add.image(
      // position on the right of the last letter
      this.textInput.x + (this.blockSpacingX * 9),
      this.textInput.y + (this.blockSpacingY * (this.rows - 1)),
      'keyboard-end',
    )
      .setOrigin(0)
      .setScale(0.6)

    this.input.keyboard?.on('keyup', (event: { keyCode: number }) => {
      switch (event.keyCode) {
      case 37: // left
        if (cursor.x > 0) {
          cursor.x--
          focusBlock.x -= this.blockSpacingX
        }
        break
      case 39: // right
        if (cursor.x < this.columns - 1) {
          cursor.x++
          focusBlock.x += this.blockSpacingX
        }
        break
      case 38: // up
        if (cursor.y > 0) {
          cursor.y--
          focusBlock.y -= this.blockSpacingY
        }
        break
      case 40: // down
        if (cursor.y < this.rows - 1) {
          cursor.y++
          focusBlock.y += this.blockSpacingY
        }
        break
      case 13: // enter
      case 32: // space
        if (cursor.x === this.columns - 2 && cursor.y === this.rows - 1) {
          // delete
          nickname = nickname.slice(0, -1)

          this.nicknameLabel.setText(nickname)
        } else if (cursor.x === this.columns - 1 && cursor.y === this.rows - 1) {
          if (nickname.length >= this.nicknameMinLength) {
            this.saveNickname(nickname)
          }
        } else if (nickname.length < this.nicknameMaxLength) {
          // add
          nickname = nickname.concat(this.chars[cursor.y][cursor.x])

          this.nicknameLabel.setText(nickname)
        }
        break
      default:
        break
      }
    })

    textInput.on(
      'pointermove', (
        _pointer: any, x: number, y: number,
      ) => {
        const cx = Phaser.Math.Snap.Floor(
          x, this.blockSpacingX, 0, true,
        )
        const cy = Phaser.Math.Snap.Floor(
          y, this.blockSpacingY, 0, true,
        )

        cursor.x = cx
        cursor.y = cy
        focusBlock.x = textInput.x + (cx * this.blockSpacingX)
        focusBlock.y = textInput.y + (cy * this.blockSpacingY)
      }, this,
    )

    textInput.on(
      'pointerup', (
        _pointer: any, x: number, y: number,
      ) => {
        const cx = Phaser.Math.Snap.Floor(
          x, this.blockSpacingX, 0, true,
        )
        const cy = Phaser.Math.Snap.Floor(
          y, this.blockSpacingY, 0, true,
        )
        const char = this.chars[cy][cx]

        cursor.x = cx
        cursor.y = cy
        focusBlock.x = textInput.x + (cx * this.blockSpacingX)
        focusBlock.y = textInput.y + (cy * this.blockSpacingY)

        if (char === '<' && nickname.length > 0) {
        //  Rub
          nickname = nickname.substring(0, nickname.length - 1)
          this.nicknameLabel.text = nickname
        } else if (char === '>' && nickname.length >= this.nicknameMinLength) {
          this.saveNickname(nickname)
        } else if (nickname.length < this.nicknameMaxLength) {
        //  Add
          nickname = nickname.concat(char)
          this.nicknameLabel.text = nickname
        }
      }, this,
    )
  }

  private async saveNickname(nickname: string): Promise<void> {
    patch('/api/user', { name: nickname }).then((_res) => {
      const user = GameState.get('user') as User
      GameState.set('user', {
        ...user,
        name: nickname,
      })
      this.goBack()
    }).catch((e) => {
      this.handleError(e as Error)
    })
  }

  private handleError(e: Error): void {
    this.add.bitmapText(
      GAME_WIDTH - UI_PADDING,
      GAME_HEIGHT - UI_PADDING,
      FONT_MAIN,
      e.message,
      FONT_SIZE_SMALL,
    )
      .setOrigin(1)
  }
}