import MergedInput from 'phaser3-merged-input'

import MergedInputScene from './MergedInput'
import VirtualInputScene from './VirtualInput'

import EventBus from '~/core/EventBus'
import UserAgentPlugin from '~/plugins/user-agent'

export default class MainInput extends Phaser.Scene {
  private activeInput?: MergedInputScene | VirtualInputScene

  private isMobile!: boolean
  
  constructor() {
    super('mainInput')
  }

  // GETTERS

  public get isGoingLeft(): boolean {
    return this.activeInput?.isGoingLeft ?? false
  }

  public get isGoingRight(): boolean {
    return this.activeInput?.isGoingRight ?? false
  }

  public get isGoingUp(): boolean {
    return this.activeInput?.isGoingUp ?? false
  }

  public get isGoingDown(): boolean {
    return this.activeInput?.isGoingDown ?? false
  }

  public get justFired(): boolean {
    return this.activeInput?.justFired ?? false
  }

  public get pressedMenuValidation(): boolean {
    if (this.activeInput instanceof MergedInputScene) {
      return this.activeInput?.pressedMenuValidation
    }
    return false
  }

  public get pressedMenuCancel(): boolean {
    if (this.activeInput instanceof MergedInputScene) {
      return this.activeInput.pressedMenuCancel
    }
    return false
  }

  public get isJumping(): boolean {
    return this.activeInput?.isJumping ?? false
  }

  public get justJumped(): boolean {
    return this.activeInput?.justJumped ?? false
  }

  public get justPressedPause(): boolean {
    if (this.activeInput instanceof MergedInputScene) {
      return this.activeInput?.justPressedPause
    }
    return false
  }

  // LIFECYCLE

  protected preload(): void {
    this.load.scenePlugin('mergedInput', MergedInput)
  }

  protected create(): void {    
    const userAgent = this.plugins.get('UserAgentPlugin') as UserAgentPlugin
    this.isMobile = userAgent.isMobile

    this.bindListeners()
    this.initActiveInput()
  }

  // METHODS

  private bindListeners(): void {
    this.input.on('pointerdown', () => {
      EventBus.emit('pointer-down')
    })
  }

  private initActiveInput() {
    if (this.isMobile) {
      this.activeInput = this.scene.get('virtualInput') as VirtualInputScene
      this.scene.launch('virtualInput')
    } else {
      this.activeInput = this.scene.get('mergedInput') as MergedInputScene
      this.scene.launch('mergedInput')
    }
  }
}
