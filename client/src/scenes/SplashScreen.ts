import MenuScene from './MenuScene'

import { FONT_MAIN } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import EventBus from '~/core/EventBus'
import UserAgentPlugin from '~/plugins/user-agent'
import GameState from '~/store/game'

export default class SplashScreen extends MenuScene {
  private isMobile!: boolean

  private pressedDemoButton = false

  constructor() {
    super('splash-screen')
  }

  // LIFECYCLE

  preload(): void {
    super.preload()
  }

  protected create(): void {
    super.create()

    this.setBackground(true)
    this.showSplashScreen()
    this.addMusic()
    if (!this.sound.locked) {
      this.playMusic()
    }

    const userAgent = this.plugins.get('UserAgentPlugin') as UserAgentPlugin
    this.isMobile = userAgent.isMobile

    if (this.isMobile) {
      this.addDemoButton()
    }

    this.bindListeners()
  }

  // METHODS

  protected bindListeners(): void {
    super.bindListeners()
    
    setTimeout(() => {
      EventBus.on('input-pressed', (key: string) => {    
        if (!this.scene.isActive()) {
          return
        }

        if (key === 'SELECT') {
          this.startDemo()
        } else {
          this.handleAnyKey()
        }
        this.removeListeners()
      })

      // on screen touch
      EventBus.on(
        'pointer-down', () => {
          setTimeout(() => {
            if (!this.scene.isActive()) {
              return
            }
            if (this.pressedDemoButton) { 
              this.startDemo()
            } else {
              this.handleAnyKey()
              this.removeListeners()
            }
          })
        }, 100,
      )
    }, 500)

    this.sound.once(Phaser.Sound.Events.UNLOCKED, () => {
      console.log('Phaser.Sound.Events.UNLOCKED')
      this.playMusic()
    })
  }

  private removeListeners(): void {
    //
  }

  private handleAnyKey(): void {
    this.goToScene('main-menu')
  }

  protected addMusic(): void {
    MenuScene.music = this.sound.add('music-menu', {
      loop: true,
      volume: 0.75, 
    })
  }

  private playMusic(): void {
    MenuScene.music.play()
  }

  private showSplashScreen(): void {
    this.showLogo()
    this.showPressAnyKey([this.t('splash_press_key'), this.t('splash_press_demo_key')])
  }

  private showLogo(): void {
    this.add.image(
      GAME_WIDTH / 2, GAME_HEIGHT / 2, 'logo',
    ).setOrigin(0.5, 0.5)
  }

  private addDemoButton(): void {
    const width = 180

    this.addButton({
      scene: this,
      text: this.t('button_demo'),
      x: GAME_WIDTH - UI_PADDING - width / 2,
      y: UI_PADDING,
      width,
      onClick: () => {
        this.pressedDemoButton = true
        console.log('demo button pressed')
        console.log(this.pressedDemoButton)
      },
    })
  }

  private startDemo(): void {
    GameState.set('isDemo', true)
    this.fadeOutMusic()
    this.goToScene('level-1-intro')
  }
}