
import MenuScene from './MenuScene'

import { FONT_MAIN, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import { get } from '~/core/api'
import EventBus from '~/core/EventBus'
import Link from '~/entities/ui/Link'
import NftContractPlugin from '~/plugins/nft-contract'
import GameState from '~/store/game'
import { CypherNekoNFT } from '~/types/nft'

export default class MainMenuNfts extends MenuScene {
  nfts: CypherNekoNFT[] = []
  
  constructor() {
    super('main-menu-nfts')
  }

  public async preload(): Promise<void> {
    super.preload()
    this.load.image('genesis', 'assets/images/nfts/genesis.gif')
  }

  protected async create(): Promise<void> {
    super.create()

    this.nfts = GameState.get<CypherNekoNFT[]>('nfts')
    
    if (this.nfts.length === 0) {
      this.showMint()
      return
    }

    this.showNftsInfo()
  }

  private showMint(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2,
      GAME_HEIGHT / 2,
      FONT_MAIN,
      this.t('menu_nfts_no_nfts'),
      FONT_SIZE_MEDIUM,
    ).setOrigin(0.5)

    this.add.bitmapText(
      GAME_WIDTH / 2,
      GAME_HEIGHT / 2 + 20,
      FONT_MAIN,
      this.t('menu_nfts_mint_first'),
      FONT_SIZE_SMALL,
    ).setOrigin(0.5)

    this.addButton({
      scene: this,
      text: this.t('button_mint'),
      x: GAME_WIDTH / 2,
      y: GAME_HEIGHT / 2 + 40,
      width: 100,
      onClick: async () => {
        const nftContractPlugin = this.plugins.get('NftContractPlugin') as NftContractPlugin
        try {
          const txHash: string | void = await nftContractPlugin.mintNft()

          if (!txHash) {
            return
          }

          EventBus.emit('text-message', {
            text: this.t('menu_nfts_minting', { txHash: `${txHash.substring(0, 8)}...` }),
            type: 'alt',
            typewriter: false, 
            hideDelay: 5000,
            size: 'big',
          })

          new Link({
            scene: this,
            x: GAME_WIDTH / 2,
            y: GAME_HEIGHT - UI_PADDING,
            text: this.t('menu_nfts_minting_link'),
            url: `${import.meta.env.VITE_EXPLORER_URL}tx/${txHash}`,
          })
        } catch (error: any) {
          console.error('error', error)
          if (error.message) {
            EventBus.emit('text-message', {
              text: error.message,
              type: 'alt',
              typewriter: false, 
              hideDelay: 5000,
              size: 'big',
            })
          }
        }
      },
    })
  }

  private showNftsInfo(): void {
    this.showTitle()
    this.showNfts()
    this.showButtons()
  }

  private showTitle(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2,
      UI_PADDING,
      FONT_MAIN,
      this.t('menu_nfts_title'),
      FONT_SIZE_MEDIUM,
    ).setOrigin(0.5)
  }

  private showNfts(): void {
    this.nfts.forEach((nft, index) => {
      if (!nft.metadata) return

      if (typeof nft.metadata === 'string') {
        nft.metadata = JSON.parse(nft.metadata)
      }

      const { name, description, image } = nft.metadata

      this.add.bitmapText(
        GAME_WIDTH / 2,
        UI_PADDING + 20 + (index * 40),
        FONT_MAIN,
        `${name} #${nft.tokenId}`,
        FONT_SIZE_SMALL,
      ).setOrigin(0.5)

      this.add.bitmapText(
        GAME_WIDTH / 2,
        UI_PADDING + 20 + (index * 40) + 10,
        FONT_MAIN,
        description,
        FONT_SIZE_SMALL,
      ).setOrigin(0.5)

      const isGenesis = name === 'Genesis'

      if (!isGenesis) return

      // TODO: Handle non genesis NFTs

      // TODO: Show attributes

      this.add.image(
        GAME_WIDTH / 2,
        64 + (index + 1 * 50),
        'genesis',
      )
        .setOrigin(0.5)
        .setDisplaySize(40, 40)
    })
  }

  private showButtons(): void {
    if (!GameState.get('hasRefreshedNfts')) {
      this.showRefreshButton()
    }
    
    this.showBackButton()
  }

  private showRefreshButton(): void {
    const refreshButtonX = GameState.get('hasRefreshedNfts') ? 0 : GAME_WIDTH / 2 - 55

    this.addButton({
      scene: this,
      text: this.t('button_refresh_nfts'),
      x: refreshButtonX,
      y: GAME_HEIGHT - UI_PADDING,
      width: 110,
      onClick: async () => {
        const nfts = await get('/api/nfts/me?refresh=true')
        GameState.set('nfts', nfts)
        GameState.set('hasRefreshedNfts', true)
        this.scene.restart()
      },
    })
  }

  private showBackButton(): void {
    const backButtonX = GameState.get('hasRefreshedNfts') ? GAME_WIDTH / 2 : GAME_WIDTH / 2 + 55

    this.addButton({
      scene: this,
      text: this.t('button_back'),
      x: backButtonX,
      y: GAME_HEIGHT - UI_PADDING,
      width: 100,
      onClick: () => this.goBack(),
    })
  }
}