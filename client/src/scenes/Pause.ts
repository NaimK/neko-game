
import BaseScene from './BaseScene'

import { FONT_ALT, FONT_MAIN, FONT_SIZE_LARGE, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'

export default class Pause extends BaseScene {  
  constructor() {
    super('pause')
  }

  protected create(): void {
    super.create()
    
    this.bindListeners()
  }

  public bindListeners(): void {
    super.bindListeners()
  }
  
  private init(): void {
    // We have to call this here because scene.launch() doesn't call scene.create()
    this.create()

    this.addOverlay()
    this.addPauseText()
    this.sound.play('ui-bip')
  }

  private addPauseText(): void {
    const text = this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT / 2, FONT_ALT, this.t('pause'), FONT_SIZE_LARGE,
    ).setOrigin(0.5, 0.5)

    this.tweens.add({
      targets: text,
      alpha: 0,
      duration: 1000,
      ease: 'Power2',
      yoyo: true,
      repeat: -1,
    })

    this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT - UI_PADDING, FONT_MAIN, this.t('resume'), FONT_SIZE_SMALL,
    ).setOrigin(0.5, 0.5)
  }

  private addOverlay(): void {
    this.add.rectangle(
      0, 0, GAME_WIDTH, GAME_HEIGHT, 0x000000, 0.5,
    ).setOrigin(0, 0)
  }
}