import { COLOR_PRIMARY_Ox, COLOR_SECONDARY_Ox, COLOR_TERTIARY_Ox, COLOR_WHITE_Ox } from '~/const/colors'
import { DEFAULT_COUNTDOWN_DURATION } from '~/const/countdowns'
import { FONT_ALT, FONT_MAIN, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_WIDTH, UI_PADDING_SM } from '~/const/sizes'
import CountdownController from '~/controllers/CountdownController'
import EventBus from '~/core/EventBus'
import GameState from '~/store/game'

export default class UI extends Phaser.Scene {
  private scoreLabel!: Phaser.GameObjects.BitmapText

  private coinsLabel!: Phaser.GameObjects.BitmapText

  private livesLabel!: Phaser.GameObjects.BitmapText

  private healthBar!: Phaser.GameObjects.Graphics

  private countdown!: CountdownController
  
  constructor() {
    super('ui')
  }

  protected create(): void {
    this.setCountdown()
    this.setDefaultLabels()
    this.setCoinsLabel()
    this.setHealthBar(100)

    this.bindListeners()
  }

  private setCountdown(): void {
    const timerLabel = this.add.bitmapText(
      GAME_WIDTH / 2, UI_PADDING_SM, FONT_ALT, '0', FONT_SIZE_MEDIUM,
    )
      .setOrigin(0.5, 0)
      .setTint(
        COLOR_PRIMARY_Ox, COLOR_PRIMARY_Ox,
        COLOR_SECONDARY_Ox, COLOR_SECONDARY_Ox, 
      )

    this.countdown = new CountdownController(this, timerLabel)
    this.countdown.start(this.handleCountdownFinished.bind(this), DEFAULT_COUNTDOWN_DURATION)
  }

  private setDefaultLabels(): void {
    const { score } = GameState.getState()

    this.scoreLabel = this.add.bitmapText(
      GAME_WIDTH - UI_PADDING_SM,
      UI_PADDING_SM,
      FONT_MAIN,
      `SCORE${score}`,
      FONT_SIZE_SMALL,
    )
      .setOrigin(1, 0)
      .setTint(COLOR_SECONDARY_Ox)
      .setCharacterTint(
        5, 1, true, COLOR_WHITE_Ox,
      )
  }

  private setCoinsLabel(): void {
    // Add coin sprite to the scene
    const coin = this.add.sprite(
      GAME_WIDTH - UI_PADDING_SM - 18, UI_PADDING_SM + 10, 'coin',
    )
      .setOrigin(1, 0)
      .setScale(0.8)

    // Add coin label to the scene
    this.coinsLabel = this.add.bitmapText(
      GAME_WIDTH - UI_PADDING_SM, coin.y + 2, FONT_MAIN, '00', FONT_SIZE_SMALL,
    ).setOrigin(1, 0)
  }

  private setHealthBar(value: number): void {
    const percent = Phaser.Math.Clamp(
      value, 0, 100,
    ) / 100
    const healthBarWidth = 60
    const healthBarHeight = 7

    this.healthBar = this.add.graphics()

    this.healthBar.fillStyle(COLOR_WHITE_Ox, 1)
    this.healthBar.fillRect(
      UI_PADDING_SM, UI_PADDING_SM, healthBarWidth, healthBarHeight,
    )

    if (percent > 0) {
      const color = percent > 0.3 ? COLOR_PRIMARY_Ox : 0xff0000

      this.healthBar.fillStyle(color, 1)
      this.healthBar.fillRect(
        UI_PADDING_SM, UI_PADDING_SM, healthBarWidth * percent, healthBarHeight,
      )
    }
  }

  private handleHealthChange(value: number): void {
    this.setHealthBar(value)
  }

  private handleCountdownFinished(): void {
    this.scene.pause('level')
    this.scene.start('game-over')
  }

  private bindListeners(): void {
    EventBus.on(
      'game-state-changed', (data: { property: string, value: any}) => {
        this.handleGameStateChange(data)
      }, this,
    )

    EventBus.on('input-pressed', (key: string) => {
      if (key === 'START') {
        const gameIsPaused = GameState.getState().isPaused

        if (!gameIsPaused) {
          this.scene.pause('level')
          this.scene.pause('ui')
          this.scene.launch('pause')
          this.sound.pauseAll()
          GameState.set('isPaused', true)
        } else {
          this.scene.resume('level')
          this.scene.resume('ui')
          this.scene.stop('pause')
          this.sound.resumeAll()
          GameState.set('isPaused', false)
        }
      }
    })
  }

  private handleGameStateChange(data: { property: string, value: any}): void {
    console.log(data.property)
    
    switch (data.property) {
    case 'playerHealth':
      this.handleHealthChange(data.value)
      break
    case 'score':
      this.scoreLabel.text = `SCORE${data.value}`
      this.scoreLabel.setCharacterTint(
        5, this.scoreLabel.text.length - 5, true, COLOR_WHITE_Ox,
      )
      break
    case 'coins':
      this.coinsLabel.text = data.value.toString().padStart(2, '0')
      break
    default:
      break
    }
  }

  public update(): void {
    this.countdown.update()
  }

  // TODO: Make sure this is called when the game is over
  destroy(): void {
    console.log('destroying UI scene')

    EventBus.off(
      'game-state-changed', this.handleGameStateChange, this,
    )
  }
}