import BaseScene from './BaseScene'

import { FONT_MAIN, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import EventBus from '~/core/EventBus'
import GameState from '~/store/game'

export default class DemoRegister extends BaseScene {
  constructor() {
    super('demo-register')
  }

  protected create(): void {
    super.create()

    GameState.set('gameIsOver', true)

    this.showTitle()
    this.showText()
    this.showPressAnyKey([this.t('demo_register_press_key')])
  }

  protected showTitle(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2, UI_PADDING, FONT_MAIN, this.t('demo_register_title'), FONT_SIZE_MEDIUM,
    ).setOrigin(0.5, 0.5)
  }

  protected showText(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT / 2, FONT_MAIN, this.t('demo_register_text'), FONT_SIZE_SMALL, 1,
    ).setOrigin(0.5, 0.5)
  }

  protected bindListeners(): void {
    super.bindListeners()

    EventBus.on('input-pressed', (key: string) => {
      if (this.scene.isActive()) {
        this.handleAnyKey()
      }
    })

    EventBus.on('pointer-down', (key: string) => {
      if (this.scene.isActive()) {
        this.handleAnyKey()
      }
    })
  }

  protected removeListeners(): void {
    //
  }

  private handleAnyKey(): void {
    this.removeListeners()
    this.restartGame()
    // this.goToScene('splash-screen')
  }
}