
import MenuScene from './MenuScene'

import { FONT_MAIN, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import { get } from '~/core/api'
import EventBus from '~/core/EventBus'
import { truncateAddress } from '~/helpers/strings'
import AuthPlugin from '~/plugins/auth'
import WalletPlugin from '~/plugins/wallet'
import GameState from '~/store/game'
import { CypherNekoNFT } from '~/types/nft'
import { User } from '~/types/user'

export default class MainMenu extends MenuScene {  
  private wallet!: WalletPlugin

  private auth!: AuthPlugin
  
  constructor() {
    super('main-menu')
  }

  protected async create(): Promise<void> {
    super.create()
    await this.initWallet()
    this.bindListeners()
    this.showLogo()
    this.showWalletAddress()
    await this.showMainMenu()
  }

  private async initWallet(): Promise<void> {
    this.wallet = this.plugins.get('WalletPlugin') as WalletPlugin
    this.auth = this.plugins.get('AuthPlugin') as AuthPlugin

    if (this.auth.isLoggedIn) {
      const storedNfts = GameState.get<CypherNekoNFT[]>('nfts')

      if (storedNfts.length === 0) {
        const nfts = await get<CypherNekoNFT[]>('/api/nfts/me')
        GameState.set('nfts', nfts)
      }
    }
  }

  protected bindListeners(): void {
    EventBus.on(
      'wallet-connected', this.handleConnected, this,
    )
    EventBus.on(
      'wallet-disconnected', this.handleDisconnected, this,
    )
    EventBus.on(
      'user-authenticated', this.handleAuthenticated, this,
    )
  }

  private handleConnected(): void {
    this.scene.restart()
  }

  private handleDisconnected(): void {
    this.scene.restart()
  }

  private handleAuthenticated(): void {
    console.log('authenticated')
    this.scene.restart()
  }

  private showLogo(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2, 
      UI_PADDING, 
      FONT_MAIN,
      this.t('main_menu_title'), 
      FONT_SIZE_MEDIUM,
    )
      .setOrigin(0.5, 0)
  }

  private showWalletAddress(): void {
    if (!this.auth.isLoggedIn) return

    const address = GameState.get<{address: string, chainId: number}>('wallet').address
    const formattedAddress = truncateAddress(address)

    this.add.bitmapText(
      GAME_WIDTH / 2,
      UI_PADDING + 24,
      FONT_MAIN,
      formattedAddress,
      FONT_SIZE_SMALL,
    ).setOrigin(0.5, 0)
  }

  private showMainMenu(): void {
    this.addNftButton()

    this.addButton({
      scene: this,
      text: this.t('button_scores'),
      x: GAME_WIDTH / 2,
      y: GAME_HEIGHT - UI_PADDING - 50,
      width: 120,
      onClick: () => {
        this.goToScene('main-menu-scores')
      },
    })

    this.addButton({
      scene: this,
      text: this.t('button_start'),
      x: GAME_WIDTH / 2,
      y: GAME_HEIGHT - UI_PADDING - 25,
      width: 120,
      onClick: () => {
        this.goToNextScene()
      },
      disabled: !this.auth.isLoggedIn || !this.wallet.hasNfts,
    })

    this.addWalletButton()

    this.selectFirstButton()
  }

  private addNftButton(): void {
    if (!this.auth.isLoggedIn) return

    this.addButton({
      scene: this,
      text: this.t('button_nfts'),
      x: GAME_WIDTH / 2,
      y: GAME_HEIGHT - UI_PADDING - 75,
      width: 120,
      onClick: () => {
        this.goToScene('main-menu-nfts')
      },
    })
  }

  private addWalletButton(): void {
    const connected = this.wallet.isConnected
    const isLoggedIn = this.auth.isLoggedIn

    const tr = this.t(`button_${connected && isLoggedIn ? 'connected' : !connected ? 'connect': 'login'}`)
        
    const fctConnectWallet = () => {
      this.wallet.connectWallet()
    }

    const fctLogin = async () => {
      const onlyWhitelisted = this.wallet.onlyWhitelisted
      const isAuthorized = this.wallet.isAuthorized
      let isWhitelisted

      console.log('wallet is authorized', this.wallet.isAuthorized)

      if (onlyWhitelisted) {
        const wallet = GameState.get('wallet') as { address: string }

        if (typeof isAuthorized === 'undefined') {
          isWhitelisted = await this.wallet.checkIsWhitelisted(wallet.address)
        }

        // TODO: this message shows up even if the user is whitelisted
        // FIX THIS
        if (!isWhitelisted) {
          EventBus.emit('text-message', {
            text: this.t('main_menu_wallet_not_whitelisted', { address: truncateAddress(wallet.address) }),
            type: 'alt',
            typewriter: false, 
            hideDelay: 5000,
            size: 'big',
          })
          return
        }
      }

      const wallet = GameState.get('wallet') as {
        address: string
        chainId: number
      }

      this.auth.authenticate({
        address: wallet.address,
        chainId: wallet.chainId,
        provider: this.wallet.provider,
      })
    }

    this.addButton({
      scene: this,
      text: tr,
      x: GAME_WIDTH / 2,
      y: GAME_HEIGHT - UI_PADDING,
      width: 120,
      onClick: connected ? fctLogin : fctConnectWallet,
      disabled: connected && isLoggedIn || !this.wallet.isActive,
    })
  }

  protected goToScene(name: string): void {
    this.resetButtons()
    super.goToScene(name)
  }

  protected goToNextScene(): void {
    this.resetButtons()

    const user = GameState.get('user') as User

    if (!user.name) {
      this.goToScene('main-menu-nickname')
      return
    }

    this.fadeOutMusic()

    this.goToScene('level-1-intro')
  }
}