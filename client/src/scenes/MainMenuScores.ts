import { Column } from 'phaser-ui-tools'

import MenuScene from './MenuScene'

import { FONT_MAIN, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import { get } from '~/core/api'
import AuthPlugin from '~/plugins/auth'

export default class MainMenuScores extends MenuScene {
  private auth!: AuthPlugin

  private scores: any[] = []

  private playerBestScore!: { score: string, position: number }
  
  constructor() {
    super('main-menu-scores')
  }

  protected async create(): Promise<void> {
    super.create()

    this.auth = this.plugins.get('AuthPlugin') as AuthPlugin

    await this.getScores()
    this.showBestScores()
    this.showBackButton()

    if (this.auth.isLoggedIn && this.playerBestScore) {
      this.showPlayerBestScore()
    }
  }

  private async getScores(): Promise<void> {
    this.scores = await get('/api/scores/best')

    if (this.auth.isLoggedIn) {
      const result = await get<any>('/api/scores/me/best')
      this.playerBestScore = result.content || null  
    }
  }

  private showBestScores(): void {
    const column = new Column(
      this, this.auth.isLoggedIn && this.playerBestScore ? UI_PADDING : GAME_WIDTH / 4 + UI_PADDING, UI_PADDING,
    )

    // Add transparent rectangle that is 50% of the screen width
    column.addNode(this.add.rectangle(
      0, 0, GAME_WIDTH / 2 - UI_PADDING * 2, 1, 0xFFFFFF, 0,
    ))

    column.addNode(this.add.bitmapText(
      0,
      20,
      FONT_MAIN,
      this.t('menu_scores_best_all'),
      10,
    ))

    this.scores.forEach(score => {
      const node = this.add.bitmapText(
        0, 0, FONT_MAIN, `${score.rank}. ${score.score} - ${score.username}`, FONT_SIZE_SMALL,
      )

      column.addNode(
        node, 0, 10, Phaser.Display.Align.BOTTOM_CENTER,
      )
    })
  }

  private showPlayerBestScore(): void {
    const column = new Column(
      this, GAME_WIDTH / 2 + UI_PADDING, UI_PADDING,
    )

    // Add transparent rectangle that is 50% of the screen width
    column.addNode(this.add.rectangle(
      0, 0, GAME_WIDTH / 2 - UI_PADDING * 2, 1, 0xFFFFFF, 0,
    ))

    column.addNode(this.add.bitmapText(
      0, 0, FONT_MAIN, this.t('menu_score_best_player'), 10,
    ))

    const text = `${this.t('menu_scores_ranking')}: ${this.playerBestScore.position}\n\n${this.t('menu_scores_score')}: ${this.playerBestScore.score}`

    const node = this.add.bitmapText(
      0, 0, FONT_MAIN, text, FONT_SIZE_SMALL, 1,
    )

    column.addNode(
      node, 0, 10, Phaser.Display.Align.BOTTOM_CENTER,
    )
  }

  private showBackButton(): void {
    this.addButton({
      scene: this,
      text: this.t('button_back'),
      x: GAME_WIDTH / 2,
      y: GAME_HEIGHT - UI_PADDING,
      width: 100,
      onClick: () => this.goBack(),
    })
  }
}