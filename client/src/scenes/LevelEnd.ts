
import BaseScene from './BaseScene'

import { FONT_MAIN, FONT_SIZE_LARGE, FONT_SIZE_MEDIUM, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import { post } from '~/core/api'
import EventBus from '~/core/EventBus'
import GameState from '~/store/game'

export default class LevelEnd extends BaseScene {
  private score = 0

  private coins = 0
  
  private enemiesKilled = 0
  
  private lives = 0
  
  private totalScore = 0
  
  private coinsMultiplier = 5

  private enemiesKilledMultiplier = 1

  private livesMultiplier = 10
  
  constructor() {
    super('level-end')
  }

  protected create(): void {      
    super.create()

    this.calculateScore()
    this.showTitle()
    this.showScore()
    this.saveStats().then(() => {
      this.saveScore().then (() => {
        this.showPressAnyKey([this.t('level_end_press_key')])
      })
    }).catch((e) => {
      console.error(e)
    })
  }

  protected bindListeners(): void {
    super.bindListeners()

    setTimeout(() => {
      EventBus.on('input-pressed', (key: string) => {
        if (this.scene.isActive()) {
          this.handleAnyKey()
        }
      })

      EventBus.on('pointer-down', (key: string) => {
        if (this.scene.isActive()) {
          this.handleAnyKey()
        }
      })
    }, 1000)
  }

  protected removeListeners(): void {
    //
  }

  private handleAnyKey(): void {
    this.removeListeners()

    if (GameState.get('isDemo')) {
      this.goToScene('demo-register')
      return
    }

    this.restartGame()
  }

  private calculateScore(): void {
    this.score = GameState.get('score')
    this.coins = GameState.get('coins')
    this.enemiesKilled = GameState.get('enemiesKilled')
    this.lives = GameState.get('lives')

    this.totalScore = this.score 
      + (this.coins * this.coinsMultiplier) 
      + (this.enemiesKilled * this.enemiesKilledMultiplier) 
      + (this.lives * this.livesMultiplier)
  }

  private showTitle(): void {
    this.add.bitmapText(
      GAME_WIDTH / 2, UI_PADDING, FONT_MAIN, this.t('level_end_title'), FONT_SIZE_MEDIUM,
    ).setOrigin(0.5, 0.5)
  }

  private showScore(): void {
    const coinLabel = this.getLabel(
      'coins', this.coins, this.coinsMultiplier,
    )
    const enemiesKilledLabel = this.getLabel(
      'enemies_killed', this.enemiesKilled, this.enemiesKilledMultiplier,
    )
    const livesLabel = this.getLabel(
      'lives', this.lives, this.livesMultiplier,
    )

    this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT / 3, FONT_MAIN, `${this.t('score')}: ${this.score}`, FONT_SIZE_MEDIUM,
    ).setOrigin(0.5, 0.5)

    // Display stats
    this.addStat(coinLabel, 0)
    this.addStat(enemiesKilledLabel, 15)
    this.addStat(livesLabel, 30)

    this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT - UI_PADDING * 3, FONT_MAIN, `${this.t('total_score')} ${this.totalScore}`, FONT_SIZE_LARGE,
    ).setOrigin(0.5, 0.5)
  }

  private async saveStats(): Promise<void> {
    if (GameState.get('isDemo')) {
      return
    }

    try {
      await post('/api/plays/stats', { 
        duration: GameState.get('completionTime'),
        coins: GameState.get('coins'),
        enemiesKilled: GameState.get('enemiesKilled'),
        lives: GameState.get('lives'),
        playerHealth: GameState.get('playerHealth'),
      })
    } catch (error) {
      console.error(error)
    }
  }

  private async saveScore(): Promise<void> {
    if (GameState.get('isDemo')) {
      return
    }

    try {
      post('/api/scores', {
        score: this.totalScore,
        level: GameState.get('level'), 
      })
    } catch (error) {
      console.error(error)
    }
  }

  private getLabel(
    label: string, value: number, multiplier: number,
  ): string {
    return `${this.t(label)}: ${value} x ${multiplier} = ${value * multiplier}`
  }

  private addStat(label: string, position: number): void {
    this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT / 2 + position, FONT_MAIN, label, FONT_SIZE_SMALL,
    ).setOrigin(0.5, 0.5)
  }
}