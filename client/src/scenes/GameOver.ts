
import BaseScene from './BaseScene'

import { FONT_ALT, FONT_SIZE_MEDIUM } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH } from '~/const/sizes'
import EventBus from '~/core/EventBus'

export default class GameOver extends BaseScene {
  constructor() {
    super('game-over')
  }

  protected create(): void {
    super.create()

    this.showTitle()
    this.showPressAnyKey([this.t('game_over_press_key')])
  }

  protected showTitle(): void {
    this.add.rectangle(
      GAME_WIDTH / 2, GAME_HEIGHT / 2, GAME_WIDTH, GAME_HEIGHT, 0x000000, 1,
    ).setOrigin(0.5, 0.5)
      
    this.add.bitmapText(
      GAME_WIDTH / 2, GAME_HEIGHT / 2, FONT_ALT, this.t('game_over'), FONT_SIZE_MEDIUM,
    ).setOrigin(0.5, 0.5)
  }

  protected bindListeners(): void {
    super.bindListeners()

    EventBus.on('input-pressed', (key: string) => {
      if (this.scene.isActive()) {
        this.handleAnyKey()
      }
    })
  }

  protected removeListeners(): void {
    //
  }

  private handleAnyKey(): void {
    this.removeListeners()
    this.restartGame()
  }
}