import MainInput from './MainInput'

import { FONT_MAIN, FONT_SIZE_SMALL } from '~/const/fonts'
import { GAME_HEIGHT, GAME_WIDTH, UI_PADDING } from '~/const/sizes'
import KeyboardController from '~/controllers/KeyboardController'
import EventBus from '~/core/EventBus'
import { BindedKeys, KEY_BINDINGS } from '~/core/keyBindings'

export default class BaseScene extends Phaser.Scene {
  public keys!: BindedKeys

  static readonly FADE_DURATION = 1000

  private intervals: Array<number>  = []

  private static previousScene: string

  private static currentScene: string

  public text!: {
    [key: string]: string
  }

  constructor(key: string) {
    super(key)
  }

  // GETTERS

  public get mainInput(): MainInput {
    return this.scene.get('mainInput') as MainInput
  }

  protected preload(): void {
    this.setKeys()
  }

  protected create(): void {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.text = this.localization?.translations
    this.cameras.main.fadeIn(
      500, 0, 0, 0,
    )

    this.bindListeners()
    this.useDevSettings()
  }

  protected restartGame(): void {
    // TODO: Make a clean restart
    window.location.reload()
  }

  // variables is an object with keys and values

  public t(key: string, variables?: object): string {
    if (variables) {
      let text = this.text[key]

      if (!text) {
        return `MISSING TRANSLATION : ${key}`
      }

      Object.entries(variables).forEach(([key, value]) => {
        console.log(key, value)
        text = text.replace(`{{${key}}}`, value)
      })

      return text
    }
    
    return this.text[key]
  }

  private setKeys(): void {
    this.keys = this.input.keyboard?.addKeys(KEY_BINDINGS) as BindedKeys
    new KeyboardController(this)
  }

  protected bindListeners(): void {
    EventBus.on(
      'wallet-disconnected', this.handleWalletDisconnected, this,
    )
  }

  protected showPressAnyKey(text?: string | Array<string>): void {
    if (!text) {
      text = this.t('splash_press_key')
    }

    if (Array.isArray(text)) {
      // Alternate between two texts

      const loadingLabel1 = this.add.bitmapText(
        GAME_WIDTH / 2, GAME_HEIGHT - UI_PADDING, FONT_MAIN, text[0], FONT_SIZE_SMALL,
      ).setOrigin(0.5, 0.5)

      const loadingLabel2 = this.add.bitmapText(
        GAME_WIDTH / 2, GAME_HEIGHT - UI_PADDING, FONT_MAIN, text[1], FONT_SIZE_SMALL,
      ).setOrigin(0.5, 0.5)

      loadingLabel1.visible = true
      loadingLabel2.visible = false
  
      // Make the text blink
      const interval1 = setInterval(() => {
        loadingLabel1.visible = !loadingLabel1.visible
      }, 2000)
      const interval2 = setInterval(() => {
        loadingLabel2.visible = !loadingLabel2.visible
      }, 2000)

      this.intervals.push(interval1)
      this.intervals.push(interval2)
    } else {
      const loadingLabel = this.add.bitmapText(
        GAME_WIDTH / 2, GAME_HEIGHT - UI_PADDING, FONT_MAIN, text, FONT_SIZE_SMALL,
      ).setOrigin(0.5, 0.5)
  
      // Make the text blink
      const interval = setInterval(() => {
        loadingLabel.visible = !loadingLabel.visible
      }, 750)

      this.intervals.push(interval)
    }
  }

  protected goToScene(name: string): void {
    if (name === BaseScene.currentScene) {
      console.warn(`Already in scene ${name}`)
      return
    }

    BaseScene.previousScene = BaseScene.currentScene ?? 'splash-screen'
    BaseScene.currentScene = name

    if (!this.scene.get(name)) {
      throw new Error(`Scene ${name} doesn't exist`)
      return
    }

    this.clearIntervals()

    this.cameras.main.fadeOut(
      BaseScene.FADE_DURATION, 0, 0, 0,
    )

    setTimeout(() => {
      this.scene.stop(this.scene.key)

      setTimeout(() => {
        this.scene.start(name)
      }, 100)
    }, BaseScene.FADE_DURATION)
  }

  protected clearIntervals(): void {
    this.intervals.forEach((interval) => {
      clearInterval(interval)
    })
  }

  protected goBack(): void {
    console.log(`Going back to scene ${BaseScene.previousScene}`)
    this.goToScene(BaseScene.previousScene)
  }

  private handleWalletDisconnected(): void {
    window.location.reload()
  }

  // TODO: Extract to a separate class (dev plugin)
  protected useDevSettings(): void {
    if (import.meta.env.VITE_MUTE_SOUND === 'true') {
      this.sound.mute = true
    } else {
      if (import.meta.env.VITE_MUTE_MUSIC === 'true') {
        setTimeout(() => {
          this.sound.stopByKey('music-menu')
          this.sound.stopByKey('music-level-1')
        }, 10)
      }
    }
    if (import.meta.env.VITE_LOG_GAME_STATE === 'true') {
      EventBus.on('game-state-changed', (data: { property: string, value: any}) => {
        console.log(`Game state changed: ${data.property} = ${data.value}`)
      })
    }
  }
}