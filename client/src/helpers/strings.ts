import { ethers } from 'ethers'

function truncateAddress(address: string) {
  try {
    const normalizedAddress = ethers.getAddress(address)
    return `${normalizedAddress.substring(0, 5)}...${normalizedAddress.substring(normalizedAddress.length - 4)}`
  } catch (error) {
    console.error(`Invalid Ethereum address: ${address}`)
    return address
  }
}

function normalizeAddress(address: string) {
  try {
    return ethers.getAddress(address)
  } catch (error) {
    console.error(`Invalid Ethereum address: ${address}`)
    return address
  }
}

export { truncateAddress, normalizeAddress }