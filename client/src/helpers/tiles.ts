export const getTileUnderPlayer = (
  player: Phaser.Physics.Arcade.Sprite, tilemapLayer: Phaser.Tilemaps.TilemapLayer, playerOffsetX = 0,
) => {
  const offsetBottom = 3
  const playerBottomY = player.y + (player.height / 2) + offsetBottom
  const tile = tilemapLayer.tilemap.getTileAtWorldXY(
    player.x + playerOffsetX, playerBottomY, undefined, undefined, tilemapLayer,
  )

  return {
    tile,
    position: 'bottom',
  }
}

export const getTileOnPlayersTop = (
  player: Phaser.Physics.Arcade.Sprite, tilemapLayer: Phaser.Tilemaps.TilemapLayer, playerOffsetX = 0,
) => {
  const offset = 3
  const tile = tilemapLayer.tilemap.getTileAtWorldXY(
    player.x + playerOffsetX, player.y - (player.height / 2) - offset, undefined, undefined, tilemapLayer,
  )

  return {
    tile,
    position: 'top',
  }
}

export const getTileOnPlayersLeft = (
  player: Phaser.Physics.Arcade.Sprite, tilemapLayer: Phaser.Tilemaps.TilemapLayer, playerOffsetX = 0,
) => {
  const offset = 3
  const tile = tilemapLayer.tilemap.getTileAtWorldXY(
    player.x + playerOffsetX - (player.width / 2) - offset, player.y, undefined, undefined, tilemapLayer,
  )

  return {
    tile,
    position: 'left',
  }
}

export const getTileOnPlayersRight = (
  player: Phaser.Physics.Arcade.Sprite, tilemapLayer: Phaser.Tilemaps.TilemapLayer, playerOffsetX = 0,
) => {
  const offset = 3
  const tile = tilemapLayer.tilemap.getTileAtWorldXY(
    player.x + playerOffsetX + (player.width / 2) + offset, player.y, undefined, undefined, tilemapLayer,
  )

  return {
    tile,
    position: 'right',
  }
}

export const getTileOnPlayersTopRight = (
  player: Phaser.Physics.Arcade.Sprite, tilemapLayer: Phaser.Tilemaps.TilemapLayer, playerOffsetX = 0,
) => {
  const offset = 3
  const tile = tilemapLayer.tilemap.getTileAtWorldXY(
    player.x + playerOffsetX + (player.width / 2) + offset, player.y - (player.height / 2) - offset, undefined, undefined, tilemapLayer,
  )

  return {
    tile,
    position: 'top-right',
  }
}

export const getTileOnPlayersTopLeft = (
  player: Phaser.Physics.Arcade.Sprite,
  tilemapLayer: Phaser.Tilemaps.TilemapLayer,
  playerOffsetX = 0,
): {
  tile: Phaser.Tilemaps.Tile,
  position: string,
} => {
  const offset = 3
  const tile = tilemapLayer.tilemap.getTileAtWorldXY(
    player.x + playerOffsetX - (player.width / 2) - offset, player.y - (player.height / 2) - offset, undefined, undefined, tilemapLayer,
  )

  return {
    tile,
    position: 'top-left', 
  }
}

export const getFirstCollidingTile = (
  player: Phaser.Physics.Arcade.Sprite,
  tilemapLayer: Phaser.Tilemaps.TilemapLayer, 
  options: {
    playerOffsetX?: number,
    playerOffsetY?: number,
    only: string[],
  } = { 
    playerOffsetX: 0,
    playerOffsetY: 0,
    only: [],
  },
): { tile: Phaser.Tilemaps.Tile, position: string } | null => {
  const fcts: ((
    player: Phaser.Physics.Arcade.Sprite,
    tilemapLayer: Phaser.Tilemaps.TilemapLayer
  ) => {
    tile: Phaser.Tilemaps.Tile,
    position: string,
  } | null)[] = []

  const fctsMap: {
    [key: string]: ((
      player: Phaser.Physics.Arcade.Sprite,
      tilemapLayer: Phaser.Tilemaps.TilemapLayer,
    ) => {
      tile: Phaser.Tilemaps.Tile,
      position: string,
    } | null)
  }
   = {
     'top': (...args) => getTileOnPlayersTop(...args),
     'bottom': (...args) => getTileUnderPlayer(...args),
     'left': (...args) => getTileOnPlayersLeft(...args),
     'right': (...args) => getTileOnPlayersRight(...args),
     'top-right': (...args) => getTileOnPlayersTopRight(...args),
     'top-left': (...args) => getTileOnPlayersTopLeft(...args),
   }

  if (options.only.length) {
    options.only.forEach((key) => {
      fcts.push(fctsMap[key])
    })
  } else {
    Object.values(fctsMap).forEach((fct) => {
      fcts.push(fct)
    })
  }

  let matchedTile: {
    tile: Phaser.Tilemaps.Tile,
    position: string,
  } | null = null

  fcts.every((fct) => {
    const result = fct(player, tilemapLayer)

    if (result?.tile) {
      matchedTile = result
      return false
    }
    return true
  })

  return matchedTile
}
