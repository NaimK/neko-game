const networks = {
  1: 'mainnet',
  5: 'goerli',
  11155111: 'sepolia',
}

export { networks }