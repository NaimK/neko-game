export const get = async <T>(endpoint: string, query?: object): Promise<T> => {
  let url = `${import.meta.env.VITE_BACKEND_URL}${endpoint}`

  // Parse query params
  if (query) {
    const params = new URLSearchParams(query as Record<string, string>)
    url += `?${params.toString()}`
  }

  const response = await fetch(url, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  })

  if (!response.ok) {
    const data = JSON.parse(await response.text())
    throw new Error(data.message)
  }

  // If response is ok without any content (204), return empty object
  if (response.status === 204) {
    return {} as T
  }

  return response.json()
}

export const post = async (endpoint: string, data: unknown) => {
  const url = `${import.meta.env.VITE_BACKEND_URL}${endpoint}`
  
  const response = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  })

  // if response status is of 400 or 500, throw error
  if (!response.ok) {
    const data = JSON.parse(await response.text())
    throw new Error(data.message)
  }

  return response.json()
}

export const patch = async (endpoint: string, data: unknown) => {
  const url = `${import.meta.env.VITE_BACKEND_URL}${endpoint}`
  
  const response = await fetch(url, {
    method: 'PATCH',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  })

  // if response status is of 400 or 500, throw error
  if (!response.ok) {
    const data = JSON.parse(await response.text())
    throw new Error(data.message)
  }

  return response.json()
}