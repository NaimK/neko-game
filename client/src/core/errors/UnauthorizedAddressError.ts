export default class UnauthorizedAddressError extends Error {
  constructor(message: string) {
    super(message)
    this.name = 'UnauthorizedAddressError'
  }
}
