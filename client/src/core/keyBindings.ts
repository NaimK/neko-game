const { KeyCodes } = Phaser.Input.Keyboard

export interface KeyBindings {
  UP: number
  RIGHT: number
  DOWN: number
  LEFT: number
  JUMP: number
  FIRE: number
  PAUSE: number
  ENTER: number
  D: number
}

export interface BindedKeys {
  UP: Phaser.Input.Keyboard.Key
  RIGHT: Phaser.Input.Keyboard.Key
  DOWN: Phaser.Input.Keyboard.Key
  LEFT: Phaser.Input.Keyboard.Key
  JUMP: Phaser.Input.Keyboard.Key
  FIRE: Phaser.Input.Keyboard.Key
  PAUSE: Phaser.Input.Keyboard.Key
  ENTER: Phaser.Input.Keyboard.Key
  D: Phaser.Input.Keyboard.Key
}

export const KEY_BINDINGS : KeyBindings = {
  UP: KeyCodes.UP,
  RIGHT: KeyCodes.RIGHT,
  DOWN: KeyCodes.DOWN,
  LEFT: KeyCodes.LEFT,
  JUMP: KeyCodes.CTRL,
  FIRE: KeyCodes.SPACE,
  PAUSE: KeyCodes.ESC,
  ENTER: KeyCodes.ENTER,
  D: KeyCodes.D,
}