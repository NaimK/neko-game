import { Scene } from 'phaser'

import { FONT_ALT, FONT_MAIN } from '~/const/fonts'
import UserAgentPlugin from '~/plugins/user-agent'

export default class AssetsController {
  private scene: Scene

  private userAgent!: UserAgentPlugin
  
  constructor(scene: Scene) {
    this.scene = scene
    this.userAgent = this.scene.plugins.get('UserAgentPlugin') as UserAgentPlugin
  }

  public loadAssets(): void {
    this.loadFonts()
    this.loadImages()
    this.loadAtlases()
    this.loadTileMapAssets()
    this.loadSounds()
  }

  private loadFonts(): void {
    this.scene.load.bitmapFont(
      FONT_ALT, 'assets/fonts/metal-slug.png', 'assets/fonts/metal-slug.xml',
    )
    this.scene.load.bitmapFont(
      FONT_MAIN, 'assets/fonts/pixel-operator.png', 'assets/fonts/pixel-operator.xml',
    )
  }

  private loadImages(): void {
    // MENUS
    this.scene.load.image('menu-bg-back', 'assets/images/menu/background/back.png')
    this.scene.load.image('menu-bg-far', 'assets/images/menu/background/far.png')
    this.scene.load.image('menu-bg-front', 'assets/images/menu/background/front.png')
    this.scene.load.image('menu-bg-middle', 'assets/images/menu/background/middle.png')
    this.scene.load.image('menu-bg-near', 'assets/images/menu/background/near.png')
    this.scene.load.image('keyboard-focus-block', 'assets/images/menu/block.png')
    this.scene.load.image('keyboard-rub', 'assets/images/menu/arrow-left.png')
    this.scene.load.image('keyboard-end', 'assets/images/menu/end.png')
    // UI
    this.scene.load.image('logo', 'assets/images/ui/logo.png')
    // this.scene.load.image('scroll-track', 'assets/images/ui/scroll-track.png')
    // this.scene.load.spritesheet(
    //   'scroll-bar', 'assets/images/ui/scroll-bar.png', {
    //     frameWidth: 11,
    //     frameHeight: 22,
    //   },
    // )

    // LEVEL
    this.scene.load.image('buildings-bg', 'assets/maps/assets/environment/background/buildings-bg.png')
    this.scene.load.image('near-buildings-bg', 'assets/maps/assets/environment/background/near-buildings-bg.png')
    this.scene.load.image('skyline-a', 'assets/maps/assets/environment/background/skyline-a.png')
    this.scene.load.image('skyline-b', 'assets/maps/assets/environment/background/skyline-b.png')
    
    this.scene.load.image('underground-bg', 'assets/maps/assets/environment/background/underground-bg.png')
    this.scene.load.image('underground-far', 'assets/maps/assets/environment/background/underground-far.png')
    
    // COINS
    this.scene.load.spritesheet(
      'coin', 'assets/sprites/coin.png', {
        frameWidth: 11,
        frameHeight: 12,
      },
    )
  }

  private loadAtlases(): void {
    // UI
    this.scene.load.atlas(
      'ui-buttons', 'assets/atlas/ui-buttons.png', 'assets/atlas/ui-buttons.json',
    )
    this.scene.load.atlas(
      'ui-modals', 'assets/atlas/ui-modals.png', 'assets/atlas/ui-modals.json',
    )
    // Player
    this.scene.load.atlas(
      'player', 'assets/atlas/neko.png', 'assets/atlas/neko.json',
    )
    // Enemies
    this.scene.load.atlas(
      'robot', 'assets/atlas/robot.png', 'assets/atlas/robot.json',
    )
    this.scene.load.atlas(
      'yellow-suit', 'assets/atlas/yellow-suit.png', 'assets/atlas/yellow-suit.json',
    )
    // Bullets
    this.scene.load.atlas(
      'laser', 'assets/atlas/laser.png', 'assets/atlas/laser.json',
    )
    this.scene.load.atlas(
      'robot-laser', 'assets/atlas/robot-laser.png', 'assets/atlas/robot-laser.json',
    )
    this.scene.load.atlas(
      'yellow-suit-laser', 'assets/atlas/yellow-suit-laser.png', 'assets/atlas/yellow-suit-laser.json',
    )
    // Other
    this.scene.load.atlas(
      'ship', 'assets/atlas/ship.png', 'assets/atlas/ship.json',
    )
    this.scene.load.atlas(
      'ship-thrust', 'assets/atlas/ship-thrust.png', 'assets/atlas/ship-thrust.json',
    )
    this.scene.load.atlas(
      'checkpoint', 'assets/atlas/checkpoint.png', 'assets/atlas/checkpoint.json',
    )
  }

  private loadTileMapAssets(): void {
    this.scene.load.image('tiles', 'assets/maps/assets/tilesets/tileset.png')
    this.scene.load.tilemapTiledJSON('level1', 'assets/maps/level-1.json')
  }

  private loadSounds(): void {
    // UI
    this.scene.load.audio('ui-bip', 'assets/sound/ui/pause.mp3')
    this.scene.load.audio('checkpoint', 'assets/sound/ui/checkpoint.mp3')
    this.scene.load.audio('level-end', 'assets/sound/ui/level-end.mp3')

    // FX
    this.scene.load.audio('swoosh', 'assets/sound/fx/swoosh.mp3')
    this.scene.load.audio('beam', this.getOggOrMp3('assets/sound/fx/beam'))
    this.scene.load.audio('explosion', this.getOggOrMp3('assets/sound/fx/explosion'))
    this.scene.load.audio('hurt', this.getOggOrMp3('assets/sound/fx/hurt'))
    this.scene.load.audio('jump', 'assets/sound/fx/jump.mp3')
    this.scene.load.audio('jump-land', 'assets/sound/fx/jump-land.mp3')
    this.scene.load.audio('coin-pickup', 'assets/sound/fx/coin-pickup.mp3')
    
    // Music
    // Menus
    this.scene.load.audio('music-menu', 'assets/sound/music/cyberpunk-street.mp3')
    // Level 1
    this.scene.load.audio('music-level-1', this.getOggOrMp3('assets/sound/music/sci_fi_platformer02'))
  }

  private getOggOrMp3(path: string): string {
    if (this.userAgent.isSafari || this.userAgent.isIOS) {
      return `${path}.mp3`
    }
    return `${path}.ogg`
  }
}