import EventBus from '~/core/EventBus'

export default class KeyboardController {
  private scene: Phaser.Scene
  
  constructor(scene: Phaser.Scene) {
    this.scene = scene

    this.bindListeners()
  }

  private bindListeners(): void {
    this.listenToKeys()
  }

  private listenToKeys(): void {
    this.scene.input.keyboard.on('keydown', (event: KeyboardEvent) => {
      EventBus.emit('keydown', event)
    })
  }
}