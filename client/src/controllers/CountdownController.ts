import EventBus from '~/core/EventBus'
import GameState from '~/store/game'

export default class CountdownController {
  scene: Phaser.Scene

  label: Phaser.GameObjects.BitmapText

  timerEvent: Phaser.Time.TimerEvent | undefined

  duration = 0

  finishedCallback: (() => void) | undefined

  constructor(scene: Phaser.Scene, label:  Phaser.GameObjects.BitmapText) {
    this.scene = scene
    this.label = label
    this.bindListeners()
  }

  private bindListeners(): void {
    EventBus.on(
      'level-ended', this.handleLevelEnded, this,
    )

    console.log('countdown controller bound')
  }

  public start(callback: () => void, duration = 45000): void {
    this.stop()

    this.finishedCallback = callback
    this.duration = duration

    this.timerEvent = this.scene.time.addEvent({
      delay: duration,
      callback: () => {
        this.label.text = '0'

        this.stop()
				
        if (callback) {
          callback()
        }
      },
    })
  }

  private stop(): void {
    if (this.timerEvent) {
      this.timerEvent.destroy()
      this.timerEvent = undefined
    }
  }

  public handleLevelEnded(): void {    
    let ellapsedTime = Math.round(this.getElapsedTime())
    ellapsedTime = Math.round(ellapsedTime / 1000)
    GameState.set('completionTime', ellapsedTime)
    
    this.stop()
  }

  public getElapsedTime(): number {
    if (!this.timerEvent) {
      return 0
    }

    return this.timerEvent.getElapsed()
  }

  public update(): void {
    if (!this.timerEvent || this.duration <= 0) {
      return
    }

    const elapsed = this.timerEvent.getElapsed()
    const remaining = this.duration - elapsed
    const seconds = remaining / 1000

    this.label.text = seconds.toFixed(0)
  }
}