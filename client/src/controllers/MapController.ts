import { BRIDGES_DEPTH, PIPES_DEPTH } from '~/const/depths'
import { GAME_HEIGHT, LEVEL_HEIGHT, LEVEL_WIDTH } from '~/const/sizes'
import Level from '~/scenes/levels/Level'

export default class MapController {
  private scene: Level

  private map!: Phaser.Tilemaps.Tilemap

  private groundLayer!: Phaser.Tilemaps.TilemapLayer

  private lavaLayer!: Phaser.Tilemaps.TilemapLayer

  private forefrontSetLayer!: Phaser.Tilemaps.TilemapLayer

  private platformsLayer!: Phaser.Tilemaps.TilemapLayer

  private pipesLayer!: Phaser.Tilemaps.TilemapLayer

  private bridgesLayer!: Phaser.Tilemaps.TilemapLayer

  private doorsLayer!: Phaser.Tilemaps.TilemapLayer
  
  constructor(scene: Level) {
    this.scene = scene
  }

  // GETTERS
  public get ground(): Phaser.Tilemaps.TilemapLayer {
    return this.groundLayer
  }

  public get lava(): Phaser.Tilemaps.TilemapLayer {
    return this.lavaLayer
  }

  public get platforms(): Phaser.Tilemaps.TilemapLayer {
    return this.platformsLayer
  }

  public get bridges(): Phaser.Tilemaps.TilemapLayer {
    return this.bridgesLayer
  }

  public get doors(): Phaser.Tilemaps.TilemapLayer {
    return this.doorsLayer
  }

  public setAll(): void {
    this.setBackground()
    this.setMap()
  }

  public setBackground(): void {
    this.setSkyline()
    this.setUnderground()
  }

  private setSkyline(): void {
    const skylineA = this.scene.add.tileSprite(
      0, -20, LEVEL_WIDTH, 240, 'skyline-a',
    )
    const bgFar = this.scene.add.tileSprite(
      0, 80, LEVEL_WIDTH, 124, 'buildings-bg',
    )
    const bgNear = this.scene.add.tileSprite(
      0, 50, LEVEL_WIDTH, 209, 'near-buildings-bg',
    )

    skylineA.setOrigin(0, 0)
    skylineA.setScrollFactor(0.2)
    bgFar.setOrigin(0, 0)
    bgFar.setScrollFactor(0.3)
    bgNear.setOrigin(0, 0)
    bgNear.setScrollFactor(0.75)
  }

  private setUnderground(): void {
    const undergroundBg = this.scene.add.tileSprite(
      0, GAME_HEIGHT, LEVEL_WIDTH, LEVEL_HEIGHT - GAME_HEIGHT, 'underground-bg',
    )
    
    undergroundBg.setOrigin(0, 0)

    const undergroundFar = this.scene.add.tileSprite(
      0, GAME_HEIGHT * 2, LEVEL_WIDTH, LEVEL_HEIGHT - GAME_HEIGHT, 'underground-far',
    )

    undergroundFar.setOrigin(0, 0)
  }

  public setMap():void  {
    this.map = this.scene.make.tilemap({ key: 'level1' })

    const tileset = this.map.addTilesetImage('tileset', 'tiles')

    this.groundLayer = this.map.createLayer('Tiles/ground', tileset)
    this.lavaLayer = this.map.createLayer('Tiles/lava', tileset)
    this.forefrontSetLayer = this.map.createLayer('Tiles/forefront-set', tileset)
    this.platformsLayer = this.map.createLayer('Tiles/platforms', tileset)
    this.pipesLayer = this.map.createLayer('Tiles/pipes', tileset).setDepth(PIPES_DEPTH)
    this.bridgesLayer = this.map.createLayer('Tiles/bridges', tileset).setDepth(BRIDGES_DEPTH)
    this.doorsLayer = this.map.createLayer('Tiles/doors', tileset)
  }

  public getMap(): Phaser.Tilemaps.Tilemap {
    return this.map
  }
}