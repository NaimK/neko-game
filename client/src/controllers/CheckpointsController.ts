import { post } from '~/core/api'
import EventBus from '~/core/EventBus'
import Checkpoint from '~/entities/Checkpoint'
import Level from '~/scenes/levels/Level'
import GameState from '~/store/game'

export default class CheckpointsController {
  private scene: Level

  private checkpoints: Checkpoint[] = []

  private currentCheckpointId = 0

  private messageShown = false

  private levelEndFlag = false

  constructor(scene: Level) {
    this.scene = scene
    this.setCheckpoints()
    this.bindListeners()
    this.setCollisions()
    this.setLevelEnd()
  }

  private setCheckpoints(): void {
    const checkpointsLayer = this.scene.map.getObjectLayer('Objects/checkpoints')!.objects

    checkpointsLayer.forEach((entry) => {
      if (entry.x && entry.y) {
        const checkpoint = new Checkpoint(
          this.scene, entry.x, entry.y, entry.id,
        )

        this.checkpoints.push(checkpoint)
      } else {
        throw new Error('Checkpoint without coordinates')
      }
    })
  }

  private setLevelEnd(): void {
    const levelEnd = this.scene.map.getObjectLayer('Objects/level-end')!.objects[0]
    
    if (levelEnd.x && levelEnd.y) {
      GameState.set('levelEnd',{
        x: levelEnd.x,
        y: levelEnd.y, 
      })
    }
  }

  private bindListeners(): void {
    EventBus.on(
      'player-reached-checkpoint', this.handlePlayerReachedCheckpoint, this,
    )

    EventBus.on('direction-pressed', (key: string) => {
      if (key === 'UP') {
        if (this.playerIsOnLevelEnd() && !this.levelEndFlag) {
          this.levelEndFlag = true
          EventBus.emit('level-ended')
          this.scene.sound.play('level-end')
          this.scene.scene.stop('ui')
          this.scene.cameras.main.fadeOut(
            1000, 0, 0, 0,
          )
          this.scene.cameras.main.once(Phaser.Cameras.Scene2D.Events.FADE_OUT_COMPLETE, () => {
            if (this.scene.scene.isActive('level')) {
              this.scene.scene.sleep('level')
              this.scene.scene.launch('level-end')
            }
          })
        }
      }
    })
  }

  private setCollisions(): void {
    this.scene.physics.add.collider(
      this.scene.player,
      this.checkpoints,
      (player, checkpoint) => {
        this.handlePlayerReachedCheckpoint(player, checkpoint as Checkpoint)
      },
    )
  }

  private handlePlayerReachedCheckpoint(player: any, checkpoint: Checkpoint): void {
    if (checkpoint.id !== this.currentCheckpointId) {
      this.currentCheckpointId = checkpoint.id

      this.scene.sound.play('checkpoint')

      GameState.set('lastCheckpoint', {
        id: checkpoint.id,
        x: checkpoint.x,
        y: checkpoint.y,
      })

      this.saveCheckpoint()

      EventBus.emit('checkpoint-reached')
      EventBus.emit('text-message',{ 
        text: this.scene.t('messages_checkpoint_reached'),
        size: 'small',
      })
    }
  }

  public update(dt: number): void {
    if (this.playerIsOnLevelEnd() && !this.messageShown) {
      const gamepadName = GameState.getState().gamepad?.name
      let key = ''

      switch (gamepadName) {
      case 'xbox':
        key = 'X'
        break
      case 'ps':
        key = 'SQUARE'
        break
      case 'standard':
        key = 'Y button'
        break
      default:
        key = 'UP'
        break
      }

      EventBus.emit('text-message', this.scene.t('messages_level_1_end_door', { key }))
      this.messageShown = true
    }
  }

  protected playerIsOnLevelEnd(): boolean {
    const { x, y } = this.scene.player
    const { x: endX, y: endY } = GameState.getState().levelEnd

    if (!endX || !endY) {
      return false
    }

    return x > endX && y > endY
  }

  private async saveCheckpoint(): Promise<void> {
    if (GameState.get('isDemo')) {
      return
    }

    try {
      await post('/api/checkpoints', {
        checkpoint: this.currentCheckpointId,
        level: GameState.getState().level, 
      })
    } catch (error) {
      console.error(error)
    }
  }
}