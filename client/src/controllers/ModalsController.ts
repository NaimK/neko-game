import { Scene } from 'phaser'

import { GAME_WIDTH } from '~/const/sizes'
import EventBus from '~/core/EventBus'
import Modal from '~/entities/ui/Modal'
import Level from '~/scenes/levels/Level'
import GameState from '~/store/game'

interface ModalMessageOptions {
  text: string
  size?: 'small' | 'default' | 'big'
  typewriter?: boolean
  x?: number,
  y?: number,
  width?: number,
  height?: number,
  center?: boolean,
  centerLabel?: boolean,
  type?: 'default' | 'alt',
  hideDelay?: number,
  debounced?: boolean,
}

export default class ModalsController {
  private scene: Scene

  public modal?: Modal

  private static lastModalTime = 0

  constructor(scene: Scene) {
    this.scene = scene

    this.bindListeners()
  }

  private bindListeners(): void {
    EventBus.on(
      'level-started', () => {
        if (this.scene instanceof Level) {
          const gamepadName = GameState.getState().gamepad?.name
          let text

          if (gamepadName) {
            text = this.scene.text['level_intro_gamepad'] as string
          } else {
            text = this.scene.text['level_intro'] as string
          } 

          this.showModal({
            text,
            hideDelay: 10000,
          })
        }
      }, this,
    )
    EventBus.on('text-message', (modalMessage: string | ModalMessageOptions) => {
      if (typeof modalMessage !== 'string'  && modalMessage.debounced) {
        this.debouncedShowModal(modalMessage)
      } else {
        this.showModal(modalMessage)
      }
    })
  }

  private debouncedShowModal(modalMessage: string | ModalMessageOptions) {
    const now = Date.now()
    const diff = now - ModalsController.lastModalTime
    const delay = 1000

    if (diff < delay) {
      return
      // We could also call this to queue the modal display
      // setTimeout(() => {
      //   this.showModal(modalMessage)
      // }, delay - diff)
    } else {
      this.showModal(modalMessage)
      ModalsController.lastModalTime = now
    }

  }

  public showModal(modalMessage: string | ModalMessageOptions): Modal {
    console.log('showModal', modalMessage)

    // Only one modal at a time
    if (this.modal) {
      this.modal.destroy()
      this.modal = null as unknown as Modal
    }

    const { size, text, type, hideDelay, typewriter } = typeof modalMessage === 'string' ? {
      size: 'default',
      text: modalMessage,
      type: 'default',
      hideDelay: 7000,
      typewriter: true,
    } : modalMessage

    const width = size === 'small' ? 160 : size === 'big' ? 200 : 160
    const height = size === 'small' ? 32 : size === 'big' ? 100 : 60

    this.modal = new Modal({
      scene: this.scene,
      x: GAME_WIDTH / 2,
      y: 50,
      width,
      height,
      text,
      type: (type || 'default') as 'default' | 'alt',
      center: true,
      centerLabel: true,
      hideDelay: hideDelay ? hideDelay : size === 'small' ? 3000 : size === 'big' ? 10000 : 7000,
      callback: () => {
        this.clearModal()
      },
      typewriter,
    })

    console.log('showing modal', this.modal)

    return this.modal
  }

  public clearModal(): void {
    if (!this.modal) return
    this.modal.destroy()
    this.modal = null as unknown as Modal
  }
} 