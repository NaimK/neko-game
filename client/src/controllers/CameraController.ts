import { GAME_HEIGHT, LEVEL_HEIGHT, LEVEL_WIDTH } from '~/const/sizes'
import EventBus from '~/core/EventBus'
import Level from '~/scenes/levels/Level'
import GameState from '~/store/game'

export default class CameraController {
  private scene: Level

  private camera!: Phaser.Cameras.Scene2D.Camera

  private lerp = 0.2
  
  constructor(scene: Level) {
    this.scene = scene

    this.bindListeners()
  }

  private bindListeners(): void {
    EventBus.on(
      'floor-changed', this.handleFloorChanged, this,
    )
    EventBus.on(
      'player-respawned', this.handlePlayerRespawned, this,
    )
  }

  public setCamera(player: Phaser.Physics.Arcade.Sprite): void {
    this.camera = this.scene.cameras.main.setBounds(
      0, 0, LEVEL_WIDTH, GAME_HEIGHT,
    )

    this.camera.setLerp(this.lerp, this.lerp)

    this.camera.startFollow(player, true)
  }

  private handleFloorChanged(data: { floor: string}): void {
    if (data.floor === 'underground') {
      this.goToUnderground()
    }
  }

  private handlePlayerRespawned(): void {
    if (this.scene.player.y < GAME_HEIGHT) {
      this.goToGround()
    }
  }

  public goToGround(): void {
    this.updateCameraBounds('ground')
    GameState.set('floor', 'ground')
    EventBus.emit('floor-changed', { floor: 'ground' })
  }

  public goToUnderground(): void {
    this.updateCameraBounds('full')

    setTimeout(() => {
      this.updateCameraBounds('underground')
    }, 1000)
  }

  // Update camera bounds
  public updateCameraBounds(level: 'full'|'ground'|'underground'): void {
    this.camera.setLerp(0.1, 0.1)

    switch (level) {
    case 'full':
      this.scene.cameras.main.setBounds(
        0, 0, LEVEL_WIDTH, LEVEL_HEIGHT,
      )
      break
    case 'ground':
      this.scene.cameras.main.setBounds(
        0, 0, LEVEL_WIDTH, GAME_HEIGHT,
      )
      break
    case 'underground':
      this.scene.cameras.main.setBounds(
        0, GAME_HEIGHT, LEVEL_WIDTH, LEVEL_HEIGHT - GAME_HEIGHT, false,
      )
      break
    default:
      break
    }

    setTimeout(() => {
      this.camera.setLerp(this.lerp, this.lerp)
    }, 1000)
  }
}