import { getFirstCollidingTile, getTileUnderPlayer } from '~/helpers/tiles'
import Level from '~/scenes/levels/Level'

export default class PlayerCollisionsController {
  private scene: Level

  private currentTile: Phaser.Tilemaps.Tile | null = null

  constructor(scene: Level) {
    this.scene = scene
  }

  public setCollisions(): void {
    this.setCollisionsWithLava()
    this.setCollisionsWithGround()
    this.setCollisionsWithPlatforms()
    this.setCollisionsWithBridges()
  }

  private setCollisionsWithGround(): void {
    this.scene.ground.setCollisionByProperty({ collides: true })
    this.scene.physics.add.collider(
      this.scene.player, this.scene.ground, this.handlePlayerCollidesWithGround, undefined, this,
    )
  }

  private setCollisionsWithLava(): void {
    this.scene.lava.setCollisionByProperty({ collides: true })
    this.scene.physics.add.collider(
      this.scene.player, this.scene.lava, this.handlePlayerCollidesWithLava, undefined, this,
    )
  }

  private setCollisionsWithPlatforms(): void {
    this.scene.platforms.setCollisionByProperty({ isPlatform: true })
    this.scene.physics.add.collider(
      this.scene.player,
      this.scene.platforms,
      () => this.playerCollidesWithPlatformCallback(), 
      () => this.isPlayerOnPlatformBottom(this.scene.platforms),
      this,
    )
  }

  private setCollisionsWithBridges(): void {
    this.scene.bridges.setCollisionByProperty({ isBridge: true })
    this.scene.physics.add.collider(
      this.scene.player, 
      this.scene.bridges, 
      () => this.handlePlayerCollidesWithBridge(),
      () => this.isPlayerOnPlatformBottom(this.scene.bridges),
      this,
    )
  }

  private handlePlayerCollidesWithGround(): void {
    const tile = getTileUnderPlayer(this.scene.player, this.scene.ground)

    if (tile.tile) {
      this.scene.playerController.debouncedOnPlayerCollideWithGround()
    }
  }

  private handlePlayerCollidesWithLava(): void {
    const tile = getTileUnderPlayer(this.scene.player, this.scene.ground)

    if (tile) {
      this.scene.playerController.onPlayerCollideWithLava()
    }
  }

  private handlePlayerCollidesWithBridge(): void {
    this.scene.player.y += this.scene.platforms.tilemap.tileHeight / 4

    // TODO: Refactor this
    // Hacky way to make the player leave the bridge
    setTimeout(() => {
      this.scene.player.y -= this.scene.platforms.tilemap.tileHeight / 8
    }, 100)

    this.scene.playerController.onPlayerCollideWithBridge()
  }

  private playerCollidesWithPlatformCallback(): void {          
    this.scene.playerController.onPlayerCollideWithPlatform()
  }

  private isPlayerOnPlatformBottom(platforms: Phaser.Tilemaps.TilemapLayer): boolean {
    const result = getFirstCollidingTile(this.scene.player, platforms)

    if (!result) return false

    const position = result.position

    return position === 'bottom'
  }
}