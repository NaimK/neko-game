import EventBus from '~/core/EventBus'
import RobotBulletController from '~/entities/weapons/enemies/robot/RobotBulletController'
import RobotBulletsGroupController from '~/entities/weapons/enemies/robot/RobotBulletsGroupController'
import YellowSuitBulletController from '~/entities/weapons/enemies/yellow-suit/YellowSuitBulletController'
import YellowSuitBulletsGroupController from '~/entities/weapons/enemies/yellow-suit/YellowSuitBulletsGroupController'
import LaserController from '~/entities/weapons/player/laser/LaserController'
import LasersGroupController from '~/entities/weapons/player/laser/LasersGroupController'
import Level from '~/scenes/levels/Level'

export default class AttacksController {
  private scene: Level

  private lasersController!: LasersGroupController

  private robotBulletsController!: RobotBulletsGroupController

  private yellowSuitBulletsController!: YellowSuitBulletsGroupController

  public get enemiesBullets(): Phaser.GameObjects.Sprite[] {
    return [
      ...this.robotBulletsController.weapons.controllers.map((controller) => controller.sprite),
      ...this.yellowSuitBulletsController.weapons.controllers.map((controller) => controller.sprite),
    ]
  }

  constructor(scene: Level) {
    this.scene = scene
    this.bindListeners()
    this.init()
  }

  private bindListeners(): void {
    EventBus.on(
      'player-shoot-laser', this.handlePlayerShootLaser, this,
    )
    EventBus.on(
      'robot-shoot-laser',(robotSprite: Phaser.Physics.Arcade.Sprite) => {
        this.handleRobotShootLaser(robotSprite.body.x, robotSprite.body.y)
      }, this,
    )
    EventBus.on(
      'yellow-suit-shoot-laser',(robotSprite: Phaser.Physics.Arcade.Sprite) => {
        this.handleYellowSuitShootLaser(robotSprite.body.x, robotSprite.body.y)
      }, this,
    )
    EventBus.on(
      'laser-inactivated', this.handleLaserInactivated, this,
    )
  }

  private init(): void {
    this.lasersController = new LasersGroupController(this.scene)
    this.robotBulletsController = new RobotBulletsGroupController(this.scene)
    this.yellowSuitBulletsController = new YellowSuitBulletsGroupController(this.scene)
  }
  
  private handleLaserInactivated(): void {
    //
  }

  private handlePlayerShootLaser(): void {
    this.lasersController.fireLaser(LaserController)
  }

  private handleRobotShootLaser(x: number, y: number): void {
    this.robotBulletsController.fireRobotBullet(
      RobotBulletController, x, y,
    )
  }

  private handleYellowSuitShootLaser(x: number, y: number): void {
    this.yellowSuitBulletsController.fireYellowSuitBullet(
      YellowSuitBulletController, x, y,
    )
  }

  public update(dt: number): void {
    this.lasersController.update(dt)
    this.robotBulletsController.update(dt)
  }
}