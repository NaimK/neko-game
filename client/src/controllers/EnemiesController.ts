import RobotsGroup from '~/entities/enemies/robot/RobotsGroup'
import RobotsGroupController from '~/entities/enemies/robot/RobotsGroupController'
import YellowSuitsGroup from '~/entities/enemies/yellow-suit/YellowSuitsGroup'
import YellowSuitsGroupController from '~/entities/enemies/yellow-suit/YellowSuitsGroupController'
import Level from '~/scenes/levels/Level'

export default class EnemiesController {
  private scene: Level

  private robotsController!: RobotsGroupController

  private yellowSuitsController!: YellowSuitsGroupController

  // get all sprites
  public get sprites(): Phaser.GameObjects.Sprite[] {
    return [
      ...this.robotsController.enemies.controllers.map((controller) => controller.sprite),
      ...this.yellowSuitsController.enemies.controllers.map((controller) => controller.sprite),
    ]
  }

  constructor(scene: Level) {
    this.scene = scene
    this.init()
  }

  /**
   * Initializes the enemies controller
   * @returns void
   * @memberof EnemiesController
   * @private
   * @method init
   *  */
  private init(): void {
    this.initRobots()
    this.initYellowSuits()
  }

  /**
   * Initializes the robots controller
   * @returns void
   * @memberof EnemiesController
   * @private
   * @method initRobots
   * */
  private initRobots(): void {
    const robots: RobotsGroup = new RobotsGroup(this.scene)

    this.robotsController = new RobotsGroupController(this.scene, robots)
  }

  /**
   * Initializes the yellow suits controller
   * @returns void
   * @memberof EnemiesController
   * @private
   * @method initYellowSuits
   * */
  private initYellowSuits(): void {
    const yellowSuits: YellowSuitsGroup = new YellowSuitsGroup(this.scene)

    this.yellowSuitsController = new YellowSuitsGroupController(this.scene, yellowSuits)
  }

  public update(dt: number): void {
    this.robotsController.update(dt)
    this.yellowSuitsController.update(dt)
  }
}