// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

import path from 'path'

import { visualizer } from 'rollup-plugin-visualizer'
import { defineConfig } from 'vite'

export default defineConfig({
  build: {
    minify: 'esbuild',
    target: 'esnext', 
  },
  esbuild: process.env.NODE_ENV === 'production' ? { drop: ['console', 'debugger'] } : {},
  // esbuild: {},
  plugins: [
    visualizer({
      emitFile: true,
      filename: 'stats.html',
    }),
  ],
  resolve: {
    alias: {
      '~': path.resolve(__dirname, './src'),
      '@const' : path.resolve(__dirname, './src/const'),
      '@controllers' : path.resolve(__dirname, './src/controllers'),
      '@core' : path.resolve(__dirname, './src/core'),
      '@scenes' : path.resolve(__dirname, './src/scenes'),
    }, 
  },
  server: {
    host: '0.0.0.0',
    port: 80, 
  },
  clearScreen: false,
})
