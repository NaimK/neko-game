#!/bin/bash

# Usage:
# Swarm Manager : sh ./run_provision_node_from_local.sh --NAME neko-game-1 --DOCKERHUB_USERNAME naimneko --DOCKERHUB_TOKEN 123 --IS_MANAGER true
# Swarm Worker : sh ./run_provision_node_from_local.sh --NAME neko-game-2 --DOCKERHUB_USERNAME naimneko --DOCKERHUB_TOKEN 123 --SWARM_WORKER_TOKEN 123 --SWARM_MANAGER_IP_PORT 123:2377 --SIZE s-1vcpu-2gb

# Expected arguments:
#   1. NAME - Droplet name (required) 
#   2. DOCKERHUB_USERNAME - Dockerhub username (required)
#   3. DOCKERHUB_TOKEN - Dockerhub token (required)
#   4. IS_MANAGER - Is manager (optional)
#   5. SWARM_WORKER_TOKEN - Swarm worker token (optional)
#   6. SWARM_MANAGER_IP_PORT - Swarm manager IP:PORT (optional)
#   7. SIZE - Droplet size (optional)

set -e

while [ $# -gt 0 ]; do
    if [[ $1 == "--"* ]]; then
        v="${1/--/}"
        declare "$v"="$2"
        shift
    fi
    shift
done

if [ -z "$NAME" ]; then
  echo "NAME is required"
  exit 1
fi

if [ -z "$DOCKERHUB_USERNAME" ]; then
  echo "DOCKERHUB_USERNAME is required"
  exit 1
fi

if [ -z "$DOCKERHUB_TOKEN" ]; then
  echo "DOCKERHUB_TOKEN is required"
  exit 1
fi

if [ -z "$IS_MANAGER" ]; then
  IS_MANAGER=false

  if [ -z "$SWARM_WORKER_TOKEN" ]; then
    echo "SWARM_WORKER_TOKEN is required"
    exit 1
  fi

  if [ -z "$SWARM_MANAGER_IP_PORT" ]; then
    echo "SWARM_MANAGER_IP_PORT is required"
    exit 1
  fi
fi

if [ -z "$SIZE" ]; then
  SIZE="s-1vcpu-1gb"
fi

# SSH_FINGERPRINT_DEPLOY=$(doctl compute ssh-key list --no-header | grep "devops-with-laravel-deploy" | awk '{ print $3 }')
SSH_FINGERPRINT_OWN=$(doctl compute ssh-key list !$no-header | grep "M1 Naim" | awk '{ print $4 }')

PUBLIC_KEY=$(cat $HOME/.ssh/digitalocean_rsa.pub)

# OUTPUT=$(doctl compute droplet create --image docker-20-04 --size s-1vcpu-1gb --region ams3 --ssh-keys $SSH_FINGERPRINT_DEPLOY --ssh-keys $SSH_FINGERPRINT_OWN --no-header $NAME)
OUTPUT=$(doctl compute droplet create --image docker-20-04 --size $SIZE --region ams3 --ssh-keys $SSH_FINGERPRINT_OWN --tag-name neko-game --no-header $NAME)

DROPLET_ID=$(echo $OUTPUT | awk '{ print $1 }')

echo "Droplet ID: $DROPLET_ID"
echo "Waiting for droplet to be ready... (sleep 90s)"

sleep 90

doctl projects resources assign 815e050f-8f39-4496-9d52-a1fd1256381d --resource=do:droplet:$DROPLET_ID

sleep 10

SERVER_IP=$(doctl compute droplet get $DROPLET_ID --format PublicIPv4 --no-header)

echo "Server IP: $SERVER_IP"

if [ "$IS_MANAGER" = true ]; then
  echo "Provisioning manager node..."
  scp -C -o StrictHostKeyChecking=no -i $HOME/.ssh/digitalocean_rsa ./provision_manager_node.sh root@$SERVER_IP:./provision_manager_node.sh
  ssh -tt -o StrictHostKeyChecking=no -i $HOME/.ssh/digitalocean_rsa root@$SERVER_IP "chmod +x ./provision_manager_node.sh && ./provision_manager_node.sh \"$PUBLIC_KEY\"" "$DOCKERHUB_USERNAME" "$DOCKERHUB_TOKEN"
  echo "Manager node provisioned!"
else
  echo "Provisioning worker node..."
  scp -C -o StrictHostKeyChecking=no -i $HOME/.ssh/digitalocean_rsa ./provision_node.sh root@$SERVER_IP:./provision_node.sh
  ssh -tt -o StrictHostKeyChecking=no -i $HOME/.ssh/digitalocean_rsa root@$SERVER_IP "chmod +x ./provision_node.sh && ./provision_node.sh \"$PUBLIC_KEY\"" "$DOCKERHUB_USERNAME" "$DOCKERHUB_TOKEN" "$SWARM_WORKER_TOKEN" "$SWARM_MANAGER_IP_PORT"
  echo "Worker node provisioned!"
fi
