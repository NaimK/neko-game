#!/bin/bash

set -ex

SSH_KEY=$1
DOCKERHUB_USERNAME=$2
DOCKERHUB_TOKEN=$3

useradd -G www-data,root,sudo,docker -u 1000 -d /home/neko neko
mkdir -p /home/neko/.ssh
touch /home/neko/.ssh/authorized_keys
chown -R neko:neko /home/neko
chown -R neko:neko /usr/src
chmod 700 /home/neko/.ssh
chmod 644 /home/neko/.ssh/authorized_keys

if [ -n "$SSH_KEY" ]; then
  echo "$SSH_KEY" >> /home/neko/.ssh/authorized_keys
fi

echo "neko ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/neko

docker login -u "$DOCKERHUB_USERNAME" -p "$DOCKERHUB_TOKEN"

ufw allow 2377 && ufw allow 7946 && ufw allow 4789

# Get the IP address of the current node

SWARM_MANAGER_IP=$(hostname -I | awk '{print $1}')

docker swarm init --advertise-addr "$SWARM_MANAGER_IP"

SWARM_WORKER_TOKEN=$(docker swarm join-token worker -q)

echo "SWARM_WORKER_TOKEN: $SWARM_WORKER_TOKEN"
