#!/bin/bash

set -e

SSH_KEY=$1
DOCKERHUB_USERNAME=$2
DOCKERHUB_TOKEN=$3
SWARM_TOKEN=$4
SWARM_MANAGER_IP_PORT=$5

useradd -G www-data,root,sudo,docker -u 1000 -d /home/neko neko
mkdir -p /home/neko/.ssh
touch /home/neko/.ssh/authorized_keys
chown -R neko:neko /home/neko
chown -R neko:neko /usr/src
chmod 700 /home/neko/.ssh
chmod 644 /home/neko/.ssh/authorized_keys

if [ -n "$SSH_KEY" ]; then
  echo "$SSH_KEY" >> /home/neko/.ssh/authorized_keys
fi

echo "neko ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/neko

docker login -u "$DOCKERHUB_USERNAME" -p "$DOCKERHUB_TOKEN"

ufw allow 2377 && ufw allow 7946 && ufw allow 4789

docker swarm join --token "$SWARM_TOKEN" "$SWARM_MANAGER_IP_PORT"
