#!/bin/bash

set -ex

SERVER_IP=$1

echo "Copying docker-compose.monitor.yml /usr/src..."
scp -C -o StrictHostKeyChecking=no -i $HOME/.ssh/digitalocean_rsa ../../../docker-compose.monitor.yml root@$SERVER_IP:/usr/src/docker-compose.monitor.yml
ssh -tt -o StrictHostKeyChecking=no -i $HOME/.ssh/digitalocean_rsa root@$SERVER_IP "cd /usr/src && chmod +x ./docker-compose.monitor.yml && docker stack deploy -c ./docker-compose.monitor.yml monitoring"
echo "Swarm Visualizer deployed at http://$SERVER_IP:8888"