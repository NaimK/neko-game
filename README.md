# Cypher Neko P2E


## Project Structure

```
.
├── client
  ├── [phaser game assets]
├── server
  ├── [NestJS server]
.dockerignore
.docker-compose.yml
Dockerfile
fly.toml
package.json
README.md
```

### ./client

This is where the game files are located. Basically you develop the game from that folder

### ./server

This is where NestJS server is located.

#### Docker files

Docker servers two purposes here:
1. Provide a postgres container for database interactions
2. Manage deployments on fly.io PaaS

- `.dockerignore`: Standard docker ignore file
- `docker-compose.yml`: Includes instructions for postgres database container 
- `Dockerfile`: Used by fly.io for deployments (https://www.tomray.dev/nestjs-docker-production)

#### Other files

- `fly.toml`: fly.io config file
- `package.json`: Mainly used for shortcuts node commands
- `README.md`: This file


## Commands

#### Client

Client commands are located on the `./client` folder

#### Server

- Build docker image

```
docker build .
```

- Start DB

```bash
docker compose up -d
```

- Clear DB

If Postgres container instance is running:
```bash
docker compose down
```

Then:
```bash
docker volume rm neko-game_postgres
```

- Create prisma migration

```
npx prisma migrate dev --name change_you_name_it_table
````

- Run migrations

```
npx prisma migrate deploy
````

- Seed database

```
node ./prisma/seeds/initial.js
````

- Update prisma typescript definitions

```
npx prisma generate
````

- Run server using hot reload

```bash
yarn dev
```

- Rebuild server + client

```bash
yarn build
```

- Run production build

```bash
yarn start:prod
```

- Rebuild server + client and run production build

```bash
yarn build && yarn start:prod
```

- Deploy to fly.io server
Make sur to be connected to the right account using `fly auth whoami` before pushing!

```bash
yarn deploy
```

- Start ngrok http tunnel

```bash
yarn ngrok
```

#### fly.io

- Connect to app with ssh

```
fly ssh console --app cypher-neko-game
fly ssh console --app cypher-neko-game-db
```

- Connect to postgres instance

```
fly postgres connect --app cypher-neko-game-db
```

- Add env variable

```
fly secrets -a cypher-neko-game set VAR=val
```

- Seed database with optional seed (dev purpose)

Once connected to fly.io cypher-neko-game app, run your seed using node cli:

```
node ./prisma/seeds/whitelisted.js
````

#### Postgres

- Select database

```
\c cypher_neko_game
```

- List tables (once connected to a database)

```
\dt
```

- Example query

```
select * from "User";
```

- Database cleanup use case

We might need to cleanup dabase during development. (example at https://gitlab.com/NaimK/neko-game/-/issues/73)
Here's a way to do it:

```
\c cypher_neko_game
\dt
DROP TABLE "User";
DROP TABLE "...";
DROP TABLE _prisma_migrations;
```

Be very careful with this. This must not be done if we want to preserve existing data!