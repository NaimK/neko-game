import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ExperiencePointsService } from 'src/experience_points/experience_points.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private readonly experiencePointsService: ExperiencePointsService,
  ) {}

  // Cron runs every day at 00:00
  @Cron('0 0 * * *')
  async handleDailyNftScore() {
    try {
      const nfts = await this.experiencePointsService.getAllNfts();

      for (const nft of nfts) {
        const bestYesterdayScore =
          await this.experiencePointsService.getBestYesterdayScore(nft.tokenId);

        const xp = await this.experiencePointsService.addToTotalExperience(
          nft.tokenId,
          bestYesterdayScore,
        );

        this.logger.log(`Updated XP to ${xp} for NFT with id ${nft.tokenId}`);
      }

      this.logger.log('Finished updating experience points for NFTs');

      return;
    } catch (error) {
      this.logger.error(
        `Error updating experience points for NFTs: ${error.message}`,
        error,
      );
    }
  }
}
