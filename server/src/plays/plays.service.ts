import { Injectable } from '@nestjs/common';
import { Prisma, Play } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class PlaysService {
  constructor(private prisma: PrismaService) {}

  async createPlay(data: Prisma.PlayCreateInput): Promise<Play> {
    return this.prisma.play.create({
      data,
    });
  }

  // TODO: make this work with select
  async getPlays(params: {
    skip?: number;
    take?: number;
    cursor?: Prisma.PlayWhereUniqueInput;
    where?: Prisma.PlayWhereInput;
    orderBy?: Prisma.PlayOrderByWithRelationInput;
    include?: Prisma.PlayInclude;
    select?: Prisma.PlaySelect;
  }): Promise<Play[]> {
    const { skip, take, cursor, where, orderBy, select, include } = params;
    return this.prisma.play.findMany({
      skip,
      take,
      cursor,
      where,
      orderBy,
      // select,
      include,
    });
  }

  async getPlay(where: Prisma.PlayWhereUniqueInput): Promise<Play | null> {
    return this.prisma.play.findUnique({
      where,
    });
  }

  async getLastPlay(accountAddress: string): Promise<Play | null> {
    return this.prisma.play.findFirst({
      where: {
        user: {
          accountAddress,
        },
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
  }

  async updatePlay(params: {
    where: Prisma.PlayWhereUniqueInput;
    data: Prisma.PlayUpdateInput;
  }): Promise<Play> {
    const { where, data } = params;
    return this.prisma.play.update({
      data,
      where,
    });
  }

  // SCORES
  async getUserBestScore(address: string): Promise<any> {
    const bestScore = await this.prisma.play.findFirst({
      where: {
        score: {
          gt: 0,
        },
        user: {
          accountAddress: address,
        },
      },
      orderBy: {
        score: 'desc',
      },
    });

    if (bestScore) {
      const result = await this.prisma
        .$queryRaw`SELECT row_number() OVER (ORDER BY score DESC) AS position FROM "Play" WHERE id = ${bestScore.id}`;

      return {
        score: bestScore.score,
        position: parseInt(result[0].position),
      };
    }

    return null;
  }
}
