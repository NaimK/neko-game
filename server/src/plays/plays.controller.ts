import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Get,
  Logger,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { UserService } from 'src/user/user.service';
import { PlaysService } from './plays.service';
import { AuthGuard } from 'src/auth/auth.guard';
import { LevelsService } from 'src/levels/levels.service';

@Controller('api/plays')
export class PlaysController {
  constructor(
    private readonly playsService: PlaysService,
    private readonly levelService: LevelsService,
    private readonly userService: UserService,
  ) {}

  private readonly logger = new Logger(PlaysController.name);

  @UseGuards(AuthGuard)
  @Post()
  @ApiOperation({
    summary: 'Create play',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        level: {
          type: 'number',
        },
      },
    },
  })
  async createPlay(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];
    const { level } = req.body;

    const activeNFT = await this.userService
      .user({
        accountAddress,
      })
      .then((user) => {
        return user.activeNftId;
      });

    await this.playsService.createPlay({
      user: {
        connect: {
          accountAddress,
        },
      },
      level,
      NFT: {
        connect: {
          tokenId: activeNFT,
        },
      },
    });

    return res.status(201).json({
      message: 'OK',
    });
  }

  @UseGuards(AuthGuard)
  @Get('me')
  @ApiOperation({
    summary: 'Get my plays',
  })
  async getMyPlays(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];

    const user = await this.userService.user({
      accountAddress,
    });

    const plays = await this.playsService.getPlays({
      where: {
        user: {
          id: user.id,
        },
      },
    });

    return res.status(200).json(plays);
  }

  @UseGuards(AuthGuard)
  @Post('stats')
  @ApiOperation({
    summary: 'Save stats',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        duration: {
          type: 'number',
        },
        coins: {
          type: 'number',
        },
        enemiesKilled: {
          type: 'number',
        },
        lives: {
          type: 'number',
        },
        playerHealth: {
          type: 'number',
        },
      },
    },
  })
  async saveStats(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];
    const { duration, coins, enemiesKilled, lives, playerHealth } = req.body;

    const lastPlay = await this.playsService.getLastPlay(accountAddress);

    const level = await this.levelService.getLevel(lastPlay.level);

    if (!level) {
      this.logger.error('Error - saveStats() Level', lastPlay);
      return res.status(500).json({
        message: 'Error',
      });
    }

    if (!lastPlay || lastPlay.duration) {
      this.logger.error('Unauthorized - saveStats() Last play', lastPlay);
      return res.status(401).json({
        message: 'Unauthorized',
      });
    }

    if (level.checkpoints !== lastPlay.checkpointReached) {
      this.logger.error('Unauthorized - saveStats() Checkpoints', lastPlay);
      return res.status(401).json({
        message: 'Unauthorized',
      });
    }

    const now = new Date();
    const diff = now.getTime() - lastPlay.createdAt.getTime();
    const diffInSeconds = Math.floor(diff / 1000);

    if (
      duration < level.minCompletionTime ||
      diffInSeconds < level.minCompletionTime
    ) {
      this.logger.error('Unauthorized - saveStats() Duration', lastPlay);
      return res.status(401).json({
        message: 'Unauthorized',
      });
    }

    await this.playsService.updatePlay({
      where: {
        id: lastPlay.id,
      },
      data: {
        duration,
        coins,
        enemiesKilled,
        lives,
        playerHealth,
      },
    });

    return res.status(201).json({
      message: 'OK',
    });
  }
}
