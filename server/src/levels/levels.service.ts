import { Injectable } from '@nestjs/common';
import { Level } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class LevelsService {
  constructor(private prisma: PrismaService) {}

  async createLevel(data: {
    name: string;
    checkpoints: number;
    minCompletionTime: number;
  }): Promise<void> {
    await this.prisma.level.create({
      data: {
        minCompletionTime: data.minCompletionTime,
        name: data.name,
        slug: data.name.toLowerCase().replace(/ /g, '-'),
        checkpoints: data.checkpoints,
      },
    });
  }

  async getAllLevels(): Promise<Level[]> {
    const levels = await this.prisma.level.findMany();

    return levels;
  }

  async getLevel(slug: string): Promise<Level> {
    const level = await this.prisma.level.findUnique({
      where: {
        slug,
      },
    });

    return level;
  }

  async updateLevel(
    slug: string,
    data: {
      name: string;
      checkpoints: number;
      minCompletionTime: number;
    },
  ): Promise<void> {
    await this.prisma.level.update({
      where: {
        slug,
      },
      data: {
        name: data.name,
        checkpoints: data.checkpoints,
        minCompletionTime: data.minCompletionTime,
      },
    });
  }

  async deleteLevel(slug: string): Promise<void> {
    await this.prisma.level.delete({
      where: {
        slug,
      },
    });
  }
}
