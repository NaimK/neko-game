import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Delete,
  Get,
  Logger,
  Patch,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { LevelsService } from './levels.service';
import { AdminGuard } from 'src/admin/admin.guard';

@Controller('api/levels')
export class LevelsController {
  constructor(private readonly levelsService: LevelsService) {}

  private readonly logger = new Logger(LevelsController.name);

  @UseGuards(AdminGuard)
  @Post()
  @ApiOperation({
    summary: 'Create level',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
        },
        checkpoints: {
          type: 'number',
        },
        minCompletionTime: {
          type: 'number',
        },
      },
    },
  })
  async createLevel(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const { name, checkpoints, minCompletionTime } = req.body;

    try {
      await this.levelsService.createLevel({
        name,
        checkpoints,
        minCompletionTime,
      });
    } catch (e) {
      this.logger.error('createLevel()', e);

      return res.status(400).json({
        message: 'An error has occurred',
      });
    }

    return res.status(201).json({
      message: 'OK',
    });
  }

  @UseGuards(AdminGuard)
  @ApiOperation({
    summary: 'Get all levels',
  })
  @Get()
  async getAllLevels(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    let levels;

    try {
      levels = await this.levelsService.getAllLevels();
    } catch (e) {
      this.logger.error('getAllLevels()', e);

      return res.status(400).json({
        message: 'An error has occurred',
      });
    }

    return res.status(200).json({
      levels,
    });
  }

  @UseGuards(AdminGuard)
  @ApiOperation({
    summary: 'Get level',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        slug: {
          type: 'string',
        },
      },
    },
  })
  @Get(':slug')
  async getLevel(@Req() req: Request, @Res() res: Response): Promise<Response> {
    const { slug } = req.params;

    let level;

    try {
      level = await this.levelsService.getLevel(slug);
    } catch (e) {
      this.logger.error('getLevel()', e);

      return res.status(400).json({
        message: 'An error has occurred',
      });
    }

    if (!level) {
      return res.status(404).json({
        message: 'Level not found',
      });
    }

    return res.status(200).json({
      level,
    });
  }

  @UseGuards(AdminGuard)
  @ApiOperation({
    summary: 'Update level',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        slug: {
          type: 'string',
        },
        name: {
          type: 'string',
        },
        checkpoints: {
          type: 'number',
        },
        minCompletionTime: {
          type: 'number',
        },
      },
    },
  })
  @Patch(':slug')
  async updateLevel(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const { slug } = req.params;
    const { checkpoints, minCompletionTime, name } = req.body;

    try {
      await this.levelsService.updateLevel(slug, {
        name,
        checkpoints,
        minCompletionTime,
      });
    } catch (e) {
      this.logger.error('updateLevel()', e);

      return res.status(400).json({
        message: 'An error has occurred',
      });
    }

    return res.status(200).json({
      message: 'OK',
    });
  }

  @UseGuards(AdminGuard)
  @ApiOperation({
    summary: 'Delete level',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        slug: {
          type: 'string',
        },
      },
    },
  })
  @Delete(':slug')
  async deleteLevel(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const { slug } = req.params;

    try {
      await this.levelsService.deleteLevel(slug);
    } catch (e) {
      this.logger.error('deleteLevel()', e);

      return res.status(400).json({
        message: 'An error has occurred',
      });
    }

    return res.status(200).json({
      message: 'OK',
    });
  }
}
