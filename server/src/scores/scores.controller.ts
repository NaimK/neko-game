import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthGuard } from 'src/auth/auth.guard';
import { PlaysService } from 'src/plays/plays.service';

@Controller('api/scores')
export class ScoresController {
  constructor(private readonly playsService: PlaysService) {}

  private readonly logger = new Logger(ScoresController.name);

  @UseGuards(AuthGuard)
  @Post()
  @ApiOperation({
    summary: 'Create score',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        score: {
          type: 'number',
        },
      },
    },
  })
  async createScore(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];
    const { score } = req.body;
    const lastPlay = await this.playsService.getLastPlay(accountAddress);

    if (!lastPlay || lastPlay.score || !lastPlay.duration) {
      this.logger.error('Unauthorized - Last play', lastPlay);
      return res.status(401).json({
        message: 'Unauthorized',
      });
    }

    // If the difference between the current time
    // and the time of the update is more than 10 seconds,
    // the score is not saved.
    const now = new Date();
    const diff = now.getTime() - lastPlay.updatedAt.getTime();
    if (diff > 10000) {
      this.logger.error('Unauthorized - Too late', lastPlay);
      return res.status(401).json({
        message: 'Unauthorized',
      });
    }

    await this.playsService.updatePlay({
      where: {
        id: lastPlay.id,
      },
      data: {
        score,
      },
    });

    return res.status(201).json({
      message: 'OK',
    });
  }

  @Get('best')
  @ApiOperation({
    summary: 'Get best scores',
  })
  async getBestScores(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    // return best scores including user data
    const scores = await this.playsService.getPlays({
      where: {
        score: {
          gt: 0,
        },
      },
      orderBy: {
        score: 'desc',
      },
      take: 8,
      // TODO: adapt code to work with select (plays.service.ts)
      // select: {
      //   score: true,
      //   createdAt: true,
      // },
      include: {
        user: {
          select: {
            accountAddress: true,
            name: true,
          },
        },
      },
    });

    const scoresWithRank = scores.map((score, index) => {
      return {
        score: score.score,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        username: score.user.name,
        rank: index + 1,
      };
    });

    return res.status(200).json(scoresWithRank);
  }

  @Get('/me/best')
  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Get best score of the user',
  })
  async getBestScore(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];

    const bestScore = await this.playsService.getUserBestScore(accountAddress);

    if (!bestScore) {
      return res.status(204).json({
        message: 'No score',
      });
    }

    return res.status(200).json({
      message: 'OK',
      content: {
        score: bestScore.score,
        position: bestScore.position,
      },
    });
  }
}
