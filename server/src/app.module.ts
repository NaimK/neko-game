import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { PrismaService } from './prisma/prisma.service';
import { UserService } from './user/user.service';
import { UserController } from './user/user.controller';
import { PlaysController } from './plays/plays.controller';
import { PlaysService } from './plays/plays.service';
import { ScoresController } from './scores/scores.controller';
import { WhitelistedController } from './whitelisted/whitelisted.controller';
import { WhitelistedService } from './whitelisted/whitelisted.service';
import { LevelsController } from './levels/levels.controller';
import { LevelsService } from './levels/levels.service';
import { CheckpointsController } from './checkpoints/checkpoints.controller';
import { IpfsController } from './ipfs/ipfs.controller';
import { IpfsService } from './ipfs/ipfs.service';
import { NftsController } from './nfts/nfts.controller';
import { NftsService } from './nfts/nfts.service';
import { ExperiencePointsService } from './experience_points/experience_points.service';
import { ExperiencePointsController } from './experience_points/experience_points.controller';
import { TasksService } from './tasks/tasks.service';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../../client/dist'),
      serveRoot: '/',
    }),
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    AuthModule,
  ],
  controllers: [
    AppController,
    UserController,
    PlaysController,
    ScoresController,
    WhitelistedController,
    LevelsController,
    CheckpointsController,
    IpfsController,
    NftsController,
    ExperiencePointsController,
  ],
  providers: [
    AppService,
    PrismaService,
    UserService,
    PlaysService,
    WhitelistedService,
    LevelsService,
    IpfsService,
    NftsService,
    ExperiencePointsService,
    TasksService,
  ],
})
export class AppModule {}
