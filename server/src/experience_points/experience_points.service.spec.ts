import { Test, TestingModule } from '@nestjs/testing';
import { ExperiencePointsService } from './experience_points.service';

describe('ExperiencePointsService', () => {
  let service: ExperiencePointsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExperiencePointsService],
    }).compile();

    service = module.get<ExperiencePointsService>(ExperiencePointsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
