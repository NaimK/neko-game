import { Controller, Get, Logger, Post, Req, UseGuards } from '@nestjs/common';
import { AdminGuard } from 'src/admin/admin.guard';
import { TasksService } from 'src/tasks/tasks.service';
import { ExperiencePointsService } from './experience_points.service';

@Controller('api/experience-points')
export class ExperiencePointsController {
  constructor(
    private readonly taskService: TasksService,
    private readonly experiencePointsService: ExperiencePointsService,
  ) {}

  private readonly logger = new Logger(ExperiencePointsController.name);

  @UseGuards(AdminGuard)
  @Post('cron')
  async triggerCron() {
    try {
      await this.taskService.handleDailyNftScore();
    } catch (error) {
      this.logger.error(
        `Error updating experience points for NFTs: ${error.message}`,
        error,
      );
    }
  }

  @Get(':tokenId')
  async getExperiencePoints(@Req() req) {
    const { tokenId } = req.params;

    try {
      const experiencePoints =
        await this.experiencePointsService.getExperiencePoints(tokenId);

      return {
        xp: experiencePoints,
      };
    } catch (e) {
      this.logger.error('getExperiencePoints()', e);

      return {
        message: 'An error has occurred',
      };
    }
  }
}
