import { Injectable } from '@nestjs/common';
import { NFT } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class ExperiencePointsService {
  constructor(private prisma: PrismaService) {}

  async getAllNfts(): Promise<NFT[]> {
    const nfts = await this.prisma.nFT.findMany();

    return nfts;
  }

  async getBestYesterdayScore(tokenId: string) {
    const yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    const bestYesterdayScore = await this.prisma.play.findMany({
      where: {
        nftId: tokenId,
        createdAt: {
          gte: yesterday,
        },
      },
      orderBy: {
        score: 'desc',
      },
      take: 1,
    });

    return bestYesterdayScore[0]?.score || 0;
  }

  async addToTotalExperience(tokenId: string, points: number): Promise<number> {
    const result = await this.prisma.experiencePoints.upsert({
      where: {
        nftId: tokenId,
      },
      create: {
        nftId: tokenId,
        points,
      },
      update: {
        points: {
          increment: points,
        },
      },
    });

    return result.points;
  }

  async getExperiencePoints(tokenId: string): Promise<number> {
    const result = await this.prisma.experiencePoints.findUnique({
      where: {
        nftId: tokenId,
      },
    });

    return result?.points || 0;
  }
}
