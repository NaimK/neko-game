import { Test, TestingModule } from '@nestjs/testing';
import { ExperiencePointsController } from './experience_points.controller';

describe('ExperiencePointsController', () => {
  let controller: ExperiencePointsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExperiencePointsController],
    }).compile();

    controller = module.get<ExperiencePointsController>(ExperiencePointsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
