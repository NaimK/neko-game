import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Get,
  Post,
  Param,
  Req,
  UseGuards,
  Res,
  Logger,
  Query,
} from '@nestjs/common';
import { Response } from 'express';
import { NftsService } from './nfts.service';
import { AuthGuard } from 'src/auth/auth.guard';
import { EvmNft } from '@moralisweb3/common-evm-utils';
import { AdminGuard } from 'src/admin/admin.guard';

@Controller('api/nfts')
export class NftsController {
  constructor(private readonly nftsService: NftsService) {}

  private readonly logger = new Logger(NftsController.name);

  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Get NFTs by wallet',
  })
  @Get('/me')
  async getNftsByWallet(
    @Req() req: Request,
    @Query() query: { refresh?: boolean },
  ): Promise<any> {
    const accountAddress = req['user']['address'];

    try {
      if (!query.refresh) {
        const nftRecords = await this.nftsService.getNftsByWallet(
          accountAddress,
        );

        if (nftRecords.length > 0) {
          return nftRecords;
        }
      }

      const allWalletNfts = await this.nftsService.getMoralisNftsByWallet(
        accountAddress,
      );

      const nfts = allWalletNfts.result.filter((nft) => {
        return (
          nft.tokenAddress.lowercase ===
          process.env.NFT_CONTRACT_ADDRESS.toLowerCase()
        );
      });

      // If NFTs are found for the current user, set the first one as active
      if (nfts.length > 0) {
        // Check in the database if the user has NFT records
        const nftRecords = await this.nftsService.getNftsByWallet(
          accountAddress,
        );

        if (nfts.length > nftRecords.length || query.refresh) {
          nfts.forEach(async (nft) => {
            this.nftsService.upsertNft(accountAddress, nft);
          });

          await this.nftsService.setActiveNft(
            accountAddress,
            nfts[0].tokenId.toString(),
          );
        }

        // TODO: Handle NFT transfers
      }

      return nfts;
    } catch (error) {
      return error;
    }
  }

  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Get game active NFT',
  })
  @Get('/me/active')
  async getActiveNft(@Req() req: Request): Promise<any> {
    const accountAddress = req['user']['address'];

    try {
      const activeNft = await this.nftsService.getActiveNft(accountAddress);

      return activeNft;
    } catch (error) {
      return error;
    }
  }

  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Set active NFT',
    description:
      'Add method to set active NFT. This has to be a NFT that belongs to the current user',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        id: {
          type: 'string',
        },
      },
    },
  })
  @Post('/me/:id')
  async setActiveNft(
    @Req() req: Request,
    @Param('id') id: string,
  ): Promise<any> {
    const accountAddress = req['user']['address'];

    try {
      const allWalletNfts = await this.nftsService.getMoralisNftsByWallet(
        accountAddress,
      );

      const nfts: EvmNft[] = allWalletNfts.result.filter((nft) => {
        return (
          nft.tokenAddress.lowercase ===
          process.env.NFT_CONTRACT_ADDRESS.toLowerCase()
        );
      });

      const nft = nfts.find((nft) => nft.tokenId === id);

      if (!nft) {
        throw new Error('NFT not found');
      }

      const activeNft = await this.nftsService.setActiveNft(
        accountAddress,
        nft.tokenId.toString(),
      );

      return activeNft;
    } catch (error) {
      return error;
    }
  }

  @UseGuards(AdminGuard)
  @ApiOperation({
    summary: 'Get NFT by token ID',
  })
  @Get('/:tokenId')
  async getNftByTokenId(
    @Param('tokenId') tokenId: string,
    @Res() res: Response,
  ): Promise<any> {
    try {
      const nft = await this.nftsService.getNftByTokenId(tokenId);

      if (!nft) {
        return res.status(404).send({
          message: 'NFT not found',
        });
      }

      return res.status(200).send(nft);
    } catch (error) {
      this.logger.error('Error getting NFT by token ID', error);
    }
  }
}
