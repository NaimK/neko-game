import {
  EvmNft,
  GetWalletNFTsResponseAdapter,
} from '@moralisweb3/common-evm-utils';
import { Injectable } from '@nestjs/common';
import { NFT } from '@prisma/client';
import Moralis from 'moralis';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class NftsService {
  constructor(private prisma: PrismaService) {}

  async getMoralisNftsByWallet(
    address: string,
  ): Promise<GetWalletNFTsResponseAdapter> {
    const nfts = await Moralis.EvmApi.nft.getWalletNFTs({
      chain: process.env.CHAIN_ID,
      format: 'decimal',
      address,
      limit: 30,
      normalizeMetadata: true,
    });
    return nfts;
  }

  async getNftsByWallet(address: string): Promise<any> {
    const nfts = await this.prisma.nFT.findMany({
      where: {
        userAddress: address,
      },
    });

    return nfts;
  }

  async createNft(userAddress: string, nft: EvmNft): Promise<void> {
    await this.prisma.nFT.create({
      data: {
        userAddress,
        uri: nft.tokenUri,
        tokenId: nft.tokenId.toString(),
      },
    });
  }

  async upsertNft(userAddress: string, nft: EvmNft): Promise<void> {
    const result = await this.prisma.nFT.upsert({
      where: {
        tokenId: nft.tokenId.toString(),
      },
      create: {
        userAddress,
        uri: nft.tokenUri,
        tokenId: nft.tokenId.toString(),
        metadata: JSON.stringify(nft.metadata),
      },
      update: {
        userAddress,
        uri: nft.tokenUri,
        tokenId: nft.tokenId.toString(),
        metadata: JSON.stringify(nft.metadata),
      },
    });
  }

  async setActiveNft(accountAddress: string, tokenId: string): Promise<void> {
    await this.prisma.user.update({
      where: {
        accountAddress: accountAddress,
      },
      data: {
        activeNftId: tokenId,
      },
    });
  }

  async getActiveNft(accountAddress: string): Promise<NFT> {
    const user = await this.prisma.user.findUnique({
      where: {
        accountAddress: accountAddress,
      },
      select: {
        activeNftId: true,
      },
    });

    const activeNft = await this.prisma.nFT.findUnique({
      where: {
        tokenId: user.activeNftId,
      },
    });

    return activeNft;
  }

  async getNftByTokenId(tokenId: string): Promise<NFT> {
    const nft = await this.prisma.nFT.findUnique({
      where: {
        tokenId: tokenId,
      },
    });

    return nft;
  }

  async deleteNft(tokenId: string): Promise<void> {
    await this.prisma.nFT.delete({
      where: {
        tokenId,
      },
    });
  }
}
