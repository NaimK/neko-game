import { ApiOperation, ApiBody } from '@nestjs/swagger';
import { Controller, Logger, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { PlaysService } from 'src/plays/plays.service';
import { Response } from 'express';

@Controller('api/checkpoints')
export class CheckpointsController {
  constructor(private readonly playsService: PlaysService) {}

  private readonly logger = new Logger(CheckpointsController.name);

  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Save checkpoint',
  })
  @Post()
  async saveCheckpoint(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];

    // We are not using the checkpoint number or level from the request
    // This is just to trick the client into thinking that those are being used
    // in case they want to cheat.
    // const { checkpoint, level } = req.body;

    const lastPlay = await this.playsService.getLastPlay(accountAddress);

    if (lastPlay.score) {
      this.logger.error('Unauthorized - saveCheckpoint()', lastPlay);
      return res.status(401).json({
        message: 'Unauthorized',
      });
    }

    this.playsService.updatePlay({
      where: {
        id: lastPlay.id,
      },
      data: {
        checkpointReached: (lastPlay.checkpointReached ?? 0) + 1,
      },
    });

    return res.status(201).json({
      message: 'OK',
    });
  }
}
