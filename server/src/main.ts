import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { EvmChain } from '@moralisweb3/common-evm-utils';
import Moralis from 'moralis';
import helmet from 'helmet';
import * as cookieParser from 'cookie-parser';
import {
  PrismaClientExceptionFilter,
  PrismaClientKnownRequestExceptionFilter,
} from './filters/prisma-client-exception/prisma-client-exception.filter';
import { NotFoundExceptionFilter } from './filters/not-found-exception/not-found-exception.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  await Moralis.start({
    apiKey: process.env.MORALIS_API_KEY,
    defaultEvmApiChain:
      process.env.ENVIRONMENT === 'production'
        ? EvmChain.ETHEREUM
        : EvmChain.GOERLI,
  });

  app.use(
    helmet({
      contentSecurityPolicy: {
        directives: {
          // Allow blob URLs for images
          'img-src': ["'self'", 'data:', 'blob:'],
          // Allow inline styles and scripts
          'script-src': ["'self'", "'unsafe-inline'", "'unsafe-eval'", 'blob:'],
          // Allow connection to specified URL
          'connect-src': [
            "'self'",
            'https://sepolia.infura.io/v3/',
            'https://o4504650916233216.ingest.sentry.io',
            'https://d57bd3309590427abc796ac5c4e80f63.ingest.sentry.io',
          ],
        },
      },
    }),
  );

  app.use(cookieParser());

  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));
  app.useGlobalFilters(
    new PrismaClientKnownRequestExceptionFilter(httpAdapter),
  );
  app.useGlobalFilters(new NotFoundExceptionFilter());

  if (process.env.ENVIRONMENT === 'development') {
    const config = new DocumentBuilder()
      .setTitle('Cypher Neko API')
      .setDescription('The Cypher Neko API endpoints for the Cypher Neko game.')
      .setVersion('0.1')
      .addTag('cypher-neko')
      .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);
  }

  // await app.listen(8080);
  await app.listen(process.env.SERVER_PORT || 80);
}
bootstrap();
