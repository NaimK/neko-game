import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Get,
  Post,
  Res,
  Req,
  Body,
  HttpException,
  HttpStatus,
  UseGuards,
  Logger,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthService } from './auth.service';
import Moralis from 'moralis';
import { sign, verify } from 'jsonwebtoken';
import { AuthGuard } from './auth.guard';

@Controller('api/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  private readonly logger = new Logger(AuthController.name);

  @ApiOperation({
    summary: 'Request Moralis message',
  })
  @Post('request-message')
  async login(@Body() body: { address: string; chainId: string }) {
    const { address, chainId } = body;

    try {
      const message = await Moralis.Auth.requestMessage({
        address,
        chain: chainId,
        domain: process.env.SERVER_DOMAIN,
        uri: process.env.SERVER_URI,
        statement: `Sign in to ${process.env.SERVER_DOMAIN} using your wallet. We will only use your address to verify your identity and to keep you logged in for your upcoming sessions.`,
        timeout: 60,
      });
      return message;
    } catch (error) {
      this.logger.error('login()', error);
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
  }

  @ApiOperation({
    summary: 'Verify JWT using Moralis',
  })
  @Post('verify')
  async verify(
    @Body() body: { message: string; signature: string },
    @Res() res: Response,
  ) {
    const { message, signature } = body;

    try {
      const { address, profileId } = (
        await Moralis.Auth.verify({
          message,
          signature,
        })
      ).raw;

      const user = { address, profileId, signature };

      const token = sign(user, process.env.AUTH_SECRET);

      res.cookie('jwt', token, {
        httpOnly: true,
        sameSite: 'lax',
        path: '/',
      });

      res.status(200).json(user);
    } catch (error) {
      this.logger.error('verify()', error);
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }
  }

  // Verify the JWT in the cookies
  // If the JWT is valid, return the user's data
  // If the JWT is invalid, return a 403 status
  @ApiOperation({
    summary: 'Check if user is logged in',
  })
  @Get('/check')
  async check(@Req() req: Request, @Res() res: Response) {
    const { jwt } = req.cookies;

    if (!jwt) {
      return res.status(401).json({ message: 'Unauthorized' });
    }

    try {
      verify(jwt, process.env.AUTH_SECRET);
      res.json({
        message: 'Authorized',
      });
    } catch {
      this.logger.error('check()', 'Invalid JWT');
      return res.sendStatus(403);
    }
  }

  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Logout',
  })
  @Get('/logout')
  async logout(@Res() res: Response) {
    try {
      res.clearCookie('jwt');
      return res.json({
        message: 'Logged out',
      });
    } catch {
      this.logger.error('logout()', 'Error logging out');
      return res.sendStatus(403);
    }
  }
}
