import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Get,
  Logger,
  Patch,
  Query,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { UserService } from './user.service';
import { AuthGuard } from 'src/auth/auth.guard';

@Controller('api/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  private readonly logger = new Logger(UserController.name);

  @UseGuards(AuthGuard)
  @Get()
  @ApiOperation({
    summary: 'Get user',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        createIfNotExists: {
          type: 'boolean',
        },
      },
    },
  })
  async getUser(
    @Query() query: { createIfNotExists?: boolean },
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];

    if (!accountAddress) {
      this.logger.error('getUser() - account address is required');
      return res.status(400).send({
        message: 'account address is required',
      });
    }

    const user = await this.userService.user({
      accountAddress,
    });

    if (!user && query.createIfNotExists) {
      const user = await this.userService.createUser({
        accountAddress,
      });

      return res.status(201).send(user);
    }

    if (!user) {
      res.status(404).send({
        message: `User with accountAddress ${accountAddress} not found`,
      });
      return;
    }

    return res.status(200).send(user);
  }

  @UseGuards(AuthGuard)
  @Patch()
  @ApiOperation({
    summary: 'Update user',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
        },
        email: {
          type: 'string',
        },
      },
    },
  })
  async updateUser(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const accountAddress = req['user']['address'];
    const { name, email } = req.body;

    if (!accountAddress) {
      this.logger.error('updateUser() - account address is required');
      return res.status(400).send({
        message: 'account address is required',
      });
    }

    const user = await this.userService.user({
      accountAddress,
    });

    if (!user) {
      this.logger.warn(
        `updateUser() - User with accountAddress ${accountAddress} not found`,
      );
      return res.status(404).send({
        message: `User with accountAddress ${accountAddress} not found`,
      });
    }

    if (name && name.length < 5) {
      return res.status(400).send({
        message: 'Name must be at least 5 characters long',
      });
    }

    if (email) {
      // validate email format
      const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
      if (!emailRegex.test(email)) {
        return res.status(400).send({
          message: 'email is not valid',
        });
      }
    }

    try {
      const updatedUser = await this.userService.updateUser({
        where: {
          accountAddress,
        },
        data: {
          // prevent from updating if the value is already defined
          name: user.name ?? name,
          email: user.email ?? email,
        },
      });
      return res.status(200).send(updatedUser);
    } catch (error) {
      return res.status(400).send({
        message: 'Entry already exists',
      });
    }
  }
}
