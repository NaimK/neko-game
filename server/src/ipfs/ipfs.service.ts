import { Injectable } from '@nestjs/common';
import Moralis from 'moralis';
import { readFileSync } from 'fs';

@Injectable()
export class IpfsService {
  async uploadGenesis() {
    const image = readFileSync(`${__dirname}/../../images/nfts/genesis.gif`, {
      encoding: 'base64',
    });

    const abi = [
      {
        path: 'genesis.gif',
        content: image,
      },
    ];

    const response = await Moralis.EvmApi.ipfs.uploadFolder({ abi });
    const imagePath = response.toJSON()[0].path;

    // send attributes meta data to Moralis
    const metadata = {
      name: 'Genesis',
      description: 'The Original Neko NFT',
      image: imagePath,
      attributes: [
        {
          trait_type: 'Generation',
          value: '1',
        },
        {
          trait_type: 'Eyes',
          value: 'Purple',
        },
        {
          trait_type: 'Ears',
          value: 'Red',
        },
        {
          trait_type: 'Coat',
          value: 'White',
        },
      ],
    };

    const metadataResponse = await Moralis.EvmApi.ipfs.uploadFolder({
      abi: [
        {
          path: 'metadata.json',
          content: Buffer.from(JSON.stringify(metadata)).toString('base64'),
        },
      ],
    });

    const metadataPath = metadataResponse.toJSON()[0].path;

    return {
      image: imagePath,
      metadata: metadataPath,
    };
  }
}
