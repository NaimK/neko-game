import { ApiOperation, ApiBody } from '@nestjs/swagger';
import { Controller, Logger, Post } from '@nestjs/common';
import { IpfsService } from './ipfs.service';

@Controller('/api/ipfs')
export class IpfsController {
  constructor(private readonly ipfsService: IpfsService) {}

  private readonly logger = new Logger(IpfsController.name);

  @Post('/upload/genesis')
  @ApiOperation({
    summary: 'Upload genesis file',
  })
  async uploadGenesis() {
    try {
      const response = await this.ipfsService.uploadGenesis();
      return {
        message: 'OK',
        response,
      };
    } catch (e) {
      this.logger.error('uploadFile()', e);
      return {
        message: 'Error',
        response: e,
      };
    }
  }
}
