import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  NotFoundException,
} from '@nestjs/common';
import { Response } from 'express';
import { join } from 'path';

@Catch(NotFoundException)
export class NotFoundExceptionFilter implements ExceptionFilter {
  catch(exception: NotFoundException, host: ArgumentsHost) {
    if (exception.getStatus() === 404) {
      const ctx = host.switchToHttp();
      const response = ctx.getResponse<Response>();

      response.sendFile('404.html', {
        root: join(__dirname, '../../../client'),
      });
    }
  }
}
