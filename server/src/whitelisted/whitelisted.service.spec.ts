import { Test, TestingModule } from '@nestjs/testing';
import { WhitelistedService } from './whitelisted.service';

describe('WhitelistedService', () => {
  let service: WhitelistedService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WhitelistedService],
    }).compile();

    service = module.get<WhitelistedService>(WhitelistedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
