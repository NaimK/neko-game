import { Injectable } from '@nestjs/common';
import { Whitelisted } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class WhitelistedService {
  constructor(private prisma: PrismaService) {}

  async createWhitelisted(address: string | []): Promise<void> {
    if (Array.isArray(address)) {
      for (const a of address) {
        await this.prisma.whitelisted.create({
          data: {
            address: a,
          },
        });
      }
      return;
    }

    await this.prisma.whitelisted.create({
      data: {
        address,
      },
    });
  }

  async getAllWhitelisted(): Promise<string[]> {
    const whitelisted = await this.prisma.whitelisted.findMany({
      select: {
        address: true,
      },
    });

    return whitelisted.map((w) => w.address);
  }

  async getWhitelisted(address: string): Promise<Whitelisted> {
    const whitelisted = await this.prisma.whitelisted.findUnique({
      where: {
        address,
      },
    });

    return whitelisted;
  }

  async deleteWhitelisted(address: string): Promise<void> {
    await this.prisma.whitelisted.delete({
      where: {
        address,
      },
    });
  }

  async isActivated(): Promise<boolean> {
    return process.env.WHITELISTED_ONLY === 'true';
  }
}
