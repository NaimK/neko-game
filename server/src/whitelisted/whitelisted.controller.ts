import { ApiOperation, ApiBody } from '@nestjs/swagger';
import {
  Controller,
  Delete,
  Get,
  Logger,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { WhitelistedService } from './whitelisted.service';
import { AuthGuard } from 'src/auth/auth.guard';
import { isAddress } from 'ethers';
import { AdminGuard } from 'src/admin/admin.guard';

@Controller('api/whitelisted')
export class WhitelistedController {
  constructor(private readonly whitelistedService: WhitelistedService) {}

  private readonly logger = new Logger(WhitelistedController.name);

  @UseGuards(AdminGuard)
  @Post()
  @ApiOperation({
    summary: 'Create whitelisted address',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        address: {
          type: 'string',
        },
      },
    },
  })
  async createWhitelisted(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const { address } = req.body;

    const addresses = address.split(',');

    if (addresses.length > 1) {
      addresses.forEach((a) => {
        if (!isAddress(a)) {
          return res.status(400).json({
            message: 'Invalid address',
          });
        }
      });

      await this.whitelistedService.createWhitelisted(addresses);
      return res.status(201).json({
        message: 'OK',
      });
    }

    if (!isAddress(address)) {
      return res.status(400).json({
        message: 'Invalid address',
      });
    }

    try {
      await this.whitelistedService.createWhitelisted(address);
    } catch (e) {
      this.logger.error('createWhitelisted()', e);
      return res.status(400).json({
        message: 'Address already whitelisted',
      });
    }

    return res.status(201).json({
      message: 'OK',
    });
  }

  @Get('restrict')
  @ApiOperation({
    summary: 'Check if whitelist is activated',
  })
  async getRestrict(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const restrict = await this.whitelistedService.isActivated();

    return res.status(200).json({
      message: 'OK',
      restrict,
    });
  }

  @Get()
  @ApiOperation({
    summary: 'Get all whitelisted addresses',
  })
  async getAllWhitelisted(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const whitelisted = await this.whitelistedService.getAllWhitelisted();

    return res.status(200).json({
      message: 'OK',
      whitelisted,
    });
  }

  @Get(':address')
  @ApiOperation({
    summary: 'Check if address is whitelisted',
  })
  async getWhitelisted(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const { address } = req.params;

    const whitelisted = await this.whitelistedService.getWhitelisted(
      address as string,
    );

    return res.status(200).json({
      message: 'OK',
      whitelisted: whitelisted ? true : false,
    });
  }

  @UseGuards(AuthGuard)
  @ApiOperation({
    summary: 'Delete whitelisted address',
  })
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        address: {
          type: 'string',
        },
      },
    },
  })
  @Delete()
  async deleteWhitelisted(
    @Req() req: Request,
    @Res() res: Response,
  ): Promise<Response> {
    const { address } = req.body;

    if (!isAddress(address)) {
      return res.status(400).json({
        message: 'Invalid address',
      });
    }

    try {
      await this.whitelistedService.deleteWhitelisted(address);
    } catch (e) {
      return res.status(400).json({
        message: 'Address not whitelisted',
      });
    }

    return res.status(200).json({
      message: 'OK',
    });
  }
}
