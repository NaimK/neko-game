import { Test, TestingModule } from '@nestjs/testing';
import { WhitelistedController } from './whitelisted.controller';

describe('WhitelistedController', () => {
  let controller: WhitelistedController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WhitelistedController],
    }).compile();

    controller = module.get<WhitelistedController>(WhitelistedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
