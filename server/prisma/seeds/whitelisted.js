// eslint-disable-next-line @typescript-eslint/no-var-requires
const client = require('@prisma/client');
const { PrismaClient } = client;

const prisma = new PrismaClient({
  log: ['query', 'info', 'warn'],
});

async function main() {
  if ((await prisma.whitelisted.count()) > 0) {
    return;
  }

  const defaultWhitelist = [
    '0x70651effb37F0369829D6B41e7f44E7E71A78Ddd',
    '0xd750172B2bbDb64B0Eb75de53D99391279F28db2',
    //
    '0x7F371a69AB10516fc2E228574cc78c55a9E4526E',
    //
    '0x7836a35867e191acD09d1609988273d9cb6cd4bA',
  ];

  for await (const address of defaultWhitelist) {
    await prisma.whitelisted.create({
      data: {
        address,
      },
    });
  }

  // Used to be this, but it's not working anymore
  // https://github.com/prisma/prisma/discussions/19839?sort=top
  // await Promise.all(
  //   defaultWhitelist.map((address) =>
  //     prisma.whitelisted.create({
  //       data: {
  //         address,
  //       },
  //     }),
  //   ),
  // );
}

main()
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
