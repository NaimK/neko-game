// eslint-disable-next-line @typescript-eslint/no-var-requires
const client = require('@prisma/client');
const { PrismaClient } = client;

const prisma = new PrismaClient({
  log: ['query', 'info', 'warn'],
});

async function main() {
  if ((await prisma.level.count()) > 0) {
    return;
  }

  const level = await prisma.level.create({
    data: {
      name: 'Level 1',
      slug: 'level-1',
      checkpoints: 3,
      minCompletionTime: 65,
    },
  });
  console.log(level);
}

main()
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
