/*
  Warnings:

  - The primary key for the `Whitelisted` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `Whitelisted` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Whitelisted" DROP CONSTRAINT "Whitelisted_address_fkey";

-- AlterTable
ALTER TABLE "Whitelisted" DROP CONSTRAINT "Whitelisted_pkey",
DROP COLUMN "id";
