/*
  Warnings:

  - A unique constraint covering the columns `[activeNftId]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `nftId` to the `Play` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Play" ADD COLUMN     "nftId" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "activeNftId" TEXT;

-- CreateTable
CREATE TABLE "NFT" (
    "id" SERIAL NOT NULL,
    "tokenId" TEXT NOT NULL,
    "uri" TEXT NOT NULL,
    "userAddress" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "NFT_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "NFT_tokenId_key" ON "NFT"("tokenId");

-- CreateIndex
CREATE UNIQUE INDEX "User_activeNftId_key" ON "User"("activeNftId");

-- AddForeignKey
ALTER TABLE "Play" ADD CONSTRAINT "Play_nftId_fkey" FOREIGN KEY ("nftId") REFERENCES "NFT"("tokenId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NFT" ADD CONSTRAINT "NFT_userAddress_fkey" FOREIGN KEY ("userAddress") REFERENCES "User"("accountAddress") ON DELETE RESTRICT ON UPDATE CASCADE;
