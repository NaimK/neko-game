/*
  Warnings:

  - A unique constraint covering the columns `[slug]` on the table `Level` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `slug` to the `Level` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Level" ADD COLUMN     "slug" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Level_slug_key" ON "Level"("slug");
