-- CreateTable
CREATE TABLE "Whitelisted" (
    "id" SERIAL NOT NULL,
    "address" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Whitelisted_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Whitelisted_address_key" ON "Whitelisted"("address");

-- AddForeignKey
ALTER TABLE "Whitelisted" ADD CONSTRAINT "Whitelisted_address_fkey" FOREIGN KEY ("address") REFERENCES "User"("accountAddress") ON DELETE RESTRICT ON UPDATE CASCADE;
