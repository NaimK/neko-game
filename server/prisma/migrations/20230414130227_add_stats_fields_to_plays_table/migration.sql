-- AlterTable
ALTER TABLE "Play" ADD COLUMN     "coins" INTEGER,
ADD COLUMN     "enemiesKilled" INTEGER,
ADD COLUMN     "lives" INTEGER,
ADD COLUMN     "playerHealth" INTEGER;
