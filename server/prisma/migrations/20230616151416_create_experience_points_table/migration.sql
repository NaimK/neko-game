-- CreateTable
CREATE TABLE "ExperiencePoints" (
    "id" SERIAL NOT NULL,
    "nftId" TEXT NOT NULL,
    "points" INTEGER NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "ExperiencePoints_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "ExperiencePoints_nftId_key" ON "ExperiencePoints"("nftId");

-- AddForeignKey
ALTER TABLE "ExperiencePoints" ADD CONSTRAINT "ExperiencePoints_nftId_fkey" FOREIGN KEY ("nftId") REFERENCES "NFT"("tokenId") ON DELETE RESTRICT ON UPDATE CASCADE;
